package com.tasol.tataEmpSystem.network;


import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiInterface {
    @FormUrlEncoded
    @POST("webservice")
    Call<ResponseBody> getData(@Field("reqObject") String birthday);


    @Multipart
    @POST("webservice")
        Call<ResponseBody> sendMultFiles(@Part("reqObject") String task,
                                @Part List<MultipartBody.Part> avatar);

    @Multipart
    @POST("webservice")
    Call<ResponseBody> saveUserDetectedImage(@Part("reqObject") RequestBody task,
                                        @Part MultipartBody.Part surveyImage);
}