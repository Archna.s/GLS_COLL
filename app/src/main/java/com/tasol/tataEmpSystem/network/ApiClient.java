package com.tasol.tataEmpSystem.network;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    // public static final String BASE_URL = "http://192.168.5.155/attendanceClock/";
    public static final String BASE_URL = "http://192.168.5.155/attendanceClockAll/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}