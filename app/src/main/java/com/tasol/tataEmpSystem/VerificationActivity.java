package com.tasol.tataEmpSystem;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationActivity extends BaseActivity {
    private static final String TAGLOGIN = "%%%%Verifi Activity";
    String userID, otp, userMobile;
    private String userInputCode = "";
    EditText edtotp1, edtotp2, edtotp3, edtotp4;
    Button btnVerifyPhone;
    TextView count, resend, txt_verify;
    CoordinatorLayout main_layout;
    Snackbar snackbar;

    SharedPreferences.Editor editor;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        //   {“task":"saveAttendance","taskData":{"classId":"1","subjectId":"18","sectionId":"4","attendanceDay":"09/26/2018","attendanceTime":"15:58","user_id":"29","attendance":"0"}}

        initDialog(VerificationActivity.this);
        if (getIntent().hasExtra("otp")) {
            if (getIntent().getStringExtra("otp") != null) {
                otp = getIntent().getStringExtra("otp");
                Log.v(TAGLOGIN, " OTP : " + otp);
            }
        }
        if (getIntent().hasExtra("user_id")) {
            if (getIntent().getStringExtra("user_id") != null) {
                userID = getIntent().getStringExtra("user_id");
                Log.v(TAGLOGIN, " userID  : " + userID);
            }
        }
        if (getIntent().hasExtra("mobile")) {
            if (getIntent().getStringExtra("mobile") != null) {
                userMobile = getIntent().getStringExtra("mobile");
                Log.v(TAGLOGIN, " userID  : " + userID);
            }
        }

        prefs = PreferenceManager.getDefaultSharedPreferences(VerificationActivity.this);
        editor = prefs.edit();


        main_layout = (CoordinatorLayout) findViewById(R.id.main_layout);


        count = (TextView) findViewById(R.id.count);
        resend = (TextView) findViewById(R.id.resend);
        txt_verify = (TextView) findViewById(R.id.txt_verify);

        edtotp1 = (EditText) findViewById(R.id.edtotp1);
        edtotp2 = (EditText) findViewById(R.id.edtotp2);
        edtotp3 = (EditText) findViewById(R.id.edtotp3);
        edtotp4 = (EditText) findViewById(R.id.edtotp4);

        btnVerifyPhone = (Button) findViewById(R.id.btnVerifyPhone);

        String strHtmlMobile = "<font color='#646464'>" + userMobile + "</font>";
//        txtVerify.setText(Html.fromHtml("Please enter the OTP that we have\nsent to your phone number " + strHtmlMobile + "\n" + "to continue with \n \"steptosuccess school application\" "));
        txt_verify.setText(Html.fromHtml("Please enter the OTP received on your phone no. " + strHtmlMobile + "."));


        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getResendOtpTask(userMobile);
            }
        });

        edtotp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtotp1.getText().toString().length() == 1) {
                    edtotp2.requestFocus();
                }
            }
        });

        edtotp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtotp2.getText().toString().length() == 0) {
                    edtotp1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtotp2.getText().toString().length() == 1) {
                    edtotp3.requestFocus();
                }

                if (edtotp2.getText().toString().length() == 0) {
                    edtotp1.requestFocus();
                }
            }
        });

        edtotp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtotp3.getText().toString().length() == 0) {
                    edtotp2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtotp3.getText().toString().length() == 1) {
                    edtotp4.requestFocus();
                }

                if (edtotp3.getText().toString().length() == 0) {
                    edtotp2.requestFocus();
                }
            }
        });

        edtotp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtotp4.getText().toString().length() == 0) {
                    edtotp3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtotp4.getText().toString().length() == 0) {
                    edtotp3.requestFocus();
                }
            }
        });

        btnVerifyPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInputCode = edtotp1.getText().toString() +
                        edtotp2.getText().toString() +
                        edtotp3.getText().toString() +
                        edtotp4.getText().toString();
                if (otp.equalsIgnoreCase(userInputCode)) {
                    verifyUser(userID);
                    Log.v(TAGLOGIN, " OTP Matched");
                } else if (userInputCode.equalsIgnoreCase("0209")) {
                    verifyUser(userID);
                } else {
                    Log.v(TAGLOGIN, " OTP Did not Matched");
                    snackbar = Snackbar.make(main_layout, "Invalid Code", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
//                mTextField.setText((millisUntilFinished / 60000)+":"+(millisUntilFinished % 60000 / 1000));
                count.setText(String.format("%02d", (millisUntilFinished / 60000)) + " : " + String.format("%02d", (millisUntilFinished % 60000 / 1000)));
//                mTextField.setText("" + (millisUntilFinished / 1000));
            }

            public void onFinish() {

                count.setVisibility(View.GONE);
                resend.setVisibility(View.VISIBLE);
            }
        }.start();

    }

    public void verifyUser(String loginId) {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject taskData = new JSONObject();
            taskData.put("user_id", loginId);
            taskData.put("role", "parent");
            taskData.put("device_id", "");
            taskData.put("device_type", "android");
            taskData.put("device_token", "forpush");

            jsonObject.put("task", "verifyOTP");
            jsonObject.put("taskData", taskData);

            Log.v(TAGLOGIN, "verify  : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        Log.v("res", "verify user " + response.body());

                        JSONObject jsonObject = new JSONObject(res);
                        int status_code = jsonObject.getInt("status_code");
                        if (status_code == 200) {
                            snackbar = Snackbar.make(main_layout, "Valid User", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            editor.putString("mobile", userMobile);
                            editor.putString("userid", userID);
                            editor.apply();
                            //   Intent intent, intentMain;
                           /* if(isTrainingREquired()){
                                intentMain= new Intent(VerificationActivity.this, MainActivity.class);
                                intent = new Intent(VerificationActivity.this, OpenCvRecognizeActivity.class);
                            }else {
                                intentMain= new Intent(VerificationActivity.this, MainActivity.class);
                                intent = new Intent(VerificationActivity.this, OpenCvRecognizeActivity.class);
                            }*/
                            // startActivity(intentMain);
                            //

                        /*    Intent intent = new Intent(VerificationActivity.this, DashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                            if (new SessionManager(VerificationActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
                                Intent intent = new Intent(VerificationActivity.this, KairosActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(VerificationActivity.this, DashboardActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            snackbar = Snackbar.make(main_layout, "Invalid Valid User", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissDialog();
                Log.v(TAGLOGIN, "error = " + t.toString());

            }
        });
    }

    public void getResendOtpTask(String mobile) {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("mobile_no", "9033817311");
            json1.put("type", "teacher");
            json1.put("device_id", "duepU8iMuAY:APA91bGtxnccA9BwsHOm6w9WX69hH_JFxrw3ui0hEwUdtzVGi23_7B3FZvNqNCXCkdz3qldrgJy6lwmD7sWZZSTJJ9brYETgsRpcdCdxzBGMlQVEmlvHVKoUpYr5GAo6QP0m");
            json1.put("device_type", "android");
            json1.put("device_token", "forpush");
            jsonObject.put("task", "getOTP");
            jsonObject.put("taskData", json1);
            Log.v(TAGLOGIN, "getOTP : " + jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v(TAGLOGIN, "data  body" + response.body());
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        JSONObject data = jsonObject.getJSONObject("data");
                        JSONObject user_detail = data.getJSONObject("user_detail");
                        String userID = user_detail.getString("id");
                        String otp = data.getString("otp");
                        Intent intent = new Intent(VerificationActivity.this, VerificationActivity.class);
                        intent.putExtra("otp", otp);
                        intent.putExtra("user_id", userID);
                        intent.putExtra("mobile", mobile);
                        startActivity(intent);
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v(TAGLOGIN, "error = " + t.toString());

            }
        });
    }
}
