package com.tasol.tataEmpSystem;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kairos.Kairos;
import com.kairos.KairosListener;
import com.tasol.tataEmpSystem.beaconutils.LescanTracker;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.originalFilename;

public class KairosDashboardActivity extends BaseActivity {

    LescanTracker lescanTracker;
    TextView btnNext;
    ImageView ivBackground, ivLogout;

    KairosListener listenerForGallry;
    Kairos myKairos;
    ArrayList<String> gallerylist = new ArrayList<>();
    Timer timer;
    TimerTask task;

    int timerCount = 0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);

        btnNext = (TextView) findViewById(R.id.btnNext);
        ivBackground = (ImageView) findViewById(R.id.ivBackground);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);
        initDialog(KairosDashboardActivity.this);

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View view) {

                new SessionManager(KairosDashboardActivity.this).deleteAllSharePrefs();
                Intent intent = new Intent(KairosDashboardActivity.this, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        startScanningForBeacons();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startScanningForBeacons() {
        //Lescanner initiated here
        lescanTracker = LescanTracker.getInstance(KairosDashboardActivity.this);
        lescanTracker.initManagersAndAdapters();
        startTakingAttendance();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startTakingAttendance() {
        lescanTracker.startScanning();
        //Student Beacons Data
        getNeaestBeaconData();
//        stopScanTimer();
    }

    private void getNeaestBeaconData() {
        lescanTracker.setLescanListener(new LescanTracker.LescanListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onStudentDetected(ScanResult detectedStudents) {
                Log.d("@@@TTT", detectedStudents.getDevice().getAddress());
                String currMajor = String.valueOf((detectedStudents.getScanRecord().getBytes()[25] & 0xff) * 0x100 + (detectedStudents.getScanRecord().getBytes()[26] & 0xff));
                String currMinor = String.valueOf((detectedStudents.getScanRecord().getBytes()[27] & 0xff) * 0x100 + (detectedStudents.getScanRecord().getBytes()[28] & 0xff));
                Log.d("@@@TTT", currMajor + " ---" + currMinor);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        kairosGalleryCall();
        startTimer();
    }

    public void kairosGalleryCall() {
        btnNext.setText("");
        showDialog();
        myKairos = new Kairos();

        String app_id = "42cada49";
        String api_key = "0938911e303cba6fe43d0cb56d98be39";
        myKairos.setAuthentication(this, app_id, api_key);
        getAllSubjectID();

    }

    public void getAllSubjectID() {
        try {
            listenerForGallry = new KairosListener() {
                boolean isRecSuccess = false;

                @Override
                public void onSuccess(String response) {

                    try {
                        timer.cancel();
                        dismissDialog();
                        String username = new SessionManager(KairosDashboardActivity.this).getUserInfo().fullName + "%" + new SessionManager(KairosDashboardActivity.this).getUserInfo().id;

                        Log.e("rrrr", "gallery : " + response.toString());
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("status")) {
                            String isComplete = jsonObject.getString("status");
                            if (isComplete.equalsIgnoreCase("Complete")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("subject_ids");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    gallerylist.add(jsonArray.get(i).toString());
                                    if (username.equalsIgnoreCase(jsonArray.get(i).toString())) {
                                        isRecSuccess = true;
                                        break;
                                    }

                                }

                                Log.e("isRecSuccess : ", "" + isRecSuccess);
                                if (isRecSuccess) {
                                    LoginModel model = new SessionManager(KairosDashboardActivity.this).getUserInfo();
                                    model.istrain = "1";
                                    new SessionManager(KairosDashboardActivity.this).setUserInfo(model);
                                } else {
                                    LoginModel model = new SessionManager(KairosDashboardActivity.this).getUserInfo();
                                    model.istrain = "0";
                                    new SessionManager(KairosDashboardActivity.this).setUserInfo(model);
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    setup();
                }

                @Override
                public void onFail(String response) {
                    //     dismissDialog();
                    dismissDialog();
                    Log.e("KAIROS DEMO error ", response);
                }
            };

            myKairos.listSubjectsForGallery("neeltest1234", listenerForGallry);
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public void setup() {


        if (new SessionManager(KairosDashboardActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
            Glide.with(KairosDashboardActivity.this).load(R.mipmap.ic_launcher).into(ivBackground);

            btnNext.setText("start\ntraning");
            //  Glide.with(KairosDashboardActivity.this).load(R.mipmap.start_training).into(btnNext);
        } else {
            //Glide.with(KairosDashboardActivity.this).load(R.mipmap.take_attendance).into(btnNext);
            btnNext.setText("take\nattendance");
            String filePath = getOutputMediaFileForTraning().getPath();
            File f1 = new File(filePath);
            if (f1.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                bitmap = rotate(bitmap, 270);
                // Glide.with(KairosDashboardActivity.this).load(getOutputMediaFileForTraning()).into(ivBackground);

                ivBackground.setImageBitmap(bitmap);
            } else {
                Glide.with(KairosDashboardActivity.this).load(R.mipmap.ic_launcher).into(ivBackground);

            }

        }


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (new SessionManager(KairosDashboardActivity.this).getUserInfo().istrain.equalsIgnoreCase("1")) {
                    Intent i = new Intent(KairosDashboardActivity.this, KairosActivity.class);
                    startActivity(i);
                } else {
                    dialogTraning();
                }
            }
        });

    }

    public void dialogTraning() {
        // Create custom dialog object

        final Dialog dialog = new Dialog(KairosDashboardActivity.this);
        dialog.setCancelable(false);


        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_traning);
        // Set dialog title

        // set values for custom dialog components - text, image and button
        Button btnOkay;
        TextView txtname, txtMsg;

        txtname = (TextView) dialog.findViewById(R.id.txtname);
        txtMsg = (TextView) dialog.findViewById(R.id.txtMsg);
        btnOkay = (Button) dialog.findViewById(R.id.btnOkay);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Intent i = new Intent(KairosDashboardActivity.this, KairosActivity.class);
                startActivity(i);

            }
        });


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private static File getOutputMediaFileForTraning() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + originalFilename);

        return mediaFile;
    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "CameraAPIDemo");
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);
        // mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public void startTimer() {
        timer = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        timerCount = timerCount + 1;

                        if (timerCount == 7) {
                            timerCount = 0;
                            kairosGalleryCall();
                            Log.e("rrr", "timer count  = " + timerCount);
                        }

                    }
                });
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

}
