package com.tasol.tataEmpSystem.models;

/**
 * Created by ubuntu on 15/11/17.
 */

public class TimeLineModel {
    public String punch_type;
    public String checkIntime;
    public String remote;
    public String address;
    public String officeId;
    public String subjectId;
    public String date;
    public String studentId;
    public String status;
    public String type;
    public String checkdate;
    public String face_image;


    public String id;
    public String latitude;
    public String longitude;
}
