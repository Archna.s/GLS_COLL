package com.tasol.tataEmpSystem.models;

public class AppConfigModel {

    public String AutoApprove;
    public String kairos_galleryname;
    public String kairos_appid;
    public String kairos_appkey;
    public String uuid;
    public PunchTypeModel punching_types;
}
