package com.tasol.tataEmpSystem.models;

public class InfoWindowData {
    public String getStrImage() {
        return strImage;
    }

    public void setStrImage(String strImage) {
        this.strImage = strImage;
    }

    public String strImage;
    public String punch_type;
    public String punch_time;


    public String getPunch_type() {
        return punch_type;
    }

    public void setPunch_type(String punch_type) {
        this.punch_type = punch_type;
    }

    public String getPunch_time() {
        return punch_time;
    }

    public void setPunch_time(String punch_time) {
        this.punch_time = punch_time;
    }

    public String getPunch_location() {
        return punch_location;
    }

    public void setPunch_location(String punch_location) {
        this.punch_location = punch_location;
    }

    private String punch_location;

}