package com.tasol.tataEmpSystem.models;

import java.util.ArrayList;

public class LoginModel {
    public String id;
    public String email;
    public String fullName;
    public String gender;
    public String parentOf;
    public String checkMobileNo;
    public String username;
    public String photo;
    public String role;
    public String istrain;
    public String office_id;
    public String added_on;
    public String thumb;
    public ArrayList<String> punching_types;
    public String leavebalance;
    public String designation;


}
