package com.tasol.tataEmpSystem.services;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.tasol.tataEmpSystem.cam.FaceDetectionCamera;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.filename;
import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.originalFilename;

public class FilledAttendaceService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        //       Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        Log.e("QQQQ", "service start");


        if (intent != null && intent.getExtras() != null) {

            Log.e("wwww", "json data  :  " + intent.getStringExtra("jsondata"));
            saveUserDetectedImage(intent.getStringExtra("jsondata"));
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("QQQQ", "service onDestroy");

        //  Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }


    public void saveUserDetectedImage(String jsonObject) {
        //showDialog();
        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        RequestBody req = createPartFromString("" + jsonObject);

        Call<ResponseBody> call = apiService.saveUserDetectedImage(req, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                if (response.body() != null) {
                    //successFinalDialog();

                    try {
                        // String res = ;
                        String res = response.body().string();
                        Log.e("final response", "storeLocationAttendance - " + res);

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {

                            stopSelf();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }


                //();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("storeLocationAttendance", "error = " + t.toString());

            }
        });
    }


    private static File getOutputMediaFileForTraning() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + originalFilename);

        return mediaFile;
    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, FaceDetectionCamera.mainFoldername);
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }


}
