package com.tasol.tataEmpSystem;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.tasol.tataEmpSystem.database.DatabaseHelper;
import com.tasol.tataEmpSystem.models.AppConfigModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.FieldsValidator;
import com.tasol.tataEmpSystem.utils.OflineSessionManager;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private boolean mPermissionReady;
    public static final String TRAIN_FOLDER = "train_folder";
    public static boolean isFromTrain = false;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    String MY_PREFS_NAME = "MY_PREFS_NAME";
    String mobileNum = "";
    boolean isLoggedIN = false;
    boolean isreadytoLogin = false;
    DatabaseHelper databaseHelper = new DatabaseHelper(SplashActivity.this);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
/*

        LoginModel model = new SessionManager(SplashActivity.this).getUserInfo();
        model.istrain = "0";
        new SessionManager(SplashActivity.this).setUserInfo(model);
*/

        //  databaseHelper.deleteAllTables();


        prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        editor = prefs.edit();


        if (prefs.getString("mobile", null) != null && prefs.getString("mobile", null).length() > 0) {
            mobileNum = prefs.getString("mobile", null);
            if (mobileNum.length() >= 10) {
                isLoggedIN = true;
            } else {
                isLoggedIN = false;
            }
        } else {
            isLoggedIN = false;
        }


        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "Neel Directory" + File.separator + TRAIN_FOLDER);
        if (!directory.exists())
            Toast.makeText(this,
                    (directory.mkdirs() ? "Directory has been created" : "Directory not created"),
                    Toast.LENGTH_SHORT).show();
        else {
            // Toast.makeText(this, "Directory exists", Toast.LENGTH_SHORT).show();
        }
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storagePermssion = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int locationPermissionCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationPermissionFine = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        mPermissionReady = cameraPermission == PackageManager.PERMISSION_GRANTED
                && storagePermssion == PackageManager.PERMISSION_GRANTED && locationPermissionCoarse == PackageManager.PERMISSION_GRANTED && locationPermissionFine == PackageManager.PERMISSION_GRANTED;
        if (!mPermissionReady) {
            requirePermissions();
        } else {
            appConfigTask();

        }
    }

    private void requirePermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 11);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Map<String, Integer> perm = new HashMap<>();
        perm.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_DENIED);
        perm.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);
        perm.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_DENIED);
        perm.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_DENIED);
        for (int i = 0; i < permissions.length; i++) {
            perm.put(permissions[i], grantResults[i]);
        }
        if (perm.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && perm.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && perm.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && perm.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mPermissionReady = true;
            appConfigTask();

        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)

                    ) {
                new AlertDialog.Builder(this)
                        .setMessage(R.string.permission_warning)
                        .setPositiveButton(R.string.dismiss, null)
                        .show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isreadytoLogin) {
            appConfigTask();
        }
    }

    void proceedNext() {


        if (checkGpsServieEnabled()) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent;
                    if (isLoggedIN) {
                        // intent = new Intent(SplashActivity.this, DashboardActivity.class);
                        if (new SessionManager(SplashActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
                            intent = new Intent(SplashActivity.this, KairosActivity.class);

                        } else {
                            intent = new Intent(SplashActivity.this, DashboardActivity.class);
                        }
                        /*if (isTrainingREquired()) {
                        } else {
                            intent = new Intent(SplashActivity.this, OpenCvRecognizeActivity.class);
                        }*/
                    } else {
                        intent = new Intent(SplashActivity.this, LoginActivity.class);
                    }
                    startActivity(intent);
                    finish();

                }
            }, 0);
        } else {
            isreadytoLogin = true;
            Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(viewIntent);
        }


    }

    public boolean checkGpsServieEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
//        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
//
//        try {
//            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        } catch(Exception ex) {}
        return gps_enabled;
    }

    public void appConfigTask() {

        if (!isNetworkConnected()) {
            setupAppConfig(new OflineSessionManager(SplashActivity.this).getappConfig());
            return;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();

            jsonObject.put("task", "getConfig");
            jsonObject.put("taskData", json1);
            Log.e("request : ", "getConfig : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                if (response.body() != null) {
                    try {

                        String res = response.body().string();
                        Log.e("res", "getConfig : " + res);
                        new OflineSessionManager(SplashActivity.this).setAppConfig(res);

                        setupAppConfig(res);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    proceedNext();
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                proceedNext();
                Log.e("res", "error = " + t.toString());

            }
        });
    }

    private void setupAppConfig(String res) {

        if (!TextUtils.isEmpty(res)) {
            try {
                Log.e("RRR", "ofline app config  : " + res);

                JSONObject jsonObject = new JSONObject(res);

                if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                    JSONObject data = jsonObject.getJSONObject("data");


                    Gson gson = new Gson();
                    AppConfigModel model = gson.fromJson(data.toString(), AppConfigModel.class);
                    // model.istrain = "0";
                    new SessionManager(SplashActivity.this).setAppConfig(model);

                } else {
                    new FieldsValidator(SplashActivity.this).customToast(jsonObject.getString("message"));
                }
                proceedNext();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}