package com.tasol.tataEmpSystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.adapters.OfficelistAdapter;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.models.OfficeListModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.FieldsValidator;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewUserActivity extends BaseActivity {

    ArrayList<OfficeListModel> officeList = new ArrayList<>();
    private Spinner spnSelectCompany;
    Button btnSignUp;
    EditText edt_email_signup, edt_fullname_signup, edt_mobile_signup, edt_designation_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        spnSelectCompany = (Spinner) findViewById(R.id.spn_select_company);

        edt_email_signup = (EditText) findViewById(R.id.edt_email_signup);
        edt_fullname_signup = (EditText) findViewById(R.id.edt_fullname_signup);
        edt_mobile_signup = (EditText) findViewById(R.id.edt_mobile_signup);
        edt_designation_signup = (EditText) findViewById(R.id.edt_designation_signup);

        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isValidEmail(edt_email_signup.getText().toString())) {
                    edt_email_signup.setError("Please Enter Valid Email Address");
                } else if (TextUtils.isEmpty(edt_fullname_signup.getText().toString())) {
                    edt_fullname_signup.setError("Please Enter Full Name");
                } else if (!isValidMobile(edt_mobile_signup.getText().toString())) {
                    edt_mobile_signup.setError("Please Enter Mobile Number");
                } else if (TextUtils.isEmpty(edt_designation_signup.getText().toString())) {
                    edt_designation_signup.setError("Please Enter Designation");
                } else if (officeList.get(spnSelectCompany.getSelectedItemPosition()).office_id.equalsIgnoreCase("")) {
                    Toast.makeText(NewUserActivity.this, "Please Select Company", Toast.LENGTH_LONG).show();
                } else {
                    saveNewUser(edt_email_signup.getText().toString(), edt_fullname_signup.getText().toString(), edt_mobile_signup.getText().toString(), edt_designation_signup.getText().toString(), officeList.get(spnSelectCompany.getSelectedItemPosition()).office_id);
                }
            }
        });
        getOfficeDetails();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches() && phone.length() == 10;
    }

    // {"task":"getOfficeDetails","taskData":{}}
    private void getOfficeDetails() {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();

            jsonObject.put("task", "getOfficeDetails");
            jsonObject.put("taskData", json1);
            Log.e("res", "getOfficeDetails : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {

                        String res = response.body().string();
                        Log.e("res", "Office Details : " + res);
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            JSONArray officeArray = data.getJSONArray("offices");

                            OfficeListModel officeListModel0 = new OfficeListModel();
                            officeListModel0.office_id = "";
                            officeListModel0.office = "Select Company";
                            officeList.add(officeListModel0);

                            for (int i = 0; i < officeArray.length(); i++) {
                                JSONObject jsonObject1 = officeArray.getJSONObject(i);
                                OfficeListModel officeListModel = new OfficeListModel();
                                officeListModel.office = jsonObject1.getString("office");
                                officeListModel.office_id = jsonObject1.getString("office_id");
                                officeList.add(officeListModel);
                            }
                            setupofficelist(officeList);
                        } else {
                            new FieldsValidator(NewUserActivity.this).customToast(jsonObject.getString("message"));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("res", "error = " + t.toString());

            }
        });
    }

    private void setupofficelist(ArrayList<OfficeListModel> officeList) {

        OfficelistAdapter adapter = new OfficelistAdapter(NewUserActivity.this,
                R.layout.row_spinner_office, officeList);
        spnSelectCompany.setAdapter(adapter);

        spnSelectCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("TAG", officeList.get(i).office_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void saveNewUser(String email, String fullName, String mobileNumber, String designation, String office_id) {
        initDialog(NewUserActivity.this);
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("email", email);
            json1.put("FirstName", fullName);
            json1.put("LastName", "");
            json1.put("gender", "");
            json1.put("mobileNo", mobileNumber);
            json1.put("designation", designation);
            json1.put("office_id", office_id);

            jsonObject.put("task", "addProfile");
            jsonObject.put("taskData", json1);
            Log.e("res", "addProfile : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                if (response.body() != null) {
                    try {

                        String res = response.body().string();
                        Log.e("response : ", "Office Details : " + res);
                        JSONObject jsonObject = new JSONObject(res);


                        if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                            JSONObject user_detail = jsonObject.getJSONObject("user_data");

                            Gson gson = new Gson();
                            LoginModel model = gson.fromJson(user_detail.toString(), LoginModel.class);
                            model.istrain = "0";
                            new SessionManager(NewUserActivity.this).setUserInfo(model);
                            Toast.makeText(NewUserActivity.this, "Register Successfully", Toast.LENGTH_LONG).show();

                            SharedPreferences.Editor editor;
                            SharedPreferences prefs;

                            prefs = PreferenceManager.getDefaultSharedPreferences(NewUserActivity.this);
                            editor = prefs.edit();

                            editor.putString("mobile", edt_mobile_signup.getText().toString());
                            editor.putString("userid", model.id);
                            editor.apply();

                            Intent intent = new Intent(NewUserActivity.this, KairosActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            new FieldsValidator(NewUserActivity.this).customToast(jsonObject.getString("message"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissDialog();
                Log.e("res", "error = " + t.toString());

            }
        });
    }
}
