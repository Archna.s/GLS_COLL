package com.tasol.tataEmpSystem.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.TimetableSubModel;
import com.tasol.tataEmpSystem.R;

import java.util.ArrayList;

/**
 * Created by user on 22/5/2017.
 */


public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.RecyclerViewHolders> {

    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<TimetableSubModel> filterSubjectlist;
    String semName = "";

    public ScheduleAdapter(Context context, ArrayList<TimetableSubModel> filterSubjectlist) {
        // this.itemList = itemList;
        this.context = context;
        this.filterSubjectlist = filterSubjectlist;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shedule, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);
        TimetableSubModel model = filterSubjectlist.get(position);

        holder.txtTime.setText(model.start);
        holder.txtname.setText(model.subjectId);
        //holder.txtClass.setText(semName);


    }

    public void setSemName(String semName) {
        this.semName = semName;
      //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return filterSubjectlist.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtTime, txtname, txtClass;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtname = (TextView) itemView.findViewById(R.id.txtname);
            txtClass = (TextView) itemView.findViewById(R.id.txtClass);

            //  int newHeight = (150*itemList.get())


        }
    }

}