package com.tasol.tataEmpSystem.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;
import com.tasol.tataEmpSystem.models.PunchTypeWithStatusModel;

import java.util.ArrayList;

/**
 * Created by user on 22/5/2017.
 */


public class PunchTypesAdapter extends RecyclerView.Adapter<PunchTypesAdapter.RecyclerViewHolders> {

    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<PunchTypeWithStatusModel> punchinTypes = new ArrayList<>();
    OnclickCustomlistener onclickCustomlistener;

    public PunchTypesAdapter(Context context, ArrayList<PunchTypeWithStatusModel> punchinTypes, OnclickCustomlistener onclickCustomlistener) {
        // this.itemList = itemList;
        this.context = context;
        this.punchinTypes = punchinTypes;
        this.onclickCustomlistener = onclickCustomlistener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_punchcard, parent, false);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.txtPunchTitle.setText(punchinTypes.get(position).name);
        holder.llPunchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (punchinTypes.get(position).isActive) {
                    onclickCustomlistener.onclicklistener(position, "");
                }
            }
        });

        if (!punchinTypes.get(position).isActive) {
            holder.ivCheck.setVisibility(View.GONE);
            holder.llPunchView.setBackgroundColor(context.getResources().getColor(R.color.dark_grey));
        } else {
            holder.llPunchView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            if (punchinTypes.get(position).isClick) {
                holder.ivCheck.setImageResource(R.mipmap.checked_two);
            } else {
                holder.ivCheck.setImageResource(R.mipmap.unchecked_two);
            }
            holder.ivCheck.setVisibility(View.VISIBLE);
        }



        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);


    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return punchinTypes.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtPunchTitle, txtPunchValue;
        ImageView ivCheck;
        LinearLayout llPunchView;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtPunchTitle = (TextView) itemView.findViewById(R.id.txtPunchTitle);
            txtPunchValue = (TextView) itemView.findViewById(R.id.txtPunchValue);
            ivCheck = (ImageView) itemView.findViewById(R.id.ivCheck);
            llPunchView = (LinearLayout) itemView.findViewById(R.id.llPunchView);


            //  int newHeight = (150*itemList.get())


        }
    }

}