package com.tasol.tataEmpSystem.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.models.DrawerModel;

import java.util.ArrayList;

/**
 * Created by aipxperts on 18/7/17.
 */

public class DrawerAdapter extends BaseAdapter {
    Context context;
    ArrayList<DrawerModel> drawerList = new ArrayList<>();

    public DrawerAdapter(Context context, ArrayList<DrawerModel> drawerList) {
        this.context = context;
        this.drawerList = drawerList;
    }

    @Override
    public int getCount() {
        return drawerList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        final View view = mInflater.inflate(R.layout.row_drawer, parent, false);

        DrawerModel model = drawerList.get(position);

        TextView txtName = (TextView) view.findViewById(R.id.txtName);
        ImageView img = (ImageView) view.findViewById(R.id.img);
        txtName.setText(model.name);

        Glide.with(context).load(drawerList.get(position).img).into(img);
        return view;
    }
}
