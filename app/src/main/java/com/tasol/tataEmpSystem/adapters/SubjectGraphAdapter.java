package com.tasol.tataEmpSystem.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.AttendanceModel;
import com.tasol.tataEmpSystem.R;

import java.util.ArrayList;

/**
 * Created by user on 22/5/2017.
 */


public class SubjectGraphAdapter extends RecyclerView.Adapter<SubjectGraphAdapter.RecyclerViewHolders> {

    private ArrayList<AttendanceModel> itemList = new ArrayList<>();
    private Context context;

    public SubjectGraphAdapter(Context context, ArrayList<AttendanceModel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_subject_graph, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);

//        int nreHeight = (170 * itemList.get(position)) / 100;

        AttendanceModel attendanceModel = itemList.get(position);

        int nreHeight = (170 * Integer.parseInt(attendanceModel.percent)) / 100;

        int dpWidthInPx = (int) (14 * context.getResources().getDisplayMetrics().density);
        int dpHeightInPx = (int) (nreHeight * context.getResources().getDisplayMetrics().density);

        // FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(dpWidthInPx, dpHeightInPx);
        // txtView.setLayoutParams(layoutParams);

        holder.txtView.getLayoutParams().height = dpHeightInPx;
        holder.txtView.getLayoutParams().width = dpWidthInPx;
        String strSubjectName = attendanceModel.name;
        String outputName = "";
        if (strSubjectName.length() < 3) {
            for (int i = 0; i < strSubjectName.length(); i++) {
                outputName = outputName + strSubjectName.charAt(i) + "\n";
            }
        } else {
            for (int i = 0; i < 3; i++) {
                outputName = outputName + strSubjectName.charAt(i) + "\n";
            }
        }

        holder.txt_subject_name.setText(outputName);

        if (position < 2) {
            holder.txtView.setBackgroundColor(context.getResources().getColor(R.color.light_pink));
        } else if (position < 4) {
            holder.txtView.setBackgroundColor(context.getResources().getColor(R.color.light_orange));

        } else {
            holder.txtView.setBackgroundColor(context.getResources().getColor(R.color.light_green));

        }

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
        //  return 10;
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtView, txt_subject_name;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtView = (TextView) itemView.findViewById(R.id.txtView);
            txt_subject_name = (TextView) itemView.findViewById(R.id.txt_subject_name);


            //  int newHeight = (150*itemList.get())


        }
    }

}