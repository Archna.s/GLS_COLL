package com.tasol.tataEmpSystem.adapters;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.models.OfficeListModel;

import java.util.ArrayList;

public class OfficelistAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<OfficeListModel> items;
    private final int mResource;

    public OfficelistAdapter(@NonNull Context context, @LayoutRes int resource,
                             @NonNull ArrayList objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txtMonthName = (TextView) view.findViewById(R.id.txtMonthName);
        View view1 = view.findViewById(R.id.view1);



        txtMonthName.setText(items.get(position).office);

        return view;
    }
}
