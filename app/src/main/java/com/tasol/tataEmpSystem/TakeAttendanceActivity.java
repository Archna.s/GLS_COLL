package com.tasol.tataEmpSystem;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tasol.tataEmpSystem.adapters.PunchTypesAdapter;
import com.tasol.tataEmpSystem.cam.FaceDetectionCamera;
import com.tasol.tataEmpSystem.fragments.BaseFragment;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;
import com.tasol.tataEmpSystem.models.AppConfigModel;
import com.tasol.tataEmpSystem.models.GetLocationAttendanceModel;
import com.tasol.tataEmpSystem.models.PunchTypeWithStatusModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.FieldsValidator;
import com.tasol.tataEmpSystem.utils.SessionManager;
import com.tasol.tataEmpSystem.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.filename;

public class TakeAttendanceActivity extends BaseFragment {

    RecyclerView recyclerView;
    ArrayList<GetLocationAttendanceModel> locationlist = new ArrayList<>();
    ArrayList<PunchTypeWithStatusModel> punchinTypes = new ArrayList<>();

    String strPunchType;
    TextView txtSubmit, txtName;
    ImageView ivTakePhoto;
    PunchTypesAdapter adapter;

    double latitude;
    double longitude;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_take_attendance, container, false);

        initDialog(getActivity());

        latitude = ((DashboardActivity) getActivity()).getLat();
        longitude = ((DashboardActivity) getActivity()).getLong();

        txtName = (TextView) view.findViewById(R.id.txtName);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        txtSubmit = (TextView) view.findViewById(R.id.txtSubmit);
        ivTakePhoto = (ImageView) view.findViewById(R.id.ivTakePhoto);


        txtName.setText(new SessionManager(getActivity()).getUserInfo().username);
        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);

        //  Glide.with(getActivity()).load(file).into(ivTakePhoto);
        //  ivTakePhoto.setImageBitmap(Utils.originalBitmap);
        Glide.with(getActivity()).load(ApiClient.BASE_URL + new SessionManager(getActivity()).getUserInfo().photo).into(ivTakePhoto);


        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isSelectType = false;

                for (int i = 0; i < punchinTypes.size(); i++) {
                    if (punchinTypes.get(i).isClick) {
                        isSelectType = true;
                        break;
                    }
                }
                if (isSelectType) {
                    storeLocationAttendance();
                } else {

                    new FieldsValidator(getActivity()).customToast("Please select punch type.");
                }
            }
        });

        if (new SessionManager(getActivity()).getPunchin().equalsIgnoreCase("")) {
            getLocationAttendance();
        } else {
            getPunchinTypesList();
        }

        return view;
    }


    private void getPunchinTypesList() {

        AppConfigModel model = new AppConfigModel();
        model = new SessionManager(getActivity()).getAppConfig();

        for (int i = 0; i < model.punching_types.master.size(); i++) {
            PunchTypeWithStatusModel punchTypeWithStatusModel = new PunchTypeWithStatusModel();

            punchTypeWithStatusModel.name = model.punching_types.master.get(i);
            punchTypeWithStatusModel.isActive = true;


            punchinTypes.add(punchTypeWithStatusModel);

        }
        for (int i = 0; i < model.punching_types.blocker.size(); i++) {
            PunchTypeWithStatusModel punchTypeWithStatusModel = new PunchTypeWithStatusModel();
            punchTypeWithStatusModel.name = model.punching_types.blocker.get(i);
            punchTypeWithStatusModel.isActive = true;

            punchinTypes.add(punchTypeWithStatusModel);
        }
        for (int i = 0; i < model.punching_types.optional.size(); i++) {
            PunchTypeWithStatusModel punchTypeWithStatusModel = new PunchTypeWithStatusModel();
            punchTypeWithStatusModel.name = model.punching_types.optional.get(i);
            punchTypeWithStatusModel.isActive = true;


            punchinTypes.add(punchTypeWithStatusModel);
        }

        for (int i = 0; i < punchinTypes.size(); i++) {
            String punchType = new SessionManager(getActivity()).getPunchin();

            if (punchType.equalsIgnoreCase("")) {

                if (punchinTypes.get(i).name.equalsIgnoreCase("Check In")) {
                    punchinTypes.get(i).isActive = true;
                } else {
                    punchinTypes.get(i).isActive = false;
                }

            } else if (punchType.equalsIgnoreCase("Check In")) {

                if (punchinTypes.get(i).name.equalsIgnoreCase("Check In")) {
                    punchinTypes.get(i).isActive = false;
                } /*else if (punchinTypes.get(i).name.equalsIgnoreCase("lunch end")) {
                    punchinTypes.get(i).isActive = false;
                } else if (punchinTypes.get(i).name.equalsIgnoreCase("poi out")) {
                    punchinTypes.get(i).isActive = false;
                } */ else {
                    punchinTypes.get(i).isActive = true;
                }


            } else if (punchType.equalsIgnoreCase("Check Out")) {

                if (punchinTypes.get(i).name.equalsIgnoreCase("Check In")) {
                    punchinTypes.get(i).isActive = true;
                } else {
                    punchinTypes.get(i).isActive = false;
                }

            } else if (punchType.equalsIgnoreCase("Lunch Start")) {
                if (punchinTypes.get(i).name.equalsIgnoreCase("Lunch End")) {
                    punchinTypes.get(i).isActive = true;
                } else {
                    punchinTypes.get(i).isActive = false;
                }

            } else if (punchType.equalsIgnoreCase("Lunch End")) {

                if (punchinTypes.get(i).name.equalsIgnoreCase("Check In")) {
                    punchinTypes.get(i).isActive = false;
                } /*else if (punchinTypes.get(i).name.equalsIgnoreCase("Lunch End")) {
                    punchinTypes.get(i).isActive = false;
                } */ else {
                    punchinTypes.get(i).isActive = true;
                }

            } else if (punchType.equalsIgnoreCase("Offsite In")) {
                if (punchinTypes.get(i).name.equalsIgnoreCase("Offsite Out")) {
                    punchinTypes.get(i).isActive = true;
                } else {
                    punchinTypes.get(i).isActive = false;
                }

            } else if (punchType.equalsIgnoreCase("Offsite Out")) {
                if (punchinTypes.get(i).name.equalsIgnoreCase("Check In")) {
                    punchinTypes.get(i).isActive = false;
                } /*else if (punchinTypes.get(i).name.equalsIgnoreCase("poi out")) {
                    punchinTypes.get(i).isActive = false;
                } */ else {
                    punchinTypes.get(i).isActive = true;
                }
            }

        }

        Log.e("111", "punch in type list : " + punchinTypes.size());
        setupRecylerview();
    }

    private void setupRecylerview() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new PunchTypesAdapter(getActivity(), punchinTypes, onclickCustomlistener);
        recyclerView.setAdapter(adapter);

    }

    private void getLocationAttendance() {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", new SessionManager(getActivity()).getUserInfo().id);
//            json1.put("user_id", "3");
            json1.put("attendanceday_from", getCurrentTimeStamp("MM/dd/yyyy").replaceAll("\\\\", ""));


            jsonObject.put("task", "getLocAttendance");
            jsonObject.put("taskData", json1);
            Log.e("request", "getLocAttendance : " + (jsonObject.toString()).replaceAll("\\\\", ""));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject.toString().replaceAll("\\\\", ""));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("response", "data  body" + response.body());
//                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        Gson gson = new Gson();
                        // model.istrain = "0";


                        if (jsonObject.getInt("status_code") == 200) {
                            JSONArray locationArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < locationArray.length(); i++) {
                                JSONObject jsonObject1 = locationArray.getJSONObject(i);
                                GetLocationAttendanceModel model = gson.fromJson(jsonObject1.toString(), GetLocationAttendanceModel.class);
                                locationlist.add(model);

                            }
                            new SessionManager(getActivity()).setPunchin(locationlist.get((locationlist.size() - 1)).punch_type);

                            Log.e("5678", "size  : " + locationlist.size());

                        }
                        getPunchinTypesList();


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        ((DashboardActivity) getActivity()).setTitle("Attendance");
    }

    public void storeLocationAttendance() {
        //  showDialog();


        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            // json1.put("user_id", new SessionManager(getActivity()).getUserInfo().id);
            json1.put("user_office_id", new SessionManager(getActivity()).getUserInfo().office_id);
            json1.put("user_id", new SessionManager(getActivity()).getUserInfo().id);
            json1.put("latitude", latitude);
            json1.put("longitude", longitude);
            json1.put("beacon_major", ((DashboardActivity) getActivity()).getCurrMajor());
            json1.put("beacon_minor", ((DashboardActivity) getActivity()).getCurrMinor());
            json1.put("punch_type", "in");
            // json1.put("remote", "1");
            json1.put("status","1");
            json1.put("punching_time", getCurrentTimeStamp("yyyy-MM-dd hh:mm:ss"));


            jsonObject.put("task", "storeLocationAttendance");
            jsonObject.put("taskData", json1);
            Log.e("request ", "storeLocationAttendance  -  : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        Utils.attendaceObject = "" + jsonObject;
        RequestBody req = createPartFromString("" + jsonObject);

        Utils.strPunchinType = strPunchType;

        Intent intent = new Intent(getActivity(), KairosActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        //   Call<ResponseBody> call = apiService.saveUserDetectedImage(req, body);
/*
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();

                if (response.body() != null) {
                    //successFinalDialog();

                    try {
                        // String res = ;
                        String res = response.body().string();
                        Log.e(" response", "storeLocationAttendance - " + res);

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {
                            new SessionManager(getActivity()).setPunchin(strPunchType);
                            replaceFragment(new MonthWiseAttendanceChildeFragment(), false);

                        } else {
                            new FieldsValidator(getActivity()).customToast(jsonObject.getString("message"));
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }


                //();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("storeLocationAttendance", "error = " + t.toString());

            }
        });
*/
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                MultipartBody.FORM, descriptionString);
    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, FaceDetectionCamera.mainFoldername);
    }

    OnclickCustomlistener onclickCustomlistener = new OnclickCustomlistener() {
        @Override
        public void onclicklistener(int pos, String str) {

            strPunchType = punchinTypes.get(pos).name;
            Toast.makeText(getActivity(), strPunchType, Toast.LENGTH_SHORT).show();

            for (int i = 0; i < punchinTypes.size(); i++) {
                punchinTypes.get(i).isClick = false;

            }
            punchinTypes.get(pos).isClick = true;
            adapter.notifyDataSetChanged();

        }
    };

    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {
        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    private Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        return bitmap;
    }

}
