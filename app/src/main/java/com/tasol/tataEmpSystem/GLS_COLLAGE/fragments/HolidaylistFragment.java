package com.tasol.tataEmpSystem.GLS_COLLAGE.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.adapters.ViewPagerAdapter;
import com.tasol.tataEmpSystem.database.HolidayHelper;
import com.tasol.tataEmpSystem.fragments.BaseFragment;
import com.tasol.tataEmpSystem.fragments.SheduleFragment;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HolidaylistFragment extends BaseFragment {

    ViewPager viewpager;
    TabLayout tabLayout;
    ArrayList<String> monthlist = new ArrayList<>();
    ArrayList<String> holidaylist = new ArrayList<>();
    HolidayHelper holidayHelper;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        holidayHelper = new HolidayHelper(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month_attendance, container, false);

        initDialog(getActivity());
        setupToolbar();
        setup(view);

        return view;
    }

    private void setupToolbar() {
        ((DashboardActivity) getActivity()).rightImageVisibility(View.VISIBLE);
        ((DashboardActivity) getActivity()).setTitle("Dashboard");

    }

    private void setup(View view) {

        viewpager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewpager);

        //getMonthListtask();
        monthlist.add("All");
        monthlist.add("January");
        monthlist.add("February");
        monthlist.add("March");
        monthlist.add("April");
        monthlist.add("May");
        monthlist.add("June");
        monthlist.add("July");
        monthlist.add("August");
        monthlist.add("September");
        monthlist.add("October");
        monthlist.add("November");
        monthlist.add("December");


        if (isNetworkAvailable(getActivity())) {
            getholidayllistTask();
        } else {

            holidaylist.addAll(holidayHelper.getAllHistory());
            setupViewPager(monthlist);

        }


    }

    @Override
    public void onResume() {
        super.onResume();

        // ((DashboardActivity) getActivity()).rightImageVisibility(View.VISIBLE);
        ((DashboardActivity) getActivity()).setTitle("Holiday List");


    }

    private void setupViewPager(ArrayList<String> monthlist) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        for (int i = 0; i < monthlist.size(); i++) {
            //StringTokenizer tokens = new StringTokenizer(monthlist.get(i), " ");
            //String first_string = tokens.nextToken();
            adapter.addFragment(new HolidaylistSubfragment(), monthlist.get(i));
        }
        viewpager.setAdapter(adapter);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {


                ((HolidaylistSubfragment) adapter.getItem(viewpager.getCurrentItem())).displayPosition(holidaylist, position);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((HolidaylistSubfragment) adapter.getItem(viewpager.getCurrentItem())).displayPosition(holidaylist, 0);

            }
        }, 1000);


    }

    private void getholidayllistTask() {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();

            jsonObject.put("task", "getHolidayList");
            jsonObject.put("taskData", json1);
            Log.e("QQQQ", "getHolidayList : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        Log.e("124", "res : getHolidayList  " + res);
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status_code") == 200) {
                            Gson gson = new Gson();
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                String c = jsonArray.getString(i);
                                holidaylist.add(c);

                            }

                            holidayHelper.deleteHoliday();
                            holidayHelper.insertHoliday(holidaylist);
                            setupViewPager(monthlist);
                            //scheduleHelper.insertAllHistory(subjectlist);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());
                dismissDialog();
            }
        });
    }

    public void replaceSchedule() {
        SheduleFragment fragment = new SheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("monthlist", monthlist);
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).addToBackStack(null).commit();

    }

    public int getViewpagerPosition() {
        return viewpager.getCurrentItem();
    }

}