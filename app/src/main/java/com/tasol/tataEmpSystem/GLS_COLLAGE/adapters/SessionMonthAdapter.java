package com.tasol.tataEmpSystem.GLS_COLLAGE.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.MonthModel;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by user on 22/5/2017.
 */


public class SessionMonthAdapter extends RecyclerView.Adapter<SessionMonthAdapter.RecyclerViewHolders> {

    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<MonthModel> filterSubjectlist;
    String semName = "";
    OnclickCustomlistener onclickCustomlistener;

    public SessionMonthAdapter(Context context, ArrayList<MonthModel> filterSubjectlist, OnclickCustomlistener onclickCustomlistener) {
        // this.itemList = itemList;
        this.context = context;
        this.filterSubjectlist = filterSubjectlist;
        this.onclickCustomlistener = onclickCustomlistener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_session_month, parent, false);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);
        MonthModel model = filterSubjectlist.get(position);

        holder.txtDate.setText(dateFromate(model.name.replace("2018", ""), "MMMM", "MMM"));
        //holder.txtClass.setText(semName);

        holder.txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onclickCustomlistener.onclicklistener(position, "onClickMonth");
            }
        });

        if (model.isClick) {
            holder.txtDate.setBackgroundResource(R.drawable.circle_primary);
        } else {
            holder.txtDate.setBackgroundResource(R.drawable.circle_grey);
        }


    }

    public void setSemName(String semName) {
        this.semName = semName;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return filterSubjectlist.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtDate;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtDate = (TextView) itemView.findViewById(R.id.txtDate);

            //  int newHeight = (150*itemList.get())


        }
    }

    public String dateFromate(String strDate, String oldFormate, String newFormate) {
        SimpleDateFormat format = new SimpleDateFormat(oldFormate);
        Date newDate;
        String date = "";
        try {
            newDate = format.parse(strDate);
            format = new SimpleDateFormat(newFormate);
            date = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Formatted date====>" + date);

        return date;
    }


}