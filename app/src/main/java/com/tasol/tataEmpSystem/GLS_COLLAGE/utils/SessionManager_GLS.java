package com.tasol.tataEmpSystem.GLS_COLLAGE.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

public class SessionManager_GLS {

    private SharedPreferences mPrefs;
    private String PREF_getcourse = "pref_getClasses";
    private String PREF_getSemester = "pref_getsemester";

    private String PREF_getsubject = "PREF_getsubject";
    private String PREF_ISLUNCHIN = "PREF_ISLUNCHIN";
    private String PREF_ISOTHERIN = "PREF_ISOTHERIN";

    private String PREF_ISSESSION_START = "PREF_ISSESSION_START";


    public SessionManager_GLS(Context mContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void setCourse(String model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_getcourse, json);
        e.apply();
    }

    public String getCourse() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_getcourse, "");
        String model = gson.fromJson(json, String.class);
        return model;
    }

    public void setSemester(String model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_getSemester, json);
        e.apply();
    }

    public String getSemester() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_getSemester, "");
        String model = gson.fromJson(json, String.class);
        return model;
    }

    public void setSubject(String model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_getsubject, json);
        e.apply();
    }

    public String getSubject() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_getsubject, "");
        String model = gson.fromJson(json, String.class);
        return model;
    }


    public void deleteAllSharePrefs() {
        SharedPreferences.Editor e = mPrefs.edit();
        e.clear().commit();
    }


    public void setISSessionStart(String model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_ISSESSION_START, json);
        e.apply();
    }

    public String getISSessionStart() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_ISSESSION_START, "false");
        String model = gson.fromJson(json, String.class);
        return model;
    }


}
