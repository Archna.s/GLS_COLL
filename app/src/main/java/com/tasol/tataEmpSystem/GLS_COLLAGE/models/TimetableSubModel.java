package com.tasol.tataEmpSystem.GLS_COLLAGE.models;

public class TimetableSubModel {

    public String id;
    public String sectionId;
    public String subjectId;
    public String start;
    public String end;
    public String dayName;
    public String className;
    public String sectionName;


}
