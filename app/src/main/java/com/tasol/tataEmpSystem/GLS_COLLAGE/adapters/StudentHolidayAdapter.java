package com.tasol.tataEmpSystem.GLS_COLLAGE.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tasol.tataEmpSystem.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by user on 22/5/2017.
 */


public class StudentHolidayAdapter extends RecyclerView.Adapter<StudentHolidayAdapter.RecyclerViewHolders> {

    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<String> filterSubjectlist;
    String semName = "";
    int parentPOS;

    public StudentHolidayAdapter(Context context, ArrayList<String> filterSubjectlist, int parentPOS) {
        // this.itemList = itemList;
        this.context = context;
        this.filterSubjectlist = filterSubjectlist;
        this.parentPOS = parentPOS;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_holiday_student, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);
        String model = filterSubjectlist.get(position);
        String str;
        if (parentPOS == 0) {
            str = dateFromate(model, "yyyy-MM-dd", "dd MMM");
        } else {
            str = dateFromate(model, "yyyy-MM-dd", "dd");
        }
        holder.txtDate.setText(str);
        //holder.txtClass.setText(semName);


    }

    public void setSemName(String semName) {
        this.semName = semName;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return filterSubjectlist.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtDate;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtDate = (TextView) itemView.findViewById(R.id.txtDate);

            //  int newHeight = (150*itemList.get())


        }
    }

    public String dateFromate(String strDate, String oldFormate, String newFormate) {
        SimpleDateFormat format = new SimpleDateFormat(oldFormate);
        Date newDate;
        String date = "";
        try {
            newDate = format.parse(strDate);
            format = new SimpleDateFormat(newFormate);
            date = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Formatted date====>" + date);

        return date;
    }

}