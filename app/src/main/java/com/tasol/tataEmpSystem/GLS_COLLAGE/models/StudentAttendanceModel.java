package com.tasol.tataEmpSystem.GLS_COLLAGE.models;

/**
 * Created by aipxperts on 18/7/17.
 */

public class StudentAttendanceModel {

    public String present_count;
    public String absent_count;
    public String this_month;
    public String start_date;
    public String end_date;
}
