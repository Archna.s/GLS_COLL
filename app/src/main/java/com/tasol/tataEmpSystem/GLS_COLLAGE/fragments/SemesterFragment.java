package com.tasol.tataEmpSystem.GLS_COLLAGE.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.GLS_COLLAGE.adapters.SemesterAdapter;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.SemesterModel;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.SessionManager_GLS;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.fragments.BaseFragment;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.FieldsValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SemesterFragment extends BaseFragment {

    RecyclerView recylerview;
    ArrayList<SemesterModel> coursellist = new ArrayList<>();
    TextView txtTitle;
    String courseID = "";
    FloatingActionButton txtNext;
    SemesterModel selectedModel;
    SemesterAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gls_fragment_course, container, false);
        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtNext = (FloatingActionButton) view.findViewById(R.id.txtNext);

        setup();
        return view;

    }

    public void clearData() {
        coursellist.clear();
        adapter.notifyDataSetChanged();
    }

    private void setup() {

        setupRecylerview();


        txtTitle.setText("Select Semester");
        courseID = getArguments().getString("courseId");
        Log.e("AAA", "Course id : " + courseID);

        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean isValid = false;
                for (int i = 0; i < coursellist.size(); i++) {
                    if (coursellist.get(i).isClick) {
                        isValid = true;
                        break;
                    }
                }

                if (isValid) {
                    SubjectFragment ldf = new SubjectFragment();
                    Bundle args = new Bundle();
                    Utils.sectionId = selectedModel.id;
                    args.putString("courseId", selectedModel.id);
                    ldf.setArguments(args);
                    getFragmentManager().beginTransaction().replace(R.id.main_fragment, ldf).addToBackStack(null).commit();

                } else {
                    new FieldsValidator(getActivity()).customToast("Please select course.");

                }
            }
        });


        initDialog(getActivity());
        if (isNetworkAvailable(getActivity())) {
            getCourseData();

        } else {
            setupdata(new SessionManager_GLS(getActivity()).getSemester());
        }
    }


    public void getCourseData() {
        clearData();
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("studentClass", courseID);
            json1.put("webservice", "1");

            jsonObject.put("task", "getClassSections");
            jsonObject.put("taskData", json1);
            Log.v("request : ", "getClassSections : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v("res", "getClassSections " + response.body());
                dismissDialog();

                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        new SessionManager_GLS(getActivity()).setSemester(res);
                        setupdata(res);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("res", "error = " + t.toString());

            }
        });
    }

    public void setupdata(String res) {
        try {
            JSONObject jsonObject = new JSONObject(res);

            if (jsonObject.getInt("status_code") == 200) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                Gson gson = new Gson();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    SemesterModel model = gson.fromJson(jsonObject1.toString(), SemesterModel.class);
                    coursellist.add(model);
                }
                adapter.notifyDataSetChanged();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupRecylerview() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recylerview.setLayoutManager(layoutManager);
        adapter = new SemesterAdapter(getActivity(), coursellist, onclickCustomlistener);
        recylerview.setAdapter(adapter);

    }

    OnclickCustomlistener onclickCustomlistener = new OnclickCustomlistener() {
        @Override
        public void onclicklistener(int pos, String str) {

            selectedModel = coursellist.get(pos);

            for (int i = 0; i < coursellist.size(); i++) {
                coursellist.get(i).isClick = false;
            }
            coursellist.get(pos).isClick = true;
            adapter.notifyDataSetChanged();

        }
    };


}
