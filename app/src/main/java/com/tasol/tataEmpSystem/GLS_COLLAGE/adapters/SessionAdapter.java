package com.tasol.tataEmpSystem.GLS_COLLAGE.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.SessionModel;
import com.tasol.tataEmpSystem.R;

import java.util.ArrayList;

/**
 * Created by user on 22/5/2017.
 */


public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.RecyclerViewHolders> {

    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<SessionModel> filterSubjectlist;
    String semName = "";

    public SessionAdapter(Context context, ArrayList<SessionModel> filterSubjectlist) {
        // this.itemList = itemList;
        this.context = context;
        this.filterSubjectlist = filterSubjectlist;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shedule, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);
        SessionModel model = filterSubjectlist.get(position);

        holder.txtTime.setText(model.className);
        holder.txtname.setText(model.sectionName);
        //holder.txtClass.setText(semName);


    }

    public void setSemName(String semName) {
        this.semName = semName;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return filterSubjectlist.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtTime, txtname, txtClass;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtname = (TextView) itemView.findViewById(R.id.txtname);
            txtClass = (TextView) itemView.findViewById(R.id.txtClass);

            //  int newHeight = (150*itemList.get())


        }
    }

}