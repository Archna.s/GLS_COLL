package com.tasol.tataEmpSystem.GLS_COLLAGE.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mikesu.horizontalexpcalendar.HorizontalExpCalendar;
import com.mikesu.horizontalexpcalendar.common.Config;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.GLS_COLLAGE.adapters.SessionMonthAdapter;
import com.tasol.tataEmpSystem.GLS_COLLAGE.adapters.SessionTeacherAdapter;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.MonthModel;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.SessionTeacherModel;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.fragments.BaseFragment;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//
public class SessionSubfragment extends BaseFragment {

    private static final String TAGMONTH = "12445";
    RecyclerView recylerview, recylerviewMonth;
    private HorizontalExpCalendar horizontalExpCalendar;
    ArrayList<SessionTeacherModel> sessionlist = new ArrayList<>();

    SessionTeacherAdapter adapter;
    TextView txtMessage, txtMessage1;
    ArrayList<String> hoplidaylist = new ArrayList<>();
    LinearLayout llNo;
    ArrayList<MonthModel> monthlist = new ArrayList<>();
    SessionMonthAdapter sessionMonthAdapter;
    /// ArrayList<ArrayList<TimetableSubModel>> dayslistWithsubject = new ArrayList<>();

    String subjectId = "";
    String advmonth = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // scheduleHelper = new HolidayHelper(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        Log.v(TAGMONTH," ScheduleFrag Called : ");
        View view = inflater.inflate(R.layout.fragment_session, container, false);

        initDialog(getActivity());
        setup(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //setupToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        ((DashboardActivity) getActivity()).isVisibleivDrop(View.GONE);
    }

    public void displayPosition(String subjectId) {

        sessionlist.clear();
        adapter.notifyDataSetChanged();
        this.subjectId = subjectId;
        if (isNetworkAvailable(getActivity())) {

            getMonthListtask();
        } else {
            // subjectlist.addAll(scheduleHelper.getAllHistory());

        }

       /* hoplidaylist.clear();
        if (position == 0) {
            for (int i = 0; i < month.size(); i++) {
                String d1 = dateFromate(month.get(i), "yyyy-MM-dd", "dd-MMM");
                hoplidaylist.add(d1);
            }
        } else {
            int pos = position + 0;
            String strPos = "";

            if (pos < 10) {
                strPos = "0" + pos;
            } else {
                strPos = "" + pos;
            }

            Log.e("12345", "" + strPos);


            for (int i = 0; i < month.size(); i++) {
                if (month.get(i).contains("-" + strPos + "-")) {
                    String d1 = dateFromate(month.get(i), "yyyy-MM-dd", "dd");
                    hoplidaylist.add(d1);
                }
            }

        }
        if (hoplidaylist.size() == 0) {
            recylerview.setVisibility(View.GONE);
            llNo.setVisibility(View.VISIBLE);
        } else {
            recylerview.setVisibility(View.VISIBLE);
            llNo.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();

*/
    }

    private void setup(View view) {

        recylerviewMonth = (RecyclerView) view.findViewById(R.id.recylerviewMonth);
        llNo = (LinearLayout) view.findViewById(R.id.llNo);
        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        txtMessage = (TextView) view.findViewById(R.id.txtMessage);
        txtMessage1 = (TextView) view.findViewById(R.id.txtMessage1);
        setupRecylerview();
//        setDates(view, true);
//        setupDropDown();


        horizontalExpCalendar = (HorizontalExpCalendar) view.findViewById(R.id.calendar);

        horizontalExpCalendar.setHorizontalExpCalListener(new HorizontalExpCalendar.HorizontalExpCalListener() {
            @Override
            public void onCalendarScroll(DateTime dateTime) {
                Log.i("TAG", "onCalendarScroll: " + dateTime.toString());

                if (dateTime.getMonthOfYear() > 8) {

                }
            }

            @Override
            public void onDateSelected(DateTime dateTime) {
                if (horizontalExpCalendar.isExpanded()) {
                    horizontalExpCalendar.collapse();
                }
                // getWeekDaylist(dateTime.toString());
                //  Log.e("TAG", "onDateSelected: " + dateTime.toString() + " date  : " + getWeekDaylist(dateTime.toString()));
            }

            @Override
            public void onChangeViewPager(Config.ViewPagerType viewPagerType) {
                Log.e("TAG", "onChangeViewPager: " + viewPagerType.name());
            }
        });
    }


    public void expCollCalender() {
        if (horizontalExpCalendar.isExpanded()) {
            horizontalExpCalendar.collapse();
            ((DashboardActivity) getActivity()).setivDropDown(R.mipmap.down_arrow);
        } else {
            ((DashboardActivity) getActivity()).setivDropDown(R.mipmap.up_arrow);

            horizontalExpCalendar.expand();
        }
    }


    private void setupRecylerview() {


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recylerview.setLayoutManager(layoutManager);
        adapter = new SessionTeacherAdapter(getActivity(), sessionlist);
        recylerview.setAdapter(adapter);


    }


    private String getDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

    //{"task":"getteacherAttendanceView","taskData":
// {"teacherId":"191","sectionId":"1","subjectId":"1","advmonth":"July 2018"}}
    public void getteacherAttendanceView() {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("teacherId", "191");
            //  json1.put("sectionId", "1");
            json1.put("subjectId", subjectId);
            json1.put("advmonth", advmonth);


            jsonObject.put("task", "getteacherAttendanceView");
            jsonObject.put("taskData", json1);
            Log.v("res", "getteacherAttendanceView : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v("res", "getteacherAttendanceView " + response.body());
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            Gson gson = new Gson();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                SessionTeacherModel model = gson.fromJson(jsonObject1.toString(), SessionTeacherModel.class);
                                sessionlist.add(model);
                            }
                            if (sessionlist.size() == 0) {
                                txtMessage1.setVisibility(View.VISIBLE);
                            } else {
                                txtMessage1.setVisibility(View.GONE);
                            }
                            adapter.notifyDataSetChanged();

                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("res", "error = " + t.toString());

            }
        });
    }


    public void getMonthListtask() {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            // json1.put("device_type", "android");

            jsonObject.put("task", "getMonthsList");
            jsonObject.put("taskData", json1);
            Log.v("res", "getMonthListtask : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v("res", "getMonthsList " + response.body());
                dismissDialog();
                monthlist.clear();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {
                            JSONArray jsonArray = jsonObject.getJSONArray("months");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                MonthModel monthModel = new MonthModel();
                                monthModel.name = jsonArray.getString(i);

                                if (TextUtils.isEmpty(Utils.advMonthName)) {
                                    if (i == (jsonArray.length() - 1)) {
                                        monthModel.isClick = true;
                                    }
                                }
                                monthlist.add(monthModel);


                            }

                        }


                        if (TextUtils.isEmpty(Utils.advMonthName)) {
                            if (TextUtils.isEmpty(advmonth)) {
                                advmonth = monthlist.get(monthlist.size() - 1).name;

                            }
                        } else {
                            monthlist.get(Utils.advMonthPos).isClick = true;
                            advmonth = Utils.advMonthName;
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        recylerviewMonth.setLayoutManager(layoutManager);
                        sessionMonthAdapter = new SessionMonthAdapter(getActivity(), monthlist, onclickCustomlistener);
                        recylerviewMonth.setAdapter(sessionMonthAdapter);
                        getteacherAttendanceView();


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("res", "error = " + t.toString());

            }
        });
    }

    OnclickCustomlistener onclickCustomlistener = new OnclickCustomlistener() {
        @Override
        public void onclicklistener(int pos, String str) {

            if (str.equalsIgnoreCase("onClickMonth")) {

                for (int i = 0; i < monthlist.size(); i++) {
                    monthlist.get(i).isClick = false;
                }
                monthlist.get(pos).isClick = true;

                sessionlist.clear();
                adapter.notifyDataSetChanged();
                sessionMonthAdapter.notifyDataSetChanged();

                advmonth = monthlist.get(pos).name;
                Utils.advMonthName = advmonth;
                Utils.advMonthPos = pos;

                if (isNetworkAvailable(getActivity())) {

                    getteacherAttendanceView();
                } else {
                    // subjectlist.addAll(scheduleHelper.getAllHistory());

                }
            }

        }
    };


}
