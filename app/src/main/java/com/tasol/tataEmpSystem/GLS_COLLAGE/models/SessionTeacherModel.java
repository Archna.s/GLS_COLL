package com.tasol.tataEmpSystem.GLS_COLLAGE.models;

/**
 * Created by aipxperts on 18/7/17.
 */

public class SessionTeacherModel {

    public String id;
    public String classId;
    public String sectionId;
    public String subjectId;
    public String majorminor;
    public String teacherId;
    public String startTime;

    public String endTime;
    public String className;
    public String sectionName;
    public String present_count;
    public String absent_count;
    public String total_count;


}
