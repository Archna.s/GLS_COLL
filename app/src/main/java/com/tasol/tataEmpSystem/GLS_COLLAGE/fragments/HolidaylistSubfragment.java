package com.tasol.tataEmpSystem.GLS_COLLAGE.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikesu.horizontalexpcalendar.HorizontalExpCalendar;
import com.mikesu.horizontalexpcalendar.common.Config;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.GLS_COLLAGE.adapters.HolidayAdapter;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.TimetableSubModel;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.fragments.BaseFragment;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//
public class HolidaylistSubfragment extends BaseFragment {

    private static final String TAGMONTH = "12445";
    RecyclerView recylerview;
    //    HorizontalCalendar horizontalCalendar;
//    Spinner spinner;
//    boolean isFirstTime = true;
    ArrayList<String> monthlist = new ArrayList<>();
    //    ArrayList<String> allmonthlist = new ArrayList<>();
    private HorizontalExpCalendar horizontalExpCalendar;
    //    private static final String TAGMONTH = "%%%%ScheduleFrag";

    ArrayList<TimetableSubModel> subjectlist = new ArrayList<>();
    ArrayList<TimetableSubModel> filterSubjectlist = new ArrayList<>();
    HolidayAdapter adapter;
    TextView txtMessage;
    Boolean isDown = true;
    String semName = "";
    ArrayList<String> hoplidaylist = new ArrayList<>();
    LinearLayout llNo;
    /// ArrayList<ArrayList<TimetableSubModel>> dayslistWithsubject = new ArrayList<>();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

       // scheduleHelper = new HolidayHelper(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        Log.v(TAGMONTH," ScheduleFrag Called : ");
        View view = inflater.inflate(R.layout.fragment_holidays, container, false);

        initDialog(getActivity());
        setup(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //setupToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        ((DashboardActivity) getActivity()).isVisibleivDrop(View.GONE);
    }

    public void displayPosition(ArrayList<String> month, int position) {
        hoplidaylist.clear();
        if (position == 0) {
            for (int i = 0; i < month.size(); i++) {
                String d1 = dateFromate(month.get(i), "yyyy-MM-dd", "dd-MMM");
                hoplidaylist.add(d1);
            }
        } else {
            int pos = position + 0;
            String strPos = "";

            if (pos < 10) {
                strPos = "0" + pos;
            } else {
                strPos = "" + pos;
            }

            Log.e("12345", "" + strPos);


            for (int i = 0; i < month.size(); i++) {
                if (month.get(i).contains("-" + strPos + "-")) {
                    String d1 = dateFromate(month.get(i), "yyyy-MM-dd", "dd");
                    hoplidaylist.add(d1);
                }
            }

        }
        if (hoplidaylist.size() == 0) {
            recylerview.setVisibility(View.GONE);
            llNo.setVisibility(View.VISIBLE);
        } else {
            recylerview.setVisibility(View.VISIBLE);
            llNo.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();


    }

    private void setup(View view) {

        llNo = (LinearLayout) view.findViewById(R.id.llNo);
        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        txtMessage = (TextView) view.findViewById(R.id.txtMessage);
//        spinner = (Spinner) view.findViewById(R.id.spinner);

        Bundle args = getArguments();
        if (args != null) {

            monthlist = (ArrayList<String>) args.getSerializable("monthlist");
        }

        //  addAllmonthlist();
        setupRecylerview();
//        setDates(view, true);
//        setupDropDown();


        horizontalExpCalendar = (HorizontalExpCalendar) view.findViewById(R.id.calendar);

        horizontalExpCalendar.setHorizontalExpCalListener(new HorizontalExpCalendar.HorizontalExpCalListener() {
            @Override
            public void onCalendarScroll(DateTime dateTime) {
                Log.i("TAG", "onCalendarScroll: " + dateTime.toString());

                if (dateTime.getMonthOfYear() > 8) {

                }
            }

            @Override
            public void onDateSelected(DateTime dateTime) {
                if (horizontalExpCalendar.isExpanded()) {
                    horizontalExpCalendar.collapse();
                }
                // getWeekDaylist(dateTime.toString());
                //  Log.e("TAG", "onDateSelected: " + dateTime.toString() + " date  : " + getWeekDaylist(dateTime.toString()));
            }

            @Override
            public void onChangeViewPager(Config.ViewPagerType viewPagerType) {
                Log.e("TAG", "onChangeViewPager: " + viewPagerType.name());
            }
        });
    }


    public void expCollCalender() {
        if (horizontalExpCalendar.isExpanded()) {
            horizontalExpCalendar.collapse();
            ((DashboardActivity) getActivity()).setivDropDown(R.mipmap.down_arrow);
        } else {
            ((DashboardActivity) getActivity()).setivDropDown(R.mipmap.up_arrow);

            horizontalExpCalendar.expand();
        }
    }


    private void setupRecylerview() {


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recylerview.setLayoutManager(layoutManager);
        adapter = new HolidayAdapter(getActivity(), hoplidaylist);
        recylerview.setAdapter(adapter);

        if (isNetworkAvailable(getActivity())) {
            //  getholidayllistTask();
        } else {
            // subjectlist.addAll(scheduleHelper.getAllHistory());

        }
    }


    private String getDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

}
