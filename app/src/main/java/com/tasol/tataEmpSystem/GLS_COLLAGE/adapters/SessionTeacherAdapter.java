package com.tasol.tataEmpSystem.GLS_COLLAGE.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.SessionTeacherModel;
import com.tasol.tataEmpSystem.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by user on 22/5/2017.
 */


public class SessionTeacherAdapter extends RecyclerView.Adapter<SessionTeacherAdapter.RecyclerViewHolders> {

    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<SessionTeacherModel> filterSubjectlist;
    String semName = "";

    public SessionTeacherAdapter(Context context, ArrayList<SessionTeacherModel> filterSubjectlist) {
        // this.itemList = itemList;
        this.context = context;
        this.filterSubjectlist = filterSubjectlist;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_teacher_session, parent, false);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);
        SessionTeacherModel model = filterSubjectlist.get(position);

        String startDate = dateFromate(model.startTime, "yyyy-MM-dd hh:mm:ss", "dd/MM/yyyy hh:mm a");
        String endDate = dateFromate(model.endTime, "yyyy-MM-dd hh:mm:ss", "hh:mm a");

        holder.txtSession.setText(model.className + " - " + model.sectionName);
        holder.txtDate.setText(startDate + " - " + endDate);
        holder.txtBlock.setText(model.className);
        holder.txtAttendance.setText(model.present_count + "/" + model.total_count);
        //holder.txtClass.setText(semName);


    }

    public void setSemName(String semName) {
        this.semName = semName;
        //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return filterSubjectlist.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtDate, txtBlock, txtAttendance, txtSession;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtSession = (TextView) itemView.findViewById(R.id.txtSession);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtBlock = (TextView) itemView.findViewById(R.id.txtBlock);
            txtAttendance = (TextView) itemView.findViewById(R.id.txtAttendance);

            //  int newHeight = (150*itemList.get())


        }
    }

    public String dateFromate(String strDate, String oldFormate, String newFormate) {
        SimpleDateFormat format = new SimpleDateFormat(oldFormate);
        Date newDate;
        String date = "";
        try {
            newDate = format.parse(strDate);
            format = new SimpleDateFormat(newFormate);
            date = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Formatted date====>" + date);

        return date;
    }


}