package com.tasol.tataEmpSystem.GLS_COLLAGE.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.GLS_COLLAGE.adapters.SubjectAdapter;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.SubjectModel;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.SessionManager_GLS;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils;
import com.tasol.tataEmpSystem.KairosActivity;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.fragments.BaseFragment;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.FieldsValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubjectFragment extends BaseFragment {

    RecyclerView recylerview;
    ArrayList<SubjectModel> coursellist = new ArrayList<>();
    TextView txtTitle;
    String courseID = "";
    FloatingActionButton txtNext;
    SubjectModel selectedModel;
    SubjectAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gls_fragment_course, container, false);
        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtNext = (FloatingActionButton) view.findViewById(R.id.txtNext);

        setup();

        return view;

    }

    private void setup() {

        setupRecylerview();

        txtTitle.setText("Select Subject");
        courseID = getArguments().getString("courseId");
        Log.e("AAA", "Course id : " + courseID);

        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /*    SubjectFragment ldf = new SubjectFragment();
                Bundle args = new Bundle();

                //  args.putString("subjectId", selectedModel.id);
                //ldf.setArguments(args);
                getFragmentManager().beginTransaction().replace(R.id.main_fragment, ldf).addToBackStack(null).commit();
*/
                boolean isValid = false;
                for (int i = 0; i < coursellist.size(); i++) {
                    if (coursellist.get(i).isClick) {
                        isValid = true;
                        break;
                    }
                }
                if (isValid) {
                    Utils.subjectId = selectedModel.id;
                    Intent intent = new Intent(getActivity(), KairosActivity.class);
                    // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    new FieldsValidator(getActivity()).customToast("Please select subject.");


                }
            }
        });


        initDialog(getActivity());
        if (isNetworkAvailable(getActivity())) {
            getCourseData();

        } else {
            setupData(new SessionManager_GLS(getActivity()).getSubject());
        }
    }

    public void clearData() {
        coursellist.clear();
        adapter.notifyDataSetChanged();
    }

    public void getCourseData() {
        clearData();
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("studentClass", courseID);
            json1.put("webservice", "1");

            jsonObject.put("task", "getClassSubjects");
            jsonObject.put("taskData", json1);
            Log.v("request : ", "getClassSubjects : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.v("res", "getClassSubjects " + response.body());
                dismissDialog();

                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        new SessionManager_GLS(getActivity()).setSubject(res);
                        setupData(res);


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("res", "error = " + t.toString());

            }
        });
    }

    public void setupData(String res) {
        try {

            JSONObject jsonObject = new JSONObject(res);

            if (jsonObject.getInt("status_code") == 200) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                Gson gson = new Gson();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    SubjectModel model = gson.fromJson(jsonObject1.toString(), SubjectModel.class);
                    coursellist.add(model);
                }

                adapter.notifyDataSetChanged();

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupRecylerview() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recylerview.setLayoutManager(layoutManager);
        adapter = new SubjectAdapter(getActivity(), coursellist, onclickCustomlistener);
        recylerview.setAdapter(adapter);

    }

    OnclickCustomlistener onclickCustomlistener = new OnclickCustomlistener() {
        @Override
        public void onclicklistener(int pos, String str) {

            selectedModel = coursellist.get(pos);

            for (int i = 0; i < coursellist.size(); i++) {
                coursellist.get(i).isClick = false;
            }
            coursellist.get(pos).isClick = true;
            adapter.notifyDataSetChanged();

        }
    };


}
