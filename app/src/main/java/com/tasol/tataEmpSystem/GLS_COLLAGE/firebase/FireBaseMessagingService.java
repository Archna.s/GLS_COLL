package com.tasol.tataEmpSystem.GLS_COLLAGE.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by tasol on 19/7/18.
 */

public class FireBaseMessagingService extends FirebaseMessagingService {
    String TAG="%%%FirebaseMEssagingService";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.v(TAG," FBS Messaging Service "+remoteMessage.getData());
    }
}
