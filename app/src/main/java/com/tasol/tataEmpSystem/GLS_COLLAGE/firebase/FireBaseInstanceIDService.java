package com.tasol.tataEmpSystem.GLS_COLLAGE.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by tasol on 19/7/18.
 */

public class FireBaseInstanceIDService extends FirebaseInstanceIdService {
    String TAG="%%%FirebaseInstance Service";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG,"Refresh "+refreshedToken);
    }
}
