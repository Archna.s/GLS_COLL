package com.tasol.tataEmpSystem.GLS_COLLAGE.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.CourseModel;
import com.tasol.tataEmpSystem.interfaces.OnclickCustomlistener;

import java.util.ArrayList;

/**
 * Created by user on 22/5/2017.
 */


public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.RecyclerViewHolders> {
    // 258 ,772
    //  private ArrayList<Integer> itemList = new ArrayList<>();
    private Context context;
    ArrayList<CourseModel> coursellist;
    OnclickCustomlistener onclickCustomlistener;

    public CourseAdapter(Context context, ArrayList<CourseModel> coursellist, OnclickCustomlistener onclickCustomlistener) {
        // this.itemList = itemList;
        this.context = context;
        this.coursellist = coursellist;
        this.onclickCustomlistener = onclickCustomlistener;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gls_row_course, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        //  holder.img.setImageResource(itemList.get(position));
        //  Glide.with(context).load(itemList.get(position)).centerCrop().into(holder.img);
        holder.txtCourseName.setText(coursellist.get(position).className);
        holder.lllayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onclickCustomlistener.onclicklistener(position, "");
            }
        });

        if (coursellist.get(position).isClick) {
            Glide.with(context).load(R.mipmap.radio_button_two).into(holder.ivRadio);
        } else {
            Glide.with(context).load(R.mipmap.radio_button).into(holder.ivRadio);

        }


    }

    @Override
    public int getItemCount() {
        //  return this.itemList.size();
        return coursellist.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder {
        TextView txtCourseName;
        ImageView ivRadio;
        LinearLayout lllayout;

        public RecyclerViewHolders(View itemView) {
            super(itemView);

            txtCourseName = (TextView) itemView.findViewById(R.id.txtCourseName);
            ivRadio = (ImageView) itemView.findViewById(R.id.ivRadio);
            lllayout = (LinearLayout) itemView.findViewById(R.id.lllayout);


            //  int newHeight = (150*itemList.get())


        }
    }

}