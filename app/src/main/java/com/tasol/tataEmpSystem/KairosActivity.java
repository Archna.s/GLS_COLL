package com.tasol.tataEmpSystem;


// {"ErrCode":3001,"Message":"API temporarily unavailable"}

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kairos.Kairos;
import com.kairos.KairosListener;
import com.tasol.tataEmpSystem.adapters.OfficelistAdapter;
import com.tasol.tataEmpSystem.cam.CameraSurfaceView;
import com.tasol.tataEmpSystem.cam.FaceDetectionCamera;
import com.tasol.tataEmpSystem.cam.FrontCameraRetriever;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.models.OfficeListModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.SessionManager;
import com.tasol.tataEmpSystem.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.filename;

public class KairosActivity extends BaseActivity implements FrontCameraRetriever.Listener, FaceDetectionCamera.Listener {


    KairosListener listener;
    Kairos myKairos;

    String onClickWhat = "";
    String onClickTechno = "";
    public static Activity activity;
    FrameLayout mainFramelayout;
    boolean isPhototaken = false;
    FaceDetectionCamera mFaceDetectionCamera;
    SurfaceView cameraSurface;
    private TextView helloWorldTextView, txtBottom;
    Bitmap finalBitmap = null;
    String galleryId = "neeltest1234";
    View viewDetectionOverlay;
    Animation mAnimation;
    ArrayList<OfficeListModel> officelist = new ArrayList<>();
    Spinner spinner;
    boolean isSelecteOffice = false;
    String getuserIDfromKairos = "";
    boolean isAnimating = false;
    boolean isDialogOpen = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kairos);

        setup();


    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private void setup() {

        initDialog(KairosActivity.this);
        viewDetectionOverlay = (View) findViewById(R.id.viewDetectionOverlay);
        FrontCameraRetriever.retrieveFor(this);
        mainFramelayout = ((FrameLayout) findViewById(R.id.camera));
        helloWorldTextView = (TextView) findViewById(R.id.helloWorldTextView);
        spinner = (android.widget.Spinner) findViewById(R.id.Spinner);
        txtBottom = (TextView) findViewById(R.id.txtBottom);
        activity = KairosActivity.this;

        if (location != null) {
            Log.e("555", "" + location.getLatitude() + "   " + location.getLongitude());
        }

        if (new SessionManager(KairosActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
            txtBottom.setVisibility(View.VISIBLE);
            dialogTraning();
        } else {
            isDialogOpen = false;
            txtBottom.setVisibility(View.GONE);
        }


        //startFaceRecognize();
    }

    public void animate() {
        viewDetectionOverlay.setVisibility(View.VISIBLE);
        mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        viewDetectionOverlay.setAnimation(mAnimation);
    }

    public void stopAnimate() {
        mAnimation.cancel();
    }

    public void startAnimate() {
        mAnimation.start();
    }


    private void startFaceRecognize() {
        //  startanimate();
        listener = new KairosListener() {

            @Override
            public void onSuccess(String response) {
                //{"Errors":[{"ErrCode":5002,"Message":"no faces found in the image"}]}
                Log.e("KAIROS DEMO", response);
                //    dismissDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("ErrCode")) {
                        reScanDialog();
                    } else {

                        if (onClickWhat.equals("recognition")) {
                            OnclickRecognition(response);
                        } else {

                            Gson gson = new Gson();
                            LoginModel model = new SessionManager(KairosActivity.this).getUserInfo();
                            model.istrain = "1";
                            new SessionManager(KairosActivity.this).setUserInfo(model);

                            OnclickRecognition(response);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFail(String response) {
                //     dismissDialog();

                Log.e("KAIROS DEMO error", response);
            }
        };


        myKairos = new Kairos();

        String app_id = "42cada49";
        String api_key = "0938911e303cba6fe43d0cb56d98be39";
        myKairos.setAuthentication(this, app_id, api_key);


    }

    @Override
    public void onFaceDetected() {
        Log.e("TTT", "isDialogOpen " + isDialogOpen);
        if (!isDialogOpen) {
            if (!isPhototaken) {

                if (!isAnimating) {
                    isAnimating = true;
                    animate();
                    viewDetectionOverlay.setVisibility(View.VISIBLE);
                }

                isPhototaken = true;
                Handler handler2 = new Handler();
                handler2.postDelayed(new Runnable() {
                    public void run() {
                        mFaceDetectionCamera.takePhoto();
                        //rescan.setVisibility(View.GONE);
                    }
                }, 2500);
            }
            helloWorldTextView.setText(R.string.face_detected_message);
        }
    }

    @Override
    public void onFaceTimedOut() {
        helloWorldTextView.setText(R.string.face_detected_then_lost_message);
        if (isAnimating) {
            isAnimating = false;
            //mAnimation.setBackgroundColor(Color.TRANSPARENT);
            //// mAnimation.cancel();
            viewDetectionOverlay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFaceDetectionNonRecoverableError() {
        helloWorldTextView.setText(R.string.error_with_face_detection);
        if (isAnimating) {
            isAnimating = false;
            mAnimation.setBackgroundColor(Color.TRANSPARENT);
            mAnimation.cancel();
            viewDetectionOverlay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaded(FaceDetectionCamera camera) {

        mFaceDetectionCamera = camera;
        Log.e("12345", "load");
        // When the front facing camera has been retrieved we still need to ensure our display is ready
        // so we will let the camera surface view initialise the camera i.e turn face detection on
        cameraSurface = new CameraSurfaceView(this, camera, this);
        // Add the surface view (i.e. camera preview to our layout)
        ((FrameLayout) findViewById(R.id.camera)).addView(cameraSurface);


    }

    @Override
    public void onFailedToLoadFaceDetectionCamera() {
        if (isAnimating) {
            isAnimating = false;
            mAnimation.setBackgroundColor(Color.TRANSPARENT);
            mAnimation.cancel();
            viewDetectionOverlay.setVisibility(View.GONE);
        }
        helloWorldTextView.setText(R.string.error_with_face_detection);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //when on Pause, release camera in order to be used from other applications
        //releaseCamera();
        mainFramelayout.removeView(cameraSurface);

    }

    public void startKairosApiActivity(Bitmap bp) {
        Intent i = new Intent(KairosActivity.this, KairosApiCallingActivity.class);
        Utils.finalBitmap = bp;
        startActivity(i);
        finish();

    }

    public void enrollkarios(final Bitmap bp) {


        Log.e("ttt", "call kairos api");
        finalBitmap = bp;
        // showDialog();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                Handler handler = new Handler();
                //  final Bitmap finalCompressedImageBitmap = compressedImageBitmap;
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Log.e("ttt", "reached in");

                        try {
                            onClickWhat = "recognition";
                            myKairos.recognize(bp, galleryId, null, null, null, null, listener);
                            //myKairos.enroll(finalBitmap, "neel test" + "%" + new SessionManager(KairosActivity.this).getUserInfo().id, galleryId, null, null, null, listener);

                        } catch (JSONException | UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e("ttt", "catch kairos" + e.toString());
                        }

                    }
                }, 1000);
            }
        }, 00);

    }

    private void OnclickRecognition(String response) {
        dismissDialog();
        try {
            JSONObject jsonObject = new JSONObject(response);

            Boolean isSuccess = jsonObject.has("images");

            if (isSuccess) {
                JSONArray jsonArray = jsonObject.optJSONArray("images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    JSONObject trasObj = c.getJSONObject("transaction");

                    String status = trasObj.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        String name = trasObj.getString("subject_id");
                        //   Toast.makeText(AndroidCameraExample.this, name, Toast.LENGTH_SHORT).show();

                        String str = name;
                        String[] split = str.split("%");
                        String firstSubString = split[0];
                        getuserIDfromKairos = split[1];

                        Log.e("890", "  full name : " + name);
                        Log.e("890", " k id : " + getuserIDfromKairos);
                        Log.e("890", " user id : " + new SessionManager(KairosActivity.this).getUserInfo().id);

                        if (new SessionManager(KairosActivity.this).getUserInfo().id.equalsIgnoreCase(getuserIDfromKairos)) {
                            //  storeLocationAttendance();
                            openDialog(name, true);

                        } else {
                            notAuthPrsonDialog();
                            //  storeLocationAttendance();
                        }


                    } else {
                        String name = trasObj.getString("message");
                        Toast.makeText(KairosActivity.this, name, Toast.LENGTH_SHORT).show();
                        if (new SessionManager(KairosActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
                            enrollDialog();
                        } else {
                            reScanDialog();
                        }
                    }
                }

            } else {
                JSONArray jsonArray = jsonObject.optJSONArray("Errors");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    String message = c.getString("Message");
                    openDialog(message, false);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void openDialog(String name, boolean isSuccess) {

        final AlertDialog alertDialog = new AlertDialog.Builder(KairosActivity.this).create();
        Log.e("098", "recog  name : " + name);

        alertDialog.setTitle("Success");

        if (name.contains("%")) {
            String str = name;
            String[] split = str.split("%");
            String firstSubString = split[0];
            getuserIDfromKairos = split[1];
            alertDialog.setMessage("Name : " + firstSubString);
        } else {
            alertDialog.setMessage("Name : " + name);

        }


        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                //finish();
                Intent i = new Intent(KairosActivity.this, DashboardActivity.class);
                //  startActivity(i);
                if (isSuccess) {


                    if (new SessionManager(KairosActivity.this).getUserInfo().id.equalsIgnoreCase(getuserIDfromKairos)) {
                        saveUserDetectedImage();

                    } else {
                        notAuthPrsonDialog();
                        //  storeLocationAttendance();
                    }


                } else {
                    isPhototaken = false;
                }
            }
        });
        alertDialog.show();
    }

    public void reScanDialog() {

        final AlertDialog alertDialog = new AlertDialog.Builder(KairosActivity.this).create();

        alertDialog.setTitle("Fail");
        alertDialog.setMessage("Please try again.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isPhototaken = false;
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    public void notAuthPrsonDialog() {

        final AlertDialog alertDialog = new AlertDialog.Builder(KairosActivity.this).create();

        alertDialog.setTitle("Fail");
        alertDialog.setMessage("You are not a authorize person.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isPhototaken = false;
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }


    public void successFinalDialog() {

        final AlertDialog alertDialog = new AlertDialog.Builder(KairosActivity.this).create();

        alertDialog.setTitle("Success");
        alertDialog.setMessage("Attendance has been taken successfully.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }


    public void enrollDialog() {
        Button btnOk, btnCancel;
        final EditText enName;
        final Dialog dialog = new Dialog(KairosActivity.this);

        dialog.setContentView(R.layout.enroll_dialog);
        dialog.setTitle("Enter Name");

        enName = (EditText) dialog.findViewById(R.id.enName);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                //   showDialog();
                enrollkarios("enroll", enName.getText().toString());


            }
        });


        dialog.show();


    }

    private void enrollkarios(final String str, final String name) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                Handler handler = new Handler();
                //   final Bitmap finalCompressedImageBitmap = compressedImageBitmap;
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Log.e("WWW", "reached in");

                        try {


                            // myKairos.recognize(s, galleryId, null, null, null, null, listener);

                            if (str.equals("recognition")) {
                                onClickWhat = "recognition";
                                myKairos.recognize(finalBitmap, galleryId, null, null, null, null, listener);
                            } else {
                                onClickWhat = "enroll";
                                Log.e("098", "enroll name : " + name + "%" + new SessionManager(KairosActivity.this).getUserInfo().id);
                                myKairos.enroll(finalBitmap, name + "%" + new SessionManager(KairosActivity.this).getUserInfo().id, galleryId, null, null, null, listener);

                            }
                        } catch (JSONException | UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e("WWW", "catch " + e.toString());
                        }

                    }
                }, 300);
            }
        }, 00);

    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    public void saveUserDetectedImage() {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", new SessionManager(KairosActivity.this).getUserInfo().id);
            json1.put("count", "1");
            json1.put("istrain", "1");
            json1.put("latitude", "" + location.getLatitude());
            json1.put("longitude", "" + location.getLongitude());

            jsonObject.put("task", "storeLocationAttendance");
            jsonObject.put("taskData", json1);
            Log.e("res ", "storeLocationAttendance  -  : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        File mediaStorageDir = getDir();
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        RequestBody req = createPartFromString("" + jsonObject);

        Call<ResponseBody> call = apiService.saveUserDetectedImage(req, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                if (response.body() != null) {
                    successFinalDialog();

                    try {
                        // String res = ;
                        String res = response.body().string();
                        Log.e("res", "storeLocationAttendance - " + res);

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {

                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }


                dismissDialog();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("storeLocationAttendance", "error = " + t.toString());

            }
        });
    }

    private void setupofficelist() {

        OfficelistAdapter adapter = new OfficelistAdapter(KairosActivity.this,
                R.layout.row_spinner_office, officelist);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {

                    isSelecteOffice = true;

                } else {
                    isSelecteOffice = false;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, FaceDetectionCamera.mainFoldername);
    }

    public void dialogTraning() {
        // Create custom dialog object

        final Dialog dialog = new Dialog(KairosActivity.this);
        dialog.setCancelable(false);


        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_traning);
        // Set dialog title

        // set values for custom dialog components - text, image and button
        Button btnOkay;
        TextView txtname, txtMsg;

        txtname = (TextView) dialog.findViewById(R.id.txtname);
        txtMsg = (TextView) dialog.findViewById(R.id.txtMsg);
        btnOkay = (Button) dialog.findViewById(R.id.btnOkay);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isDialogOpen = false;
                dialog.dismiss();
                onFaceDetected();

                //        FrontCameraRetriever.retrieveFor(activity);

            }
        });


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }



}
