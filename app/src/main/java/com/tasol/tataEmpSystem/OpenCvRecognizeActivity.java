package com.tasol.tataEmpSystem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.circularprogressbar.CircularProgressBar;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.IntPointer;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Point;
import org.bytedeco.javacpp.opencv_core.RectVector;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_face;
import org.bytedeco.javacpp.opencv_imgproc;

import java.io.File;

import static com.tasol.tataEmpSystem.TrainHelper.getCroppedImg;
import static org.bytedeco.javacpp.opencv_core.LINE_8;
import static org.bytedeco.javacpp.opencv_core.Mat;
import static org.bytedeco.javacpp.opencv_core.cvRound;
import static org.bytedeco.javacpp.opencv_face.createLBPHFaceRecognizer;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.circle;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.equalizeHist;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;
import static org.bytedeco.javacpp.opencv_imgproc.resize;
import static org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier;

/**
 * Created by djalmaafilho.
 */
public class  OpenCvRecognizeActivity extends Activity implements CvCameraPreview.CvCameraViewListener {
    public static final String TAG = "OpenCvRecognizeActivity";
    public static final String TRAIN_FOLDER = "train_folder";

    private CascadeClassifier faceDetector;
    private String[] nomes = {"", "Y Know You"};
    private int absoluteFaceSize = 0;
    private CvCameraPreview cameraView;
    boolean takePhoto;
    opencv_face.FaceRecognizer faceRecognizer = createLBPHFaceRecognizer();
    boolean trained;
    View viewDetectionOverlay;

    int knowmCount = 0;
    int unknowCount = 0;
    int totalCount = 0;
    Handler handler = new Handler();
    boolean isHanderRunning = false;
    boolean isMatchingFound = true;
    String MY_PREFS_NAME = "MY_PREFS_NAME";
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    TextView txtCount;
    boolean isRecognitionMore = true;
    public static Activity activity;
    static opencv_imgproc.CLAHE clahe = opencv_imgproc.createCLAHE();
    CircularProgressBar progressbar;


    Animation mAnimation;

    private boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);

    }

    public int dpToPx(int dp) {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opencv);
        activity = OpenCvRecognizeActivity.this;
        viewDetectionOverlay = (View) findViewById(R.id.viewDetectionOverlay);
        animate();
        txtCount = (TextView) findViewById(R.id.txtCount);
        progressbar = (CircularProgressBar) findViewById(R.id.progressbar);
        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        editor = prefs.edit();

        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
            }
        }

        cameraView = (CvCameraPreview) findViewById(R.id.camera_view);
        cameraView.setCvCameraViewListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //playvideo();
            }
        }, 1000);
        //  customDialogCamera();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    faceDetector = TrainHelper.loadClassifierCascade(OpenCvRecognizeActivity.this, R.raw.haarcascade_frontalface_alt2);
                    if (TrainHelper.isTrained(getBaseContext())) {
                        File folder = new File(getTrainDir(), TrainHelper.TRAIN_FOLDER);
                        File f = new File(folder, TrainHelper.LBPH_CLASSIFIER);
                        faceRecognizer.load(f.getAbsolutePath());
                        trained = true;
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.getLocalizedMessage(), e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                findViewById(R.id.btPhoto).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        takePhoto = true;
                    }
                });
                findViewById(R.id.btTrain).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        train();
                    }
                });
                findViewById(R.id.btReset).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            editor.clear();
                            editor.commit();
                            TrainHelper.reset(getBaseContext());
                            //Toast.makeText(getBaseContext(), "Reseted with sucess.", Toast.LENGTH_SHORT).show();
                            finish();
                        } catch (Exception e) {
                            Log.d(TAG, e.getLocalizedMessage(), e);
                        }
                    }
                });

                handler.postDelayed(runnable, 100);

                if (MainActivity.isFromTrain) {
                    //  customDialog();
                } else {
                    //  handler.postDelayed(runnable, 100);
                }
            }
        }.execute();
    }

    public void animate() {
        viewDetectionOverlay.setVisibility(View.VISIBLE);
        mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        viewDetectionOverlay.setAnimation(mAnimation);
    }

    public static int dpToPx(int dp, Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float) dp * density);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            takePhoto = true;
            handler.postDelayed(this, 100);
        }
    };


    private void setTrainTimer() {

    }

    @SuppressLint("StaticFieldLeak")
    void train() {
        int remainigPhotos = TrainHelper.PHOTOS_TRAIN_QTY - TrainHelper.qtdPhotos(getBaseContext());


        if (remainigPhotos > 0) {
            Toast.makeText(getBaseContext(), "You need more to call train: " + remainigPhotos, Toast.LENGTH_SHORT).show();
            return;
        } else if (TrainHelper.isTrained(getBaseContext())) {
            //Toast.makeText(getBaseContext(), "Already trained", Toast.LENGTH_SHORT).show();
            return;
        }

        //Toast.makeText(getBaseContext(), "Start train: ", Toast.LENGTH_SHORT).show();
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (!TrainHelper.isTrained(getBaseContext())) {
                        TrainHelper.train(getBaseContext());
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.getLocalizedMessage(), e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                try {

                    //   Toast.makeText(getBaseContext(), "Reseting after train - Sucess : " + TrainHelper.isTrained(getBaseContext()), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (Exception e) {
                    Log.d(TAG, e.getLocalizedMessage(), e);
                }
            }
        }.execute();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        absoluteFaceSize = (int) (width * 0.32f);
    }

    @Override
    public void onCameraViewStopped() {

    }

    private void capturePhoto(Mat rgbaMat) {
        try {
            TrainHelper.takePhoto(getBaseContext(), 1, TrainHelper.qtdPhotos(getBaseContext()) + 1, rgbaMat.clone(), faceDetector);
        } catch (Exception e) {
            e.printStackTrace();
        }
        takePhoto = false;
    }

    private void recognize(opencv_core.Rect dadosFace, Mat grayMat, Mat rgbaMat) {
        opencv_core.Rect corp = getCroppedImg(dadosFace);
        Mat detectedFace = new Mat(grayMat, dadosFace);
        // opencv_imgproc.threshold(detectedFace, detectedFace, 150, 255, opencv_imgproc.THRESH_OTSU);
        //    clahe.apply(detectedFace, detectedFace);

        clahe.apply(detectedFace, detectedFace);
        clahe.setClipLimit(5);
        clahe.collectGarbage();
        equalizeHist(detectedFace, detectedFace);

        //  Mat pre = tan_triggs_preprocessing(detectedFace);

        //  detectedFace = norm0255(pre);

        //
        //opencv_imgproc.threshold(detectedFace, detectedFace, 150, 255, opencv_imgproc.THRESH_OTSU);

        //  equalizeHist(detectedFace, detectedFace);

        resize(detectedFace, detectedFace, new Size(TrainHelper.IMG_SIZE_WIDTH, TrainHelper.IMG_SIZE_HEIGHT));

        IntPointer label = new IntPointer(1);
        DoublePointer reliability = new DoublePointer(1);
        faceRecognizer.predict(detectedFace, label, reliability);
        int prediction = label.get(0);
        double acceptanceLevel = reliability.get(0);
        String name;
        Log.e("RRR", " " + acceptanceLevel);
        // Log.e("RRR", "ACCEPT_LEVEL " + ACCEPT_LEVEL);
        // Log.e("RRR", "prediction " + prediction);
        //   Log.e("RRR", "=================================");
        if (prediction == -1 || acceptanceLevel >= TrainHelper.ACCEPT_LEVEL) {
            name = "Unknown Face";
            unknowCount = unknowCount + 1;

        } else {
            knowmCount = knowmCount + 1;

            Log.e("count ", "count : " + totalCount);


            name = prefs.getString("name", "No name defined");
            //   name = nomes[prediction] + " - " + acceptanceLevel;
        }
        totalCount = unknowCount + knowmCount;


        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                int per = (100 * knowmCount) / totalCount;
                //              txtCount.setText("ACCEPT LEVEL " + per + "%");
                //    txtCount.setText("ACCEPT LEVEL " + per + "%" );

                txtCount.setText("known : " + knowmCount + "  unknown : " + unknowCount + " Total : " + totalCount + "\n" + "ACCEPT LEVEL " + per + "%" + "\n  :" + acceptanceLevel);

                //int progress = TrainHelper.PHOTOS_TRAIN_QTY - remainigPhotos;
                progressbar.setMaximum(50);
                progressbar.setProgress(totalCount);


                if (totalCount == 50) {
                    isRecognitionMore = false;
                    Log.e("count ", "knowmCount : " + knowmCount);
                    Log.e("count ", "unknowCount : " + unknowCount);

                    Log.e("count ", "count : " + totalCount);
                    if (knowmCount > 20) {
                        dialogAuthentication("yes", "" + prefs.getString("name", "No name defined"), "Face found successfully.");
                    } else {
                        dialogAuthentication("no", "" + prefs.getString("name", "No name defined"), "No face matching found.");
                    }
                }
            }
        });
        int x = Math.max(dadosFace.tl().x() - 10, 0);
        int y = Math.max(dadosFace.tl().y() - 10, 0);
        // putText(rgbaMat, name, new Point(x, y), FONT_HERSHEY_PLAIN, 1.4, new opencv_core.Scalar(0, 255, 0, 0));
    }

    void showDetectedFace(RectVector faces, Mat rgbaMat) {
        int x = faces.get(0).x();
        int y = faces.get(0).y();
        int w = faces.get(0).width();
        int h = faces.get(0).height();
       /* int adjustment = 50;
        int x = faces.get(0).x()+adjustment;
        int y = faces.get(0).y()+adjustment;
        int w = faces.get(0).width()-adjustment;
        int h = faces.get(0).height()-adjustment;*/

        opencv_core.Point center = new opencv_core.Point(faces.get(0).x() + faces.get(0).width() / 2, faces.get(0).y() + faces.get(0).height() / 2);

        int radius = cvRound((faces.get(0).width() + faces.get(0).height()) * 0.25);

        circle(rgbaMat, center, radius, opencv_core.Scalar.GREEN, 2, LINE_8, 0);


        //rectangle(rgbaMat, new Point(x, y), new Point(x + w, y + h), opencv_core.Scalar.GREEN, 2, LINE_8, 0);
    }

    void showDetectedFaceArea(RectVector faces, Mat rgbaMat) {
        int x = faces.get(0).x();
        int y = faces.get(0).y();
        int w = faces.get(0).width();
        int h = faces.get(0).height();

        rectangle(rgbaMat, new Point(x, y), new Point(x + w, y + h), opencv_core.Scalar.BLUE, 2, LINE_8, 0);
    }


    void noTrainedLabel(opencv_core.Rect face, Mat rgbaMat) {
        int x = Math.max(face.tl().x() - 10, 0);
        int y = Math.max(face.tl().y() - 10, 0);
        //   putText(rgbaMat, "No trained or train unavailable", new Point(x, y), FONT_HERSHEY_PLAIN, 1.4, new opencv_core.Scalar(0, 255, 0, 0));
    }

    @Override
    public Mat onCameraFrame(Mat rgbaMat) {
        if (faceDetector != null) {
            Mat greyMat = new Mat(rgbaMat.rows(), rgbaMat.cols());
            cvtColor(rgbaMat, greyMat, CV_BGR2GRAY);
            //cvtColor(rgbaMat, greyMat, 40);

            RectVector faces = new RectVector();
            faceDetector.detectMultiScale(greyMat, faces, 1.25f, 3, 1,
                    new Size(absoluteFaceSize, absoluteFaceSize),
                    new Size(4 * absoluteFaceSize, 4 * absoluteFaceSize));

            if (faces.size() == 1) {
                showDetectedFace(faces, rgbaMat);
                if (takePhoto) {
                    capturePhoto(rgbaMat);
                    alertRemainingPhotos();
                }
                if (trained) {
                    if (isRecognitionMore) {
                        recognize(faces.get(0), greyMat, rgbaMat);
                    }
                } else {
                    noTrainedLabel(faces.get(0), rgbaMat);
                }
            }
            greyMat.release();
        }
        return rgbaMat;
    }

    void alertRemainingPhotos() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int remainigPhotos = TrainHelper.PHOTOS_TRAIN_QTY - TrainHelper.qtdPhotos(getBaseContext());
                if (remainigPhotos == 0) {
                    handler.removeCallbacks(runnable);
                    train();

                } else {
                    progressbar.setVisibility(View.VISIBLE);

                }
                int progress = TrainHelper.PHOTOS_TRAIN_QTY - remainigPhotos;
                progressbar.setMaximum(TrainHelper.PHOTOS_TRAIN_QTY);
                progressbar.setProgress(progress);
                //   Toast.makeText(getBaseContext(), "Ye to call train: " + remainigPhotos, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void open(String str, String message, String title) {

        runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(OpenCvRecognizeActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle(title);

                // Setting Dialog Message
                alertDialog.setMessage(message);

                // Setting Icon to Dialog

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke YES event
                        dialog.cancel();
                        Intent intent = new Intent(OpenCvRecognizeActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

                // Setting Negative "NO" Button

                // Showing Alert Message
                alertDialog.show();
            }
        });


    }


    public static File getTrainDir() {
        File directory = new File(Environment.getExternalStorageDirectory() + java.io.File.separator + "Neel Directory" + java.io.File.separator + TRAIN_FOLDER);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory;
    }


    public void dialogAuthentication(String str, String message, String title) {
        // Create custom dialog object

        final Dialog dialog = new Dialog(OpenCvRecognizeActivity.this);
        dialog.setCancelable(false);


        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_authentication_msg);
        // Set dialog title

        // set values for custom dialog components - text, image and button
        Button btnOkay;
        TextView txtname, txtMsg;

        txtname = (TextView) dialog.findViewById(R.id.txtname);
        txtMsg = (TextView) dialog.findViewById(R.id.txtMsg);
        btnOkay = (Button) dialog.findViewById(R.id.btnOkay);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Intent intent = new Intent(OpenCvRecognizeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        if (str.equalsIgnoreCase("yes")) {
            txtname.setText("Name : " + message + "");
            txtMsg.setText("Authentication : Success");

        } else {
            txtname.setText("Name : " + message + "");
            txtMsg.setText("Authentication : Failed");
            txtMsg.setTextColor(ContextCompat.getColor(OpenCvRecognizeActivity.this, R.color.red));
            txtname.setTextColor(ContextCompat.getColor(OpenCvRecognizeActivity.this, R.color.red));
        }


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


}
