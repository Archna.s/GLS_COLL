package com.tasol.tataEmpSystem.widgets;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.tasol.tataEmpSystem.KairosDashboardActivity;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.models.InfoWindowData;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    public Context context;

    public CustomInfoWindowGoogleMap(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.custom_window_marker, null);

        TextView punch_type = (TextView) view.findViewById(R.id.txt_punch_type);
        TextView punch_time = (TextView) view.findViewById(R.id.txt_punch_time);
        TextView punch_location = (TextView) view.findViewById(R.id.txt_punch_location);
        ImageView ivUserImage = (ImageView) view.findViewById(R.id.userImage);


//        Glide.with(context).load(infoWindowData.getImage()).into(ivUserImage);
        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        punch_type.setText(infoWindowData.getPunch_type());
        punch_time.setText(infoWindowData.getPunch_time());
        punch_location.setText(infoWindowData.getPunch_location());
        Picasso.with(context).load(infoWindowData.getStrImage()).into(ivUserImage);
        Picasso.with(context).setLoggingEnabled(true);

        return view;
    }
}
 