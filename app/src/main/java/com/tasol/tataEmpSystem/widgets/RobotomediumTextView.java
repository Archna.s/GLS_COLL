package com.tasol.tataEmpSystem.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class RobotomediumTextView extends android.support.v7.widget.AppCompatTextView{

    public RobotomediumTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public RobotomediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public RobotomediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/roboto.medium.ttf");
        setTypeface(customFont);
    }
}