package com.tasol.tataEmpSystem.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.models.AppConfigModel;
import com.tasol.tataEmpSystem.models.LoginModel;

import org.json.JSONObject;

public class OflineSessionManager {

    private SharedPreferences mPrefs;
    private String PREF_APP_CONFIG = "pref_app_config";
    private String PREF_getAttendance = "pref_getAttendance";


    public OflineSessionManager(Context mContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void setAppConfig(String jsonObject) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(jsonObject); // myObject - instance of MyObject
        e.putString(PREF_APP_CONFIG, json);
        e.apply();
    }

    public String getappConfig() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_APP_CONFIG, "");
        String model = gson.fromJson(json, String.class);
        return model;
    }


    public void setgetAttendance(String jsonObject) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(jsonObject); // myObject - instance of MyObject
        e.putString(PREF_getAttendance, json);
        e.apply();
    }

    public String getgetAttendance() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_getAttendance, "");
        String model = gson.fromJson(json, String.class);
        return model;
    }

    public void deleteAllSharePrefs() {
        SharedPreferences.Editor e = mPrefs.edit();
        e.clear().commit();
    }


}
