package com.tasol.tataEmpSystem.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.models.AppConfigModel;
import com.tasol.tataEmpSystem.models.LoginModel;

public class SessionManager {

    private SharedPreferences mPrefs;
    private String PREF_USER_INFO = "pref_user_info";
    private String PREF_AppConfig = "pref_appconfig";

    private String PREF_ISPUNCHIN = "PREF_ISPUNCHIN";
    private String PREF_ISLUNCHIN = "PREF_ISLUNCHIN";
    private String PREF_ISOTHERIN = "PREF_ISOTHERIN";


    public SessionManager(Context mContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void setUserInfo(LoginModel model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_USER_INFO, json);
        e.apply();
    }

    public LoginModel getUserInfo() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_USER_INFO, "");
        LoginModel model = gson.fromJson(json, LoginModel.class);
        return model;
    }

    public void deleteAllSharePrefs() {
        SharedPreferences.Editor e = mPrefs.edit();
        e.clear().commit();
    }

    public void setAppConfig(AppConfigModel model) {

        SharedPreferences.Editor e = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(model); // myObject - instance of MyObject
        e.putString(PREF_AppConfig, json);
        e.apply();
    }

    public AppConfigModel getAppConfig() {
        Gson gson = new Gson();
        String json = mPrefs.getString(PREF_AppConfig, "");
        AppConfigModel model = gson.fromJson(json, AppConfigModel.class);
        return model;
    }


    public void setPunchin(String ispunch) {
        SharedPreferences.Editor e = mPrefs.edit();
        e.putString(PREF_ISPUNCHIN, ispunch);
        e.apply();
    }

    public String getPunchin() {

        String ispunchin = mPrefs.getString(PREF_ISPUNCHIN, "");
        return ispunchin;
    }


    public void setLunchin(boolean ispunch) {
        SharedPreferences.Editor e = mPrefs.edit();
        e.putBoolean(PREF_ISLUNCHIN, ispunch);
        e.apply();
    }

    public boolean getLunchin() {

        Boolean ispunchin = mPrefs.getBoolean(PREF_ISLUNCHIN, false);
        return ispunchin;
    }

    public void setOtherin(boolean ispunch) {
        SharedPreferences.Editor e = mPrefs.edit();
        e.putBoolean(PREF_ISOTHERIN, ispunch);
        e.apply();
    }

    public boolean getOtherin() {

        Boolean ispunchin = mPrefs.getBoolean(PREF_ISOTHERIN, false);
        return ispunchin;
    }


}
