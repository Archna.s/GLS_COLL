package com.tasol.tataEmpSystem.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class GetMonthHelper extends DatabaseHelper {

    public static final String TABLE_NAME = "tb_getmonth";
    public static final String strdate = "strdate";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + strdate + " TEXT" + ")";

    public GetMonthHelper(Context context) {
        super(context);
    }


    public void insertHoliday(ArrayList<String> floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < floorlist.size(); i++) {
            values = new ContentValues();
            values.put(strdate, floorlist.get(i));

            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public ArrayList<String> getAllHistory() {
        ArrayList<String> list = new ArrayList<String>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                String obj = new String();
                obj = (cursor.getString(cursor.getColumnIndex("" + strdate + "")));
                list.add(obj);

            } while (cursor.moveToNext());
        }
        return list;
    }

    public void deleteHoliday() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "delete from " + TABLE_NAME + "";
        db.execSQL(query);
        db.close();
    }

}
