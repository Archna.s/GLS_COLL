package com.tasol.tataEmpSystem.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.TimetableSubModel;

import java.util.ArrayList;

public class ScheduleHelper extends DatabaseHelper {

    public static final String TABLE_NAME = "tb_schedule";
    public static final String id = "id";
    public static final String sectionId = "sectionId";
    public static final String subjectId = "subjectId";
    public static final String start = "start";
    public static final String end = "end";
    public static final String dayName = "dayName";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + id + " TEXT,"
                    + sectionId + " TEXT,"
                    + subjectId + " TEXT,"
                    + start + " TEXT,"
                    + end + " TEXT,"
                    + dayName + " TEXT" + ")";

    public ScheduleHelper(Context context) {
        super(context);
    }


    public void insertAllHistory(ArrayList<TimetableSubModel> floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < floorlist.size(); i++) {
            values = new ContentValues();
            values.put(id, floorlist.get(i).id);
            values.put(sectionId, floorlist.get(i).sectionId);
            values.put(subjectId, floorlist.get(i).subjectId);
            values.put(start, floorlist.get(i).start);
            values.put(end, floorlist.get(i).end);
            values.put(dayName, floorlist.get(i).dayName);

            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public ArrayList<TimetableSubModel> getAllHistory() {
        ArrayList<TimetableSubModel> list = new ArrayList<TimetableSubModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TimetableSubModel obj = new TimetableSubModel();
                obj.id = (cursor.getString(cursor.getColumnIndex("" + id + "")));
                obj.sectionId = (cursor.getString(cursor.getColumnIndex("" + sectionId + "")));
                obj.subjectId = (cursor.getString(cursor.getColumnIndex("" + subjectId + "")));
                obj.start = (cursor.getString(cursor.getColumnIndex("" + start + "")));
                obj.end = (cursor.getString(cursor.getColumnIndex("" + end + "")));
                obj.dayName = (cursor.getString(cursor.getColumnIndex("" + dayName + "")));

                list.add(obj);

            } while (cursor.moveToNext());
        }
        return list;
    }
    public void deleteSchedule() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "delete from " + TABLE_NAME + "";
        db.execSQL(query);
        db.close();
    }


}
