package com.tasol.tataEmpSystem.database.students;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.AttendanceModel;
import com.tasol.tataEmpSystem.database.DatabaseHelper;

import java.util.ArrayList;

public class StudentlistHelper extends DatabaseHelper {


    public static final String TABLE_NAME = "tb_std_list_attendance";
    public static final String name = "name";
    public static final String value = "value";
    public static final String percent = "percent";
    public static final String total_lec = "total_lec";
    public static final String advMonth = "advMonth";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + name + " TEXT,"
                    + value + " TEXT,"
                    + percent + " TEXT,"
                    + total_lec + " TEXT,"
                    + advMonth + " TEXT" + ")";

    public StudentlistHelper(Context context) {
        super(context);
    }


    public void insertAllHistory(ArrayList<AttendanceModel> floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < floorlist.size(); i++) {
            values = new ContentValues();
            values.put(name, floorlist.get(i).name);
            values.put(value, floorlist.get(i).value);
            values.put(percent, floorlist.get(i).percent);
            values.put(total_lec, floorlist.get(i).total_lec);
            values.put(advMonth, floorlist.get(i).advMonth);

            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public ArrayList<AttendanceModel> getAllHistory(String advMonth) {
        ArrayList<AttendanceModel> list = new ArrayList<AttendanceModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where advMonth = '" + advMonth + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                AttendanceModel obj = new AttendanceModel();
                obj.name = (cursor.getString(cursor.getColumnIndex("" + name + "")));
                obj.value = (cursor.getString(cursor.getColumnIndex("" + value + "")));
                obj.percent = (cursor.getString(cursor.getColumnIndex("" + percent + "")));
                obj.total_lec = (cursor.getString(cursor.getColumnIndex("" + total_lec + "")));
                obj.advMonth = (cursor.getString(cursor.getColumnIndex("" + this.advMonth + "")));

                list.add(obj);

            } while (cursor.moveToNext());
        }
        return list;
    }

    public void deletedata(String advMonth) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "delete from " + TABLE_NAME + " where advMonth = '" + advMonth + "'";
        db.execSQL(query);
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "delete from " + TABLE_NAME;
        db.execSQL(query);
        db.close();
    }


}
