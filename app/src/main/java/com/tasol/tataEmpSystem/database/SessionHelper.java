package com.tasol.tataEmpSystem.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.SessionModel;

import java.util.ArrayList;

public class SessionHelper extends DatabaseHelper {


    public static final String TABLE_NAME = "tb_session";

    public static final String totAttendance = "totAttendance";
    public static final String totStudents = "totStudents";
    public static final String className = "className";
    public static final String sectionName = "sectionName";
    public static final String subjectTitle = "subjectTitle";
    public static final String startTime = "startTime";
    public static final String endTime = "endTime";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + totAttendance + " TEXT,"
                    + totStudents + " TEXT,"
                    + className + " TEXT,"
                    + sectionName + " TEXT,"
                    + subjectTitle + " TEXT,"
                    + startTime + " TEXT,"
                    + endTime + " TEXT" + ")";

    public SessionHelper(Context context) {
        super(context);
    }


    public void insert(ArrayList<SessionModel> floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < floorlist.size(); i++) {
            values = new ContentValues();
            values.put(totAttendance, floorlist.get(i).totAttendance);
            values.put(totStudents, floorlist.get(i).totStudents);
            values.put(className, floorlist.get(i).className);
            values.put(sectionName, floorlist.get(i).sectionName);
            values.put(subjectTitle, floorlist.get(i).subjectTitle);
            values.put(startTime, floorlist.get(i).startTime);
            values.put(endTime, floorlist.get(i).endTime);

            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public ArrayList<SessionModel> getAllHistory() {
        ArrayList<SessionModel> list = new ArrayList<SessionModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SessionModel obj = new SessionModel();
                obj.totAttendance = (cursor.getString(cursor.getColumnIndex("" + totAttendance + "")));
                obj.totStudents = (cursor.getString(cursor.getColumnIndex("" + totStudents + "")));
                obj.className = (cursor.getString(cursor.getColumnIndex("" + className + "")));
                obj.sectionName = (cursor.getString(cursor.getColumnIndex("" + sectionName + "")));
                obj.subjectTitle = (cursor.getString(cursor.getColumnIndex("" + subjectTitle + "")));
                obj.startTime = (cursor.getString(cursor.getColumnIndex("" + startTime + "")));
                obj.endTime = (cursor.getString(cursor.getColumnIndex("" + endTime + "")));

                list.add(obj);

            } while (cursor.moveToNext());
        }
        return list;
    }

    public void deleteSchedule() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "delete from " + TABLE_NAME + "";
        db.execSQL(query);
        db.close();
    }


}
