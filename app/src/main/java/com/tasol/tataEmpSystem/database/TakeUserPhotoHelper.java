package com.tasol.tataEmpSystem.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.tasol.tataEmpSystem.models.TakeUserPhotoModel;

import java.util.ArrayList;

public class TakeUserPhotoHelper extends DatabaseHelper {

    public static final String TABLE_NAME = "userTakePhotos";

    public static final String id = "id";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String imagepath = "imagepath";
    public static final String timestamp = "timestamp";
    public static final String address = "address";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + id + " TEXT,"
                    + latitude + " TEXT,"
                    + longitude + " TEXT,"
                    + imagepath + " TEXT,"
                    + address + " TEXT,"
                    + timestamp + " TEXT" + ")";

    public TakeUserPhotoHelper(Context context) {
        super(context);
    }

    public void insertFloorDetails(TakeUserPhotoModel floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        // for (int i = 0; i < floorlist.size(); i++) {
        values = new ContentValues();
        values.put(id, floorlist.user_id);
        values.put(latitude, floorlist.latitude);
        values.put(longitude, floorlist.longitude);
        values.put(imagepath, floorlist.imagepath);
        // values.put(address, floorlist.get(i).);

        values.put(timestamp, floorlist.timestamp);
        db.insert(TABLE_NAME, null, values);
        // }
        db.close();
    }

    public ArrayList<TakeUserPhotoModel> getAllFloors() {
        ArrayList<TakeUserPhotoModel> list = new ArrayList<TakeUserPhotoModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TakeUserPhotoModel obj = new TakeUserPhotoModel();
                //   obj.id = (cursor.getString(cursor.getColumnIndex("" + id + "")));
                obj.latitude = (cursor.getString(cursor.getColumnIndex("" + latitude + "")));
                obj.longitude = (cursor.getString(cursor.getColumnIndex("" + longitude + "")));
                obj.imagepath = (cursor.getString(cursor.getColumnIndex("" + imagepath + "")));
                //  obj.address = (cursor.getString(cursor.getColumnIndex("" + address + "")));

                obj.timestamp = (cursor.getString(cursor.getColumnIndex("" + timestamp + "")));
                list.add(obj);
            } while (cursor.moveToNext());
        }
        return list;
    }
}
