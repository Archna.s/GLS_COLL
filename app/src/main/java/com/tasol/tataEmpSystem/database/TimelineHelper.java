package com.tasol.tataEmpSystem.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tasol.tataEmpSystem.models.TakeUserPhotoModel;
import com.tasol.tataEmpSystem.models.TimeLineModel;

import java.util.ArrayList;

public class TimelineHelper extends DatabaseHelper {

    public static final String TABLE_NAME = "tb_timeline";

    public static final String id = "id";
    public static final String officeId = "officeId";
    public static final String subjectId = "subjectId";
    public static final String date = "date";
    public static final String studentId = "studentId";
    public static final String status = "status";
    public static final String type = "type";
    public static final String checkdate = "checkdate";
    public static final String checkIntime = "checkIntime";
    public static final String latitude = "latitude";
    public static final String longitude = "longitude";
    public static final String remote = "remote";
    public static final String punch_type = "punch_type";
    public static final String face_image = "face_image";
    public static final String address = "address";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + id + " TEXT,"
                    + officeId + " TEXT,"
                    + subjectId + " TEXT,"
                    + date + " TEXT,"
                    + studentId + " TEXT,"
                    + status + " TEXT,"
                    + type + " TEXT,"
                    + checkdate + " TEXT,"
                    + checkIntime + " TEXT,"
                    + latitude + " TEXT,"
                    + longitude + " TEXT,"
                    + remote + " TEXT,"
                    + punch_type + " TEXT,"
                    + address + " TEXT,"
                    + face_image + " TEXT" + ")";

    public TimelineHelper(Context context) {
        super(context);
    }

    public void insertAllHistory(ArrayList<TimeLineModel> floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        for (int i = 0; i < floorlist.size(); i++) {
            values = new ContentValues();
            values.put(id, floorlist.get(i).id);
            values.put(officeId, floorlist.get(i).officeId);
            values.put(subjectId, floorlist.get(i).subjectId);
            values.put(date, floorlist.get(i).date);
            values.put(studentId, floorlist.get(i).studentId);
            values.put(status, floorlist.get(i).status);
            values.put(type, floorlist.get(i).type);
            values.put(checkdate, floorlist.get(i).checkdate);
            values.put(checkIntime, floorlist.get(i).checkIntime);
            values.put(latitude, floorlist.get(i).latitude);
            values.put(longitude, floorlist.get(i).longitude);
            values.put(remote, floorlist.get(i).remote);
            values.put(punch_type, floorlist.get(i).punch_type);
            values.put(face_image, floorlist.get(i).face_image);
            values.put(address, floorlist.get(i).address);
            db.insert(TABLE_NAME, null, values);
        }
        db.close();
    }

    public ArrayList<TimeLineModel> getAllHistory(String date) {
        ArrayList<TimeLineModel> list = new ArrayList<TimeLineModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where checkdate = '" + date + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TimeLineModel obj = new TimeLineModel();
                obj.id = (cursor.getString(cursor.getColumnIndex("" + id + "")));
                obj.officeId = (cursor.getString(cursor.getColumnIndex("" + officeId + "")));
                obj.subjectId = (cursor.getString(cursor.getColumnIndex("" + subjectId + "")));
                obj.date = (cursor.getString(cursor.getColumnIndex("" + this.date + "")));
                obj.studentId = (cursor.getString(cursor.getColumnIndex("" + studentId + "")));
                obj.status = (cursor.getString(cursor.getColumnIndex("" + status + "")));
                obj.type = (cursor.getString(cursor.getColumnIndex("" + type + "")));
                obj.checkdate = (cursor.getString(cursor.getColumnIndex("" + checkdate + "")));
                obj.checkIntime = (cursor.getString(cursor.getColumnIndex("" + checkIntime + "")));
                obj.latitude = (cursor.getString(cursor.getColumnIndex("" + latitude + "")));
                obj.longitude = (cursor.getString(cursor.getColumnIndex("" + longitude + "")));
                obj.remote = (cursor.getString(cursor.getColumnIndex("" + remote + "")));
                obj.punch_type = (cursor.getString(cursor.getColumnIndex("" + punch_type + "")));
                obj.face_image = (cursor.getString(cursor.getColumnIndex("" + face_image + "")));
                obj.address = (cursor.getString(cursor.getColumnIndex("" + address + "")));

                list.add(obj);

            } while (cursor.moveToNext());
        }
        return list;
    }

    public void deleteRecordForDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + " where checkdate = '" + date + "'"); //delete all rows in a table
        db.close();
    }
}
