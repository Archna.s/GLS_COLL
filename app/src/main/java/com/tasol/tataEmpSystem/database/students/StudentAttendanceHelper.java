package com.tasol.tataEmpSystem.database.students;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.tasol.tataEmpSystem.GLS_COLLAGE.models.StudentAttendanceModel;
import com.tasol.tataEmpSystem.database.DatabaseHelper;

import java.util.ArrayList;

public class StudentAttendanceHelper extends DatabaseHelper {

    public static final String TABLE_NAME = "tb_std_attendance";
    public static final String present_count = "present_count";
    public static final String absent_count = "absent_count";
    public static final String this_month = "this_month";
    public static final String start_date = "start_date";
    public static final String end_date = "end_date";

    public static final String CREATE_TABLE =
            "CREATE TABLE if not exists " + TABLE_NAME + "("
                    + present_count + " TEXT,"
                    + absent_count + " TEXT,"
                    + this_month + " TEXT,"
                    + start_date + " TEXT,"
                    + end_date + " TEXT" + ")";

    public StudentAttendanceHelper(Context context) {
        super(context);
    }


    public void insertAllHistory(StudentAttendanceModel floorlist) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values = new ContentValues();
        values.put(present_count, floorlist.present_count);
        values.put(absent_count, floorlist.absent_count);
        values.put(this_month, floorlist.this_month);
        values.put(start_date, floorlist.start_date);
        values.put(end_date, floorlist.end_date);

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public StudentAttendanceModel getAllHistory(String startDate) {
        StudentAttendanceModel obj = new StudentAttendanceModel();

        ArrayList<StudentAttendanceModel> list = new ArrayList<StudentAttendanceModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME + " where start_date = '" + startDate + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                obj.present_count = (cursor.getString(cursor.getColumnIndex("" + present_count + "")));
                obj.absent_count = (cursor.getString(cursor.getColumnIndex("" + absent_count + "")));
                obj.this_month = (cursor.getString(cursor.getColumnIndex("" + this_month + "")));
                obj.start_date = (cursor.getString(cursor.getColumnIndex("" + start_date + "")));
                obj.end_date = (cursor.getString(cursor.getColumnIndex("" + end_date + "")));


            } while (cursor.moveToNext());
        }
        return obj;
    }

    public void deleteSchedule() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "delete from " + TABLE_NAME + "";
        db.execSQL(query);
        db.close();
    }


}
