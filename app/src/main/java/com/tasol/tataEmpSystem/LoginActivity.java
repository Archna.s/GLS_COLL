package com.tasol.tataEmpSystem;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.models.AppConfigModel;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.ConnectionsUtils;
import com.tasol.tataEmpSystem.utils.FieldsValidator;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.filename;
import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.originalFilename;

public class LoginActivity extends BaseActivity {
    EditText edtMobileNumber;
    Button btnSigni, btnSignUp;
    private static final String TAGLOGIN = "%%%%LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initDialog(LoginActivity.this);
        btnSigni = (Button) findViewById(R.id.btnSigni);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        edtMobileNumber = (EditText) findViewById(R.id.edtMobileNumber);

        deleteImageFile();
        getGPSDetails();

        if (new SessionManager(LoginActivity.this).getAppConfig() != null) {
            AppConfigModel appConfigModel = new SessionManager(LoginActivity.this).getAppConfig();
            Log.e("TAG--", appConfigModel.AutoApprove);
            if (appConfigModel.AutoApprove.equalsIgnoreCase("1")) {
                btnSignUp.setVisibility(View.GONE);
            } else {
                btnSignUp.setVisibility(View.GONE);
            }
        }
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, NewUserActivity.class);
                startActivity(intent);
            }
        });

        btnSigni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobile = edtMobileNumber.getText().toString();
                if (mobile != null && mobile.length() == 10) {
                    if (ConnectionsUtils.isNetworkConnected(LoginActivity.this)) {
                        if (ConnectionsUtils.isInternetAvailable()) {
//                            getOtpTask(mobile);
                        } else {
                            getOtpTask(mobile);
//                            sendErrorMsg(getString(R.string.no_internet_connectivity));
                        }
                    } else {
                        //sendErrorMsg(getString(R.string.no_internet_connectivity));
                        new FieldsValidator(LoginActivity.this).customToast(getString(R.string.no_internet_connectivity));

                    }

                } else {
                    new FieldsValidator(LoginActivity.this).customToast("Invalid Mobile Number");

                    //   sendErrorMsg("Invalid Mobile Number");
                }
            }
        });
    }

    private void getGPSDetails() {

        // String lat = String.format("%.7f", ""+location.getLatitude());
        //String longi = String.format("%.7f", ""+location.getLongitude());


        // Log.e("uuu", "" + location.getLatitude() + "   " + location.getLongitude() + "   " + location.getAccuracy());
        //Log.e("uuu", "" + lat + "   " + longi + "   " + location.getAccuracy());

    }

    private void deleteImageFile() {

        File pictureFile = getOutputMediaFile();
        if (pictureFile.exists()) {
            pictureFile.delete();
        }
        File pictureTraningFile = getOutputMediaFileForTraning();
        if (pictureTraningFile.exists()) {
            pictureTraningFile.delete();
        }


    }

    private void sendErrorMsg(String s) {
        Toast.makeText(LoginActivity.this, "" + s, Toast.LENGTH_LONG).show();
    }

    public void getOtpTask(String mobile) {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("mobile_no", mobile);
//            json1.put("type", "teacher");
            //   json1.put("device_id", "duepU8iMuAY:APA91bGtxnccA9BwsHOm6w9WX69hH_JFxrw3ui0hEwUdtzVGi23_7B3FZvNqNCXCkdz3qldrgJy6lwmD7sWZZSTJJ9brYETgsRpcdCdxzBGMlQVEmlvHVKoUpYr5GAo6QP0m");
            // json1.put("device_type", "android");
            //json1.put("device_token", "forpush");
            jsonObject.put("task", "getOTP");
            jsonObject.put("taskData", json1);
            Log.e("res", "getOTP : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();


                if (response.body() != null) {
                    try {

                        String res = response.body().string();
                        Log.e("res", "login : " + res);
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                            JSONObject data = jsonObject.getJSONObject("data");
                            JSONObject user_detail = data.getJSONObject("user_detail");
                            String userID = user_detail.getString("id");
                            String otp = data.getString("otp");


                            Gson gson = new Gson();
                            LoginModel model = gson.fromJson(user_detail.toString(), LoginModel.class);
                            // model.istrain = "0";
                            new SessionManager(LoginActivity.this).setUserInfo(model);


                            Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                            intent.putExtra("otp", otp);
                            intent.putExtra("user_id", userID);
                            intent.putExtra("mobile", mobile);
                            startActivity(intent);
                            finish();
                        } else {
                            new FieldsValidator(LoginActivity.this).customToast(jsonObject.getString("message"));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissDialog();
                Log.e("res", "error = " + t.toString());

            }
        });
    }

    private boolean checkGpsServieEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        return gps_enabled;
    }

    private static File getOutputMediaFile() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename);

        return mediaFile;
    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, "CameraAPIDemo");
    }

    private static File getOutputMediaFileForTraning() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + originalFilename);

        return mediaFile;
    }


}
