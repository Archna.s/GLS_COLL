package com.tasol.tataEmpSystem;

import android.annotation.SuppressLint;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.tasol.tataEmpSystem.GLS_COLLAGE.fragments.CourseFragment;
import com.tasol.tataEmpSystem.beaconutils.LescanTracker;
import com.tasol.tataEmpSystem.database.students.StudentlistHelper;
import com.tasol.tataEmpSystem.fragments.MonthAttendaceFragment;
import com.tasol.tataEmpSystem.fragments.MonthWiseAttendanceChildeFragment;
import com.tasol.tataEmpSystem.fragments.SheduleFragment;
import com.tasol.tataEmpSystem.fragments.SlidingMenuListFragment;
import com.tasol.tataEmpSystem.fragments.StudentMonthAttendaceFragment;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends BaseActivity {

    //ActivityDashboardBinding mBinding;
    ImageView ivRight, ivDropDown;
    TextView txtTitle;
    LinearLayout llTitle;
    public SlidingMenu slidingMenu;
    ImageView imgLeft, txtTakeAttendance;
    LescanTracker lescanTracker;
    String currMajor, currMinor;
    public FloatingActionButton btnAddAttendace;
    StudentlistHelper studentlistHelper = new StudentlistHelper(DashboardActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        // mBinding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        initDialog(DashboardActivity.this);
        setup();
        startScanningForBeacons();

        //studentlistHelper.deleteAll();

        btnAddAttendace = (FloatingActionButton) findViewById(R.id.btn_add_attendance);
        btnAddAttendace.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(DashboardActivity.this, KairosActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();*/
                replaceFragment(new CourseFragment(), true);

            }
        });
        Log.e("4567", "photo : " + new SessionManager(DashboardActivity.this).getUserInfo().photo);


//        if (new SessionManager(DashboardActivity.this).getUserInfo().role.equalsIgnoreCase("student")) {
//
//            Intent intent = new Intent(DashboardActivity.this, KairosActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//            finish();
//
//        } else {
//            Bundle bundle = getIntent().getExtras();
//            if (bundle != null) {
//                if (bundle.getString("whatOpen", "").equalsIgnoreCase("mainDashboard")) {
//                    replaceFragment(new MonthAttendaceFragment(), false);
//                } else {
//                    replaceFragment(new CourseFragment(), false);
//                }
//            } else {
//                replaceFragment(new CourseFragment(), false);
//            }
//        }

        if (new SessionManager(DashboardActivity.this).getUserInfo().role.equalsIgnoreCase("student")) {

//            Intent intent = new Intent(DashboardActivity.this, KairosActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//            finish();
            replaceFragment(new StudentMonthAttendaceFragment(), false);
//            Bundle bundle = getIntent().getExtras();
//            if (bundle != null) {
//                if (bundle.getString("whatOpen", "").equalsIgnoreCase("mainDashboard")) {
//
//                } else {
//                    replaceFragment(new CourseFragment(), false);
//                }
//            } else {
//                replaceFragment(new CourseFragment(), false);
//            }

        } else {
            btnAddAttendace.setVisibility(View.GONE);
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                if (bundle.getString("whatOpen", "").equalsIgnoreCase("mainDashboard")) {
                    clearBackStack();
                    replaceFragment(new MonthWiseAttendanceChildeFragment(), false);
                } else {
                    replaceFragment(new MonthWiseAttendanceChildeFragment(), false);
                }
            } else {
                replaceFragment(new MonthWiseAttendanceChildeFragment(), false);
            }
        }



       /* if (TextUtils.isEmpty(new SessionManager(DashboardActivity.this).getUserInfo().photo) || new SessionManager(DashboardActivity.this).getUserInfo().photo.equalsIgnoreCase("null")) {
            getuserProfile();
            SlidingMenuListFragment.loadProfilePhoto();
        }


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getString("whatOpen", "").equalsIgnoreCase("mainDashboard")) {
                replaceFragment(new MonthWiseAttendanceChildeFragment(), false);

            } else {
                replaceFragment(new MonthWiseAttendanceChildeFragment(), false);
            }

        } else {
            replaceFragment(new MonthWiseAttendanceChildeFragment(), false);
        }
*/
    }

    public double getLat() {
        return location.getLatitude();
    }

    public double getLong() {
        return location.getLongitude();
    }


    private void setup() {

        ivRight = (ImageView) findViewById(R.id.ivRight);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        llTitle = (LinearLayout) findViewById(R.id.llTitle);
        ivDropDown = (ImageView) findViewById(R.id.ivDropDown);
        imgLeft = (ImageView) findViewById(R.id.imgLeft);
        txtTakeAttendance = (ImageView) findViewById(R.id.txtTakeAttendance);

        txtTakeAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getCurrentFragment() instanceof TakeAttendanceActivity) {

                } else {
                    replaceFragment(new TakeAttendanceActivity(), true);

                }
            }
        });

        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // replaceFragment(new SheduleFragment(), true);
                if (getCurrentFragment() instanceof MonthAttendaceFragment) {
                    ((MonthAttendaceFragment) getCurrentFragment()).replaceSchedule();
                }

            }
        });

        setupSlider2();
    }

    public void rightImageVisibility(int visible) {
        // ivRight.setVisibility(visible);
    }

    public void setTitle(String title) {
        txtTitle.setText(title);
    }

    public void isVisibleivDrop(int visible) {
        ivDropDown.setVisibility(visible);
    }

    public void llTitleOnClicklistener(String str, Boolean isDown) {

        llTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (str.equalsIgnoreCase("dropdown")) {
                    if (getCurrentFragment() instanceof SheduleFragment) {
                        ((SheduleFragment) getCurrentFragment()).expCollCalender();
                    }
                }
            }
        });
        ivDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (str.equalsIgnoreCase("dropdown")) {
                    if (getCurrentFragment() instanceof SheduleFragment) {
                        ((SheduleFragment) getCurrentFragment()).expCollCalender();
                    }
                }
            }
        });


    }

    public void setivDropDown(int res) {
        ivDropDown.setImageResource(res);
    }

    private void setupSlider2() {

        slidingMenu = new SlidingMenu(DashboardActivity.this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        // slidingMenu.setShadowDrawable(R.drawable.common_plus_signin_btn_text_light_pressed);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.0f);
        slidingMenu.attachToActivity(DashboardActivity.this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.sliding_menu);
        slidingMenu.setSlidingEnabled(false);


        slidingMenu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {
            @Override
            public void onOpened() {
                Log.e("333", "slioder opne");
                View view = getCurrentFocus();
                SlidingMenuListFragment.loadProfilePhoto();

                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                }


            }
        });

        slidingMenu.setOnCloseListener(new SlidingMenu.OnCloseListener() {
            @Override
            public void onClose() {
            }
        });

        imgLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slidingMenu.toggle();
            }
        });

    }

    public void toggelSlider()

    {
        slidingMenu.toggle();
    }


    private void getuserProfile() {
//        initDialog(getActivity());
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", new SessionManager(DashboardActivity.this).getUserInfo().id);
            jsonObject.put("task", "myProfile");
            jsonObject.put("taskData", json1);
            Log.e("getDashboa res", "myProfile : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                Log.e("getDash res", "data  body" + response.body());
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {
                            JSONObject dataobject = jsonObject.getJSONObject("data");
                            new SessionManager(DashboardActivity.this).getUserInfo().photo = dataobject.getString("photo");

                            LoginModel model = new SessionManager(DashboardActivity.this).getUserInfo();
                            model.photo = dataobject.getString("photo");
                            new SessionManager(DashboardActivity.this).setUserInfo(model);
                            SlidingMenuListFragment.loadProfilePhoto();

                            //   ((SlidingMenuListFragment) getCurrentFragment()).loadProfilePhoto();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissDialog();
                Log.e("getDashboar res", "error = " + t.toString());

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startScanningForBeacons() {
        //Lescanner initiated here
        lescanTracker = LescanTracker.getInstance(DashboardActivity.this);
        lescanTracker.initManagersAndAdapters();
        startTakingAttendance();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void startTakingAttendance() {
        lescanTracker.startScanning();
        //Student Beacons Data
        getNeaestBeaconData();
//        stopScanTimer();
    }

    private void getNeaestBeaconData() {
        lescanTracker.setLescanListener(new LescanTracker.LescanListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onStudentDetected(ScanResult detectedStudents) {
                Log.d("@@@TTT", detectedStudents.getDevice().getAddress());
//                String uuId = new SessionManager(DashboardActivity.this).getAppConfig().uuid;
//                if (uuId.equalsIgnoreCase(detectedStudents.getDevice().getAddress()){
//
//                }
                currMajor = String.valueOf((detectedStudents.getScanRecord().getBytes()[25] & 0xff) * 0x100 + (detectedStudents.getScanRecord().getBytes()[26] & 0xff));
                currMinor = String.valueOf((detectedStudents.getScanRecord().getBytes()[27] & 0xff) * 0x100 + (detectedStudents.getScanRecord().getBytes()[28] & 0xff));
                Log.d("@@@TTT", currMajor + " ---" + currMinor);
            }
        });
    }

    public String getCurrMajor() {
        if (TextUtils.isEmpty(currMajor)) {
            currMajor = "258";
        }
        return currMajor;
    }

    public String getCurrMinor() {
        if (TextUtils.isEmpty(currMinor)) {
            currMinor = "772";
        }
        return currMinor;
    }


}
