package com.tasol.tataEmpSystem.beaconutils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

/**
 * Created by tasol on 22/12/17.
 */

public class LescanTracker {

    private Context mContext;
    private static LescanTracker lescanTracker;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private SampleScanCallback mScanCallback;
    private List<ScanResult> resultList = new ArrayList<>();
    private String TAG = "@@@TTT LESCAN : ";
    private Timer timer;
    private long delay = 100; // delay for x sec.
    private long period = 1000; // repeat every x sec.
    List<ScanResult> sortedResultList = new ArrayList<>();
    ScanResult nearestBeacon;
    double nearestBeaconDistance;

    public static LescanTracker getInstance(Context mContext) {
        if (lescanTracker == null) {
            lescanTracker = new LescanTracker();
            lescanTracker.mContext = mContext;
        }
        lescanTracker.mContext = mContext;
        return lescanTracker;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initManagersAndAdapters() {
        //Always init this method before scanning for beacons
        int apiVersion = android.os.Build.VERSION.SDK_INT;
        if (apiVersion > android.os.Build.VERSION_CODES.KITKAT) {
            mBluetoothAdapter = ((BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE))
                    .getAdapter();
            mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void startScanning() {
        Log.v(TAG, "Scanning Started");
        if (timer == null) {
            timer = new Timer();
            activateBluetoothScan();
            startFixTimer(delay, period);
        } else {
            activateBluetoothScan();
            startFixTimer(delay, period);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void stopScanning() {
        Log.v(TAG, "Scanning Stopped");
        if (mBluetoothLeScanner != null && mScanCallback != null
                ) {
            mBluetoothLeScanner.stopScan(mScanCallback);
            mScanCallback = null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void activateBluetoothScan() {
        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            if (!mBluetoothAdapter.isEnabled()) {
                //if bluetooth is not on
                mBluetoothAdapter.enable();
                if (mScanCallback == null) {
                    mScanCallback = new SampleScanCallback();
                    mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
                }
            } else {// if bluetooth on then check if location is on and app has permission or not
                if (mScanCallback == null) {
                    mScanCallback = new SampleScanCallback();
                    mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
                }
            }
        } catch (Exception je) {
            je.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private class SampleScanCallback extends ScanCallback {
        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            Log.d("@@@BatchResult", results.toString());
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            Log.d("@@@ScanResult", result.toString());
            resultList.add(result);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Toast.makeText(mContext, "Scan failed with error: " + errorCode, Toast.LENGTH_LONG)
                    .show();
        }
    }

    public double calculateDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }
        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            return (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
        }
    }


    /**
     * Return a List of {@link ScanFilter} objects to filter by Service UUID.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private List<ScanFilter> buildScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();

        ScanFilter.Builder builder = new ScanFilter.Builder();
        // Comment out the below line to see all BLE devices around you
        scanFilters.add(builder.build());

        return scanFilters;
    }

    /**
     * Return a {@link ScanSettings} object set to use low power (to preserve battery life).
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        return builder.build();
    }

    private void startFixTimer(long delay, long period) {

        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                try {
                    stopScanning();
                    List<ScanResult> tempResult = new ArrayList<>(resultList);

                    sortedResultList = ScanResultUtils.getSortedResult(resultList);

                    if (sortedResultList != null && sortedResultList.size() > 0) {
//                        Log.v(TAGSORTED," Sorted");
                        for (int i = 0; i < sortedResultList.size(); i++) {
                            ScanResult row = sortedResultList.get(i);
                            int txPower = (int) row.getScanRecord().getBytes()[29];
                            double rssi = ((double) row.getRssi());
                            final String macID = row.getDevice().getAddress();
                            final double currDist = calculateDistance(txPower, rssi);
                            if (i == sortedResultList.size() - 1) {
                                nearestBeacon = row;
                                nearestBeaconDistance = currDist;
                            }
                        }
                    }

                    if (tempResult.size() > 0) {
                        if (getLescanListener() != null) {
                            getLescanListener().onStudentDetected(nearestBeacon);
                        }
                    }
                    sortedResultList.clear();
                    resultList.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 5000);
    }


    public interface LescanListener {
        void onStudentDetected(ScanResult detectedStudents);
    }

    public LescanListener getLescanListener() {
        return lescanListener;
    }

    public void setLescanListener(LescanListener lescanListener) {
        this.lescanListener = lescanListener;
    }

    private LescanListener lescanListener;

}
