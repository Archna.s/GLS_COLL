package com.tasol.tataEmpSystem.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mikesu.horizontalexpcalendar.HorizontalExpCalendar;
import com.mikesu.horizontalexpcalendar.common.Config;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.TimetableSubModel;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.adapters.ScheduleAdapter;
import com.tasol.tataEmpSystem.database.students.StudentScheduleHelper;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//
public class StudentSheduleFragment extends BaseFragment {

    private static final String TAGMONTH = "12445";
    RecyclerView recylerview;
    //    HorizontalCalendar horizontalCalendar;
//    Spinner spinner;
//    boolean isFirstTime = true;
    ArrayList<String> monthlist = new ArrayList<>();
    //    ArrayList<String> allmonthlist = new ArrayList<>();
    private HorizontalExpCalendar horizontalExpCalendar;
    //    private static final String TAGMONTH = "%%%%ScheduleFrag";

    ArrayList<TimetableSubModel> subjectlist = new ArrayList<>();
    ArrayList<TimetableSubModel> filterSubjectlist = new ArrayList<>();
    ScheduleAdapter adapter;
    TextView txtMessage;
    Boolean isDown = true;
    String semName = "";
    StudentScheduleHelper studentScheduleHelper;
    /// ArrayList<ArrayList<TimetableSubModel>> dayslistWithsubject = new ArrayList<>();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        studentScheduleHelper = new StudentScheduleHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        Log.v(TAGMONTH," ScheduleFrag Called : ");
        View view = inflater.inflate(R.layout.fragment_shedule, container, false);

        initDialog(getActivity());
        setup(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupToolbar();
    }

    @Override
    public void onPause() {
        super.onPause();
        ((DashboardActivity) getActivity()).isVisibleivDrop(View.GONE);
    }

    private void setupToolbar() {

        ((DashboardActivity) getActivity()).rightImageVisibility(View.GONE);
        ((DashboardActivity) getActivity()).setTitle("Schedule");
        ((DashboardActivity) getActivity()).isVisibleivDrop(View.VISIBLE);
        ((DashboardActivity) getActivity()).llTitleOnClicklistener("dropdown", isDown);


    }

    private void setup(View view) {

        recylerview = (RecyclerView) view.findViewById(R.id.recylerview);
        txtMessage = (TextView) view.findViewById(R.id.txtMessage);
//        spinner = (Spinner) view.findViewById(R.id.spinner);

        Bundle args = getArguments();
        if (args != null) {

            monthlist = (ArrayList<String>) args.getSerializable("monthlist");
        }

        //  addAllmonthlist();
        setupRecylerview();
//        setDates(view, true);
//        setupDropDown();


        horizontalExpCalendar = (HorizontalExpCalendar) view.findViewById(R.id.calendar);

        horizontalExpCalendar.setHorizontalExpCalListener(new HorizontalExpCalendar.HorizontalExpCalListener() {
            @Override
            public void onCalendarScroll(DateTime dateTime) {
                Log.i("TAG", "onCalendarScroll: " + dateTime.toString());

                if (dateTime.getMonthOfYear() > 8) {

                }
            }

            @Override
            public void onDateSelected(DateTime dateTime) {
                if (horizontalExpCalendar.isExpanded()) {
                    horizontalExpCalendar.collapse();
                }
                getWeekDaylist(dateTime.toString());
                //  Log.e("TAG", "onDateSelected: " + dateTime.toString() + " date  : " + getWeekDaylist(dateTime.toString()));
            }

            @Override
            public void onChangeViewPager(Config.ViewPagerType viewPagerType) {
                Log.e("TAG", "onChangeViewPager: " + viewPagerType.name());
            }
        });
    }


    public void expCollCalender() {
        if (horizontalExpCalendar.isExpanded()) {
            horizontalExpCalendar.collapse();
            ((DashboardActivity) getActivity()).setivDropDown(R.mipmap.down_arrow);
        } else {
            ((DashboardActivity) getActivity()).setivDropDown(R.mipmap.up_arrow);

            horizontalExpCalendar.expand();
        }
    }

    Calendar startDate = Calendar.getInstance();
//        Calendar endDate = Calendar.getInstance();
//
//
//        if (isCurrent) {
//            int year = startDate.get(Calendar.YEAR);
//            int month = startDate.get(Calendar.MONTH);
//            int day = 1;
//            startDate.set(year, month, day);
//
//            LocalDate lastDayOfMonth = new LocalDate(year, month + 1, 1).dayOfMonth().withMaximumValue();
//
//////    private void addAllmonthlist() {
//////
//////        allmonthlist.add("January");
//////        allmonthlist.add("February");
//////        allmonthlist.add("March");
//////        allmonthlist.add("April");
//////        allmonthlist.add("May");
//////        allmonthlist.add("June");
//////        allmonthlist.add("July");
//////        allmonthlist.add("August");
//////        allmonthlist.add("September");
//////        allmonthlist.add("October");
//////        allmonthlist.add("November");
//////        allmonthlist.add("December");
//////    }
////
//////    private void setDates(View view, boolean isCurrent) {
//////          int lastDay = Integer.parseInt(lastDayOfMonth.toString("dd"));
//            String lastDay1 = (lastDayOfMonth.toString("MM/dd/yyyy"));
//            Log.e("1111", "last day 123 = " + lastDay1);
//
//            endDate.set(year, month, lastDay);
//
//
//        } else {
//        }
////        setupCalander(view, startDate, endDate);
//
//    }

//    private void setupDropDown() {
//
//        ArrayList<String> categories = new ArrayList<String>();
//        categories.add("All");
//        categories.add("April");
//        categories.add("May");
//        categories.add("June");
//        categories.add("July");
//
//      /*  ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(dataAdapter);
//*/
//        CustomArrayAdapter adapter = new CustomArrayAdapter(getActivity(),
//                R.layout.row_spinner_month, monthlist);
//        spinner.setAdapter(adapter);
//
//
////        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
////            @Override
////            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
////
////                if (!isFirstTime) {
////
////                    Calendar startDate = Calendar.getInstance();
////                    Calendar endDate = Calendar.getInstance();
////
////                    int selectedMonth = 0;
////                    for (int j = 0; l < allmonthlist.size(); j++) {
////                        if (monthlist.get(i).contains(allmonthlist.get(j))) {
////                            selectedMonth = j;
////                            break;
////                        }
////                    }
////                    Log.e("222", "selected month  : " + selectedMonth);
////
////                    int year = startDate.get(Calendar.YEAR);
////                    int month = selectedMonth;
////                    int day = 1;
////
////                    LocalDate lastDayOfMonth = new LocalDate(year, month + 1, 1).dayOfMonth().withMaximumValue();
////                    int lastDay = Integer.parseInt(lastDayOfMonth.toString("dd"));
////                    String lastDay1 = (lastDayOfMonth.toString("MM/dd"));
////                    Log.e("1111", "last day  = " + lastDay1);
////
////                    startDate.set(year, month, day);
////                    endDate.set(year, month, lastDay);
////
//////                    setupCalander(view, startDate, endDate);
//////
//////                    horizontalCalendar.setRange(startDate, endDate);
//////                    horizontalCalendar.refresh();
////
////                }
////                isFirstTime = false;
////            }
////
////            @Override
////            public void onNothingSelected(AdapterView<?> adapterView) {
////
////            }
////        });
//
//    }

//    public void performSpinnerClick() {
//
//        spinner.performClick();
//
//
//    }

//    private void setupCalander(View view, Calendar startDate, Calendar endDate) {
//
////       Calendar startDate = Calendar.getInstance();
////        startDate.set(Calendar.FEBRUARY, -1);
////
////
////        Calendar endDate = Calendar.getInstance();
////        endDate.set(Calendar.MARCH, 1);
//
//        final Calendar defaultSelectedDate = Calendar.getInstance();
//
//
//         int numOfDaysInMonth = startDate.getActualMaximum(Calendar.DAY_OF_MONTH);
//           System.out.println("First Day of month: " + startDate.getTime());
//
//
////        horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
////                .range(startDate, endDate)
////                .datesNumberOnScreen(7)
////                .configure()
//////                // .formatTopText("MMM")
////                .formatMiddleText("dd")
////                .formatBottomText("EEE")
////                .sizeTopText(0)
////                .showTopText(true)
////                .showBottomText(true)
////                .textColor(Color.LTGRAY, Color.WHITE)
////                .colorTextMiddle(Color.LTGRAY, Color.parseColor("#ffffff"))
////                .end()
//////                // .defaultSelectedDate(defaultSelectedDate)
//////                /*.addEvents(new CalendarEventsPredicate() {
//////
//////                    Random rnd = new Random();
//////
//////                    @Override
//////                    public List<CalendarEvent> events(Calendar date) {
//////                        List<CalendarEvent> events = new ArrayList<>();
//////                        int count = rnd.nextInt(6);
//////
//////                        for (int i = 0; i <= count; i++) {
//////                            events.add(new CalendarEvent(Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)), "event"));
//////                        }
//////
//////                        return events;
//////                    }
//////                })*/
////                .build();
//
//        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
//            @Override
//            public void onDateSelected(Calendar date, int position) {
//                Toast.makeText(getContext(), DateFormat.format("EEE, MMM d, yyyy", date) + " is selected!", Toast.LENGTH_SHORT).show();
//
//                Calendar startDate = Calendar.getInstance();
//                Calendar endDate = Calendar.getInstance();
//
//
//                Log.e("AAA", DateFormat.format("dd", date) + "");
//                Log.e("AAA", DateFormat.format("MM", date) + "");
//                Log.e("AAA", DateFormat.format("yyyy", date) + "");
//
//                int year = Integer.parseInt(DateFormat.format("yyyy", date) + "");
//                int month = Integer.parseInt(DateFormat.format("MM", date) + "");
//                int day = Integer.parseInt(DateFormat.format("dd", date) + "");
//                startDate.set(year, month, day);
//                endDate.add(month, 1);
//
//                 setupCalander(view, startDate, endDate);
//
//
//            }
//
//        });
//
//    }


    private void setupRecylerview() {


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recylerview.setLayoutManager(layoutManager);
        adapter = new ScheduleAdapter(getActivity(), filterSubjectlist);
        recylerview.setAdapter(adapter);

        if (isNetworkAvailable(getActivity())) {
            getListAttendance();
        } else {

            subjectlist.addAll(studentScheduleHelper.getAllHistory());
            Date date = Calendar.getInstance().getTime();
            System.out.println("Current time => " + date);
            getWeekDaylist(getDateTime());
        }
    }

    private void getListAttendance() {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("student_id", new SessionManager(getActivity()).getUserInfo().id);


            jsonObject.put("task", "getTimeTable");
            jsonObject.put("taskData", json1);
            Log.v(TAGMONTH, "Req Obj : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        Log.e("124", "res : getTimeTable  " + res);
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status_code") == 200) {
                            Gson gson = new Gson();
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject c = jsonArray.getJSONObject(i);
                                JSONObject jsonObjectTimeTable = c.getJSONObject("timeTable");
                                semName = c.getString("sectionName");

                                for (int j = 1; j < (jsonObjectTimeTable.length() + 1); j++) {

                                    JSONObject jsonObjectTime = jsonObjectTimeTable.getJSONObject("" + j);
                                    String dayName = jsonObjectTime.getString("dayName");
                                    Log.e("jjjjjjj === ", "" + j + "   dayname  :  " + dayName);

                                    if (jsonObjectTime.has("sub")) {
                                        //subjectlist.clear();
                                        JSONArray jsonArraySub = jsonObjectTime.getJSONArray("sub");
                                        for (int k = 0; k < jsonArraySub.length(); k++) {
                                            JSONObject jsonObject1 = jsonArraySub.getJSONObject(k);
                                            TimetableSubModel model = new TimetableSubModel();
                                            model = gson.fromJson(jsonObject1.toString(), TimetableSubModel.class);
                                            model.dayName = dayName;
                                            subjectlist.add(model);

                                        }

                                    }


                                }

                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");


                                Date date = Calendar.getInstance().getTime();
                                System.out.println("Current time => " + date);
                                getWeekDaylist(getDateTime());


                                Log.e("dayslist = ", "size : " + subjectlist.size());


                            }
                            studentScheduleHelper.deleteSchedule();
                            studentScheduleHelper.insertAllHistory(subjectlist);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());
                dismissDialog();
            }
        });
    }

    private String getDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

    public void getWeekDaylist(String inputDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");

        Date date = null;
        try {
            date = simpleDateFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) {
            return;
        }

        SimpleDateFormat convetDateFormat = new SimpleDateFormat("EEEE");

        String weekName = convetDateFormat.format(date);

        filterSubjectlist.clear();
        for (int i = 0; i < subjectlist.size(); i++) {
            if (weekName.equalsIgnoreCase(subjectlist.get(i).dayName)) {
                filterSubjectlist.add(subjectlist.get(i));
            }
        }
        if (filterSubjectlist.size() == 0) {
            txtMessage.setVisibility(View.VISIBLE);
            recylerview.setVisibility(View.GONE);

        } else {
            txtMessage.setVisibility(View.GONE);
            recylerview.setVisibility(View.VISIBLE);

        }
        adapter.notifyDataSetChanged();
    }
}
