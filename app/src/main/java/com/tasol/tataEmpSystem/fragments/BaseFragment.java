package com.tasol.tataEmpSystem.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.tasol.tataEmpSystem.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class BaseFragment extends Fragment {
    private ProgressDialog dialog;
    protected LocationManager locationManager;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // getLocation();
    }

    public void dispProg() {

    }

    public void initPrg() {

    }

    public void disrog() {

    }

    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public Fragment getCurrentFragment() {
        Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.main_fragment);
        return currentFragment;

    }

    public void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void openkeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public boolean isOpenkeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        return imm.isAcceptingText();
    }

    public void replaceFragment(Fragment fragment, boolean isAddToBackStack) {

        if (isAddToBackStack) {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).addToBackStack(null).commit();
        } else {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, fragment).commit();

        }
    }

    public String getCurrentTimeStamp(String formate) {
        // dd-MM-yyyy-hh-mm-ss
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formate);
        String format = simpleDateFormat.format(new Date());
        Log.d("xxxx", "Current Timestamp: " + format);
        return format;

    }

    public void initDialog(Context context) {
        dialog = new ProgressDialog(context);

        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
    }


    public void showDialog() {
        if (!dialog.isShowing()) {

            dialog.show();
            dialog.setContentView(R.layout.my_progress);

        }
    }

    public void dismissDialog() {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void clearBackStack() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public String dateFromate(String strDate, String oldFormate, String newFormate) {
        SimpleDateFormat format = new SimpleDateFormat(oldFormate);
        Date newDate;
        String date = "";
        try {
            newDate = format.parse(strDate);
            format = new SimpleDateFormat(newFormate);
            date = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Formatted date====>" + date);

        return date;
    }


}
