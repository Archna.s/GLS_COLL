package com.tasol.tataEmpSystem.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.GLS_COLLAGE.fragments.CourseFragment;
import com.tasol.tataEmpSystem.GLS_COLLAGE.models.AttendanceModel;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.SessionManager_GLS;
import com.tasol.tataEmpSystem.MapFullScreenActivity;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.TakeAttendanceActivity;
import com.tasol.tataEmpSystem.adapters.SubjectGraphAdapter;
import com.tasol.tataEmpSystem.models.InfoWindowData;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.FieldsValidator;
import com.tasol.tataEmpSystem.utils.OflineSessionManager;
import com.tasol.tataEmpSystem.utils.SessionManager;
import com.tasol.tataEmpSystem.widgets.CircularTextView;
import com.tasol.tataEmpSystem.widgets.CustomInfoWindowGoogleMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import br.com.gilson.tlcpb.widget.TwoLevelCircularProgressBar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class MonthWiseAttendanceChildeFragment extends BaseFragment implements OnMapReadyCallback {

    CircularTextView txtPresent;


    TextView txtPresentText, txtPresentCount;
    TextView txtHaldDayText, txtHaldDayCount, txtMessage;
    TextView txtAbsentText, txtAbsentCount, txtLeaveBalance;
    TwoLevelCircularProgressBar progress1;
    private RecyclerView recycler_view;
    LinearLayout llMin;


    CircularTextView txtPresentRound, txtHalfDayRound, txtAbsentRound;
    private GoogleMap mMap;
    private Button filterDate;

    ImageView btnMapFull, ivTakeAttendance;

    //Calender
    Calendar myCalendar = Calendar.getInstance();
    private String myFormat = "dd MMM, yyyy";

    public static JSONArray locationArray;
    private String userId = "";
    private String added_on_date;
    private TextView rbTVPresent;
    String monthName = "";
    View view;

    TextView txtSession, txtSubject, txtSem, txtTime, txtBlock, txtStartSession;
    FrameLayout ffStartSession;

    ArrayList<AttendanceModel> attendanceModelArrayList = new ArrayList<>();

/*
    @SuppressLint("ValidFragment")
    public MonthWiseAttendanceChildeFragment(String strMonthName) {
        this.monthName = strMonthName;
    }
*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_month_child_attendance, container, false);
        }
        setup(view);
        //setupGoogleMap(view);

        return view;
    }


    private void setupGoogleMap(View view) {

        SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map));

        mapFragment.getMapAsync(this);
    }

    private void setup(View view) {

        txtSession = (TextView) view.findViewById(R.id.txtSession);
        txtSubject = (TextView) view.findViewById(R.id.txtSubject);
        txtSem = (TextView) view.findViewById(R.id.txtSem);
        txtTime = (TextView) view.findViewById(R.id.txtTime);
        ffStartSession = (FrameLayout) view.findViewById(R.id.ffStartSession);
        txtBlock = (TextView) view.findViewById(R.id.txtBlock);
        txtStartSession = (TextView) view.findViewById(R.id.txtStartSession);
        txtMessage = (TextView) view.findViewById(R.id.txtMessage);

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        ivTakeAttendance = (ImageView) view.findViewById(R.id.ivTakeAttendance);
        txtLeaveBalance = (TextView) view.findViewById(R.id.txtLeaveBalance);
        llMin = (LinearLayout) view.findViewById(R.id.llMin);
        filterDate = (Button) view.findViewById(R.id.filterDate);
        btnMapFull = (ImageView) view.findViewById(R.id.btnMapFull);

        btnMapFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapFullScreenActivity.class);
                startActivity(intent);
            }
        });
        ffStartSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(new CourseFragment(), true);

                // createLecture();
            }
        });

        txtPresentRound = (CircularTextView) view.findViewById(R.id.txtPresentRound);
        rbTVPresent = (TextView) view.findViewById(R.id.rbTVPresent);
        txtPresentText = (TextView) view.findViewById(R.id.txtPresentText);
        txtPresentCount = (TextView) view.findViewById(R.id.txtPresentCount);

        txtHalfDayRound = (CircularTextView) view.findViewById(R.id.txtHalfDayRound);
        txtHaldDayText = (TextView) view.findViewById(R.id.txtHaldDayText);
        txtHaldDayCount = (TextView) view.findViewById(R.id.txtHaldDayCount);


        txtAbsentRound = (CircularTextView) view.findViewById(R.id.txtAbsentRound);
        txtAbsentText = (TextView) view.findViewById(R.id.txtAbsentText);
        txtAbsentCount = (TextView) view.findViewById(R.id.txtAbsentCount);

        progress1 = (TwoLevelCircularProgressBar) view.findViewById(R.id.progress1);

        recycler_view = (RecyclerView) view.findViewById(R.id.recycler_view);

        Date cuurentDate = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(cuurentDate);

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        filterDate.setText(sdf.format(cuurentDate));

        LoginModel userInfo = new SessionManager(getActivity()).getUserInfo();
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(userInfo.added_on);
            added_on_date = df.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        userId = userInfo.id;

        if (new SessionManager_GLS(getActivity()).getISSessionStart().equalsIgnoreCase("true")) {
            displayEndSessionView();

        }

        setupGraph();
        setupRoundtextview();
        //  getAttendanceTask(userId, added_on_date, formattedDate);


        filterDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datedialog = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datedialog.getDatePicker().setMaxDate(new Date().getTime());
                datedialog.show();
            }
        });

        ivTakeAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(new TakeAttendanceActivity(), true);
            }
        });
//        getLocationAttendance(userId, formattedDate);

    }

    private void displayEndSessionView() {

        txtBlock.setVisibility(View.VISIBLE);
        txtSession.setText("Counting Attendance");
        txtSubject.setText("24/26");
        txtStartSession.setText("END SESSION\n00:08:12");

    }

    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity) getActivity()).setTitle("Dashboard");

    }

    private void updateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        filterDate.setText(sdf.format(myCalendar.getTime()));

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String reformattedDate = df.format(myCalendar.getTime());
//        getLocationAttendance(userId, reformattedDate);
    }

    //    {"task":"getLocAttendance","taskData":{"user_id":"6","attendanceday_from":"07/10/2018"}}
    private void getLocationAttendance(String userId, String formattedDate) {
//        initDialog(getActivity());
//        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", userId);
//            json1.put("user_id", "3");
            json1.put("attendanceday_from", formattedDate);

            jsonObject.put("task", "getLocAttendance");
            jsonObject.put("taskData", json1);
            Log.e("getDashboa res", "getLocAttendance : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("getDash res", "data  body" + response.body());
//                dismissDialog();
                mMap.clear();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {
                            locationArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < locationArray.length(); i++) {
                                JSONObject jsonObject1 = locationArray.getJSONObject(i);
                                String strLatitude = jsonObject1.getString("latitude");
                                String strLongtitude = jsonObject1.getString("longitude");

                                double lat = Double.parseDouble(strLatitude);
                                double lng = Double.parseDouble(strLongtitude);
                                LatLng latLng = new LatLng(lat, lng);
                                MarkerOptions markerOptions = new MarkerOptions().position(latLng);

                                InfoWindowData infoWindowData = new InfoWindowData();
                                infoWindowData.setStrImage(ApiClient.BASE_URL + jsonObject1.getString("face_image"));
                                String location = String.valueOf(jsonObject1.getInt("remote"));
                                if (location.equalsIgnoreCase("1")) {
                                    infoWindowData.setPunch_location("Location : Remote");
                                } else {
                                    infoWindowData.setPunch_location("Location : Office");
                                }
                                infoWindowData.setPunch_time("Time : " + jsonObject1.getString("checkdate") + " " + jsonObject1.getString("checkIntime"));
                                infoWindowData.setPunch_type("Type : " + jsonObject1.getString("punch_type"));

                                CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(getActivity());
                                mMap.setInfoWindowAdapter(customInfoWindow);

                                Marker marker = mMap.addMarker(markerOptions);
                                marker.setTag(infoWindowData);
                                marker.showInfoWindow();
                                marker.hideInfoWindow();
                                if (i == locationArray.length() - 1) {
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 12.0f));
                                }
                            }

                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());

            }
        });
    }

    private void setupGraph() {


        // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        // recycler_view.setLayoutManager(layoutManager);
        //  getGraphData(userId, "All");
        getGraphData(userId, "All");
        /*if (monthName.equalsIgnoreCase("All")) {
            getGraphData(userId, "All");
        } else {
            getGraphData(userId, monthName + " 2018");
        }*/
    }

    //    {"task":"listAttendance","taskData":{"user_id":4,"advmonth":"July 2018"}}
    private void getGraphData(String userId, String advMonth) {
        initDialog(getActivity());
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", "4");
            json1.put("advmonth", advMonth);

            jsonObject.put("task", "listAttendance");
            jsonObject.put("taskData", json1);
            Log.e("listAttendance res", "listAttendance : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("getDash res", "data  body" + response.body());
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {

                            JSONObject jsonData = jsonObject.getJSONObject("data");
                            JSONArray graphArray = jsonData.getJSONArray("attendance");
                            ArrayList<Integer> heights = new ArrayList<>();
                            heights.clear();
                            attendanceModelArrayList.clear();
                            for (int i = 0; i < graphArray.length(); i++) {
                                AttendanceModel attendanceModel = new AttendanceModel();
                                JSONObject jsonObject1 = graphArray.getJSONObject(i);
                                attendanceModel.name = jsonObject1.getString("name");
                                attendanceModel.value = jsonObject1.getString("value");
                                attendanceModel.percent = jsonObject1.getString("percent");
                                attendanceModel.total_lec = jsonObject1.getString("total_lec");
                                attendanceModelArrayList.add(attendanceModel);
                                heights.add(Integer.valueOf(jsonObject1.getString("percent")));
                            }


//                            heights.add(40);
//                            heights.add(70);
//                            heights.add(90);
//                            heights.add(100);
//                            heights.add(35);
//                            heights.add(95);

                            int nreHeight = (170 * 30) / 100;

                            int minBottomMargin = (int) (nreHeight * getResources().getDisplayMetrics().density);

                            llMin.setPadding(0, 0, 0, minBottomMargin);
                            llMin.setVisibility(View.GONE);

                            if (attendanceModelArrayList.size() == 0) {
                                txtMessage.setVisibility(View.VISIBLE);
                                recycler_view.setVisibility(View.GONE);
                            } else {
                                txtMessage.setVisibility(View.GONE);
                                recycler_view.setVisibility(View.VISIBLE);
                            }

                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            recycler_view.setLayoutManager(layoutManager);
                            SubjectGraphAdapter adapter = new SubjectGraphAdapter(getActivity(), attendanceModelArrayList);
                            recycler_view.setAdapter(adapter);
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());
                dismissDialog();
            }
        });
    }

    private void setupRoundtextview() {

        txtPresentRound.setStrokeWidth(2);
        txtPresentRound.setStrokeColor(ContextCompat.getColor(getActivity(), R.color.light_green));
        txtPresentRound.setSolidColor(ContextCompat.getColor(getActivity(), R.color.white));


        txtHalfDayRound.setStrokeWidth(2);
        txtHalfDayRound.setStrokeColor(ContextCompat.getColor(getActivity(), R.color.light_orange));
        txtHalfDayRound.setSolidColor(ContextCompat.getColor(getActivity(), R.color.white));


        txtAbsentRound.setStrokeWidth(2);
        txtAbsentRound.setStrokeColor(ContextCompat.getColor(getActivity(), R.color.light_pink));
        txtAbsentRound.setSolidColor(ContextCompat.getColor(getActivity(), R.color.white));

    }

    public void displayPosition(String month) {
        Log.e("12345", "" + month);


    }

    public void setupGetAttendanceTask(String res) {

        try {

            JSONObject jsonObject = new JSONObject(res);

            if (jsonObject.getInt("status_code") == 200) {

                JSONObject jsonDataObject = jsonObject.getJSONObject("data");
                String thisMonthPercentage = jsonDataObject.getString("this_month");
                String present_count = jsonDataObject.getString("present_count");
                String absentCount = jsonDataObject.getString("absent_count");
                txtPresentCount.setText(present_count);
                txtAbsentCount.setText(absentCount);
                rbTVPresent.setText(thisMonthPercentage);

                int total, pr, ab;

                pr = Integer.parseInt(present_count);
                ab = Integer.parseInt(absentCount);

                //pr = 15;
                //  ab = 9;


                total = pr + ab;

                pr = pr * 100 / total;
                ab = (ab * 100 / total) + pr;

                progress1.setProgressValue(pr);
                progress1.setProgressValue2(ab);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //    {"task":"getAttendance","taskData":{"user_id":"3","attendanceday_from":"01/01/2018","attendanceday_to":"12/30/2018","status":"All"}}
    private void getAttendanceTask(String userId, String added_on, String formattedDate) {

        if (!isNetworkAvailable(getActivity())) {
            setupGetAttendanceTask(new OflineSessionManager(getActivity()).getgetAttendance());
            return;
        }
        initDialog(getActivity());
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", userId);
            json1.put("attendanceday_from", added_on);
            json1.put("attendanceday_to", formattedDate);
            json1.put("status", "all");

            jsonObject.put("task", "getAttendance");
            jsonObject.put("taskData", json1);
            Log.e("getAttendance res", "getAttendance : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("getAttendance res", "data  body" + response.body());
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);


                        if (jsonObject.getInt("status_code") == 200) {

                            new OflineSessionManager(getActivity()).setgetAttendance(res);
                            setupGetAttendanceTask(res);

                        } else {
                            new FieldsValidator(getActivity()).customToast(jsonObject.getString("message"));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
    }

    private void createLecture() {
        initDialog(getActivity());
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("classId", com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils.classId);
            json1.put("sectionId", com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils.sectionId);
            json1.put("subjectId", com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils.subjectId);
            json1.put("teacherId", new SessionManager(getActivity()).getUserInfo().id);
            json1.put("major", "258");
            json1.put("minor", "772");

            jsonObject.put("task", "createLecture");
            jsonObject.put("taskData", json1);
            Log.e("res", "createLecture : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                if (response.body() != null) {
                    try {

                        String res = response.body().string();
                        Log.e("response : ", "createLecture : " + res);
                        JSONObject jsonObject = new JSONObject(res);


                        if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                            txtBlock.setVisibility(View.VISIBLE);
                            txtSession.setText("Counting Attendance");
                            txtSubject.setText("24/26");
                            txtStartSession.setText("END SESSION\n00:08:12");
                          /*  Intent intent = new Intent(getActivity(), DashboardActivity.class);
                            intent.putExtra("whatOpen", "mainDashboard");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
*/
                        } else {
                            new FieldsValidator(getActivity()).customToast(jsonObject.getString("message"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissDialog();
                Log.e("res", "error = " + t.toString());

            }
        });
    }

}
