package com.tasol.tataEmpSystem.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.GLS_COLLAGE.fragments.HolidaylistFragment;
import com.tasol.tataEmpSystem.GLS_COLLAGE.fragments.SessionFragment;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.SplashActivity;
import com.tasol.tataEmpSystem.adapters.DrawerAdapter;
import com.tasol.tataEmpSystem.models.DrawerModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.utils.SessionManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class SlidingMenuListFragment extends BaseFragment {

    ListView listView;
    ArrayList<DrawerModel> list = new ArrayList<>();
    DrawerAdapter adapter;
    public static CircleImageView imageView;
    EditText edValue;
    public static TextView txtname, txtDesignation;
    public static Context context;
    LinearLayout llIcons;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sliding_menu_list, null);
        imageView = (CircleImageView) view.findViewById(R.id.imageView);
        listView = (ListView) view.findViewById(R.id.lstSlidingMenu);
        txtname = (TextView) view.findViewById(R.id.txtname);
        llIcons = (LinearLayout) view.findViewById(R.id.llIcons);
        txtDesignation = (TextView) view.findViewById(R.id.txtDesignation);

        txtname.setText(new SessionManager(getActivity()).getUserInfo().username);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                switch (list.get(i).name) {
                    case "Profile": {
                        Intent intent = new Intent(getActivity(), DashboardActivity.class);
                        intent.putExtra("whatOpen", "mainDashboard");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        // startActivity(intent);
                        break;
                    }
                    case "Dashboard": {
                        Intent intent = new Intent(getActivity(), DashboardActivity.class);
                        intent.putExtra("whatOpen", "mainDashboard");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        break;
                    }
                    case "Schedule": {
                        if (new SessionManager(getActivity()).getUserInfo().role.equalsIgnoreCase("student")) {
                            replaceFragment(new StudentSheduleFragment(), true);
                        } else {
                            replaceFragment(new SheduleFragment(), true);
                        }

                        ((DashboardActivity) getActivity()).toggelSlider();
                        break;
                    }
                    case "My Session": {
                        replaceFragment(new SessionFragment(), true);
                        ((DashboardActivity) getActivity()).toggelSlider();
                        break;
                    }
                    case "Upcoming Holidays": {
                        replaceFragment(new HolidaylistFragment(), true);
                        ((DashboardActivity) getActivity()).toggelSlider();
                        break;
                    }
                    case "Logout": {
                        new SessionManager(getActivity()).deleteAllSharePrefs();
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        break;
                    }
                }

            }
        });

/*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount() - 1; i++) {
                    fm.popBackStack();
                }
                ((KairosDashboardActivity) getActivity()).slidingMenu.toggle();

                if (list.get(position).name.equalsIgnoreCase("Inventory Queue")) {

                    AllInventoryQueueListingFragment signal_fragment1 = new AllInventoryQueueListingFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Profile")) {

                    ProfileFragment signal_fragment1 = new ProfileFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Accept Inventory")) {
                    ((KairosDashboardActivity) getActivity()).openScaneDialog(AllInventoryQueueListingFragment.class);
                } else if (list.get(position).name.equalsIgnoreCase("Log out")) {
                    showlogoutDoalog();
                } else if (list.get(position).name.equalsIgnoreCase("Double checkout")) {
                    DoubleCheckoutlistFragment signal_fragment1 = new DoubleCheckoutlistFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Support")) {
                    ContactUsFragment signal_fragment1 = new ContactUsFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else if (list.get(position).name.equalsIgnoreCase("Used Inventory")) {
                    ((KairosDashboardActivity) getActivity()).openScaneDialog(OutBoundlistingFragment.class);
                */
/*    WereHouseActivity signal_fragment1 = new WereHouseActivity();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).addToBackStack(null).commit();
*//*

                } else if (list.get(position).name.equalsIgnoreCase("Find Location")) {
                    LocationFinderFragment signal_fragment1 = new LocationFinderFragment();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_main_container, signal_fragment1).commit();

                } else {
                    Toast.makeText(getActivity(), "Under Development.", Toast.LENGTH_SHORT).show();

                }

                // ((DashboardActivity) getActivity()).tooggelSlidingMenu();
            }
        });
*/

        setup();

        Log.e("333", "onCreateView");
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    private void setup() {

        if (!TextUtils.isEmpty(new SessionManager(context).getUserInfo().designation)) {
            txtDesignation.setVisibility(View.VISIBLE);
            txtDesignation.setText(new SessionManager(context).getUserInfo().designation);

        }
        Glide.with(getActivity()).load(ApiClient.BASE_URL + new SessionManager(getActivity()).getUserInfo().photo).into(imageView);

        DrawerModel model = new DrawerModel();
        model.name = "Profile";
        model.img = R.mipmap.gls_profile;
        list.add(model);

        model = new DrawerModel();
        model.name = "Dashboard";
        model.img = R.mipmap.gls_dashboard;
        list.add(model);

        model = new DrawerModel();
        model.name = "Schedule";
        model.img = R.mipmap.gls_shedule;
        list.add(model);

        model = new DrawerModel();
        model.name = "My Session";
        model.img = R.mipmap.gls_session;
        list.add(model);

        model = new DrawerModel();
        model.name = "Upcoming Holidays";
        model.img = R.mipmap.gls_holidays;
        list.add(model);

        model = new DrawerModel();
        model.name = "Logout";
        model.img = R.mipmap.gls_logout;
        list.add(model);


        adapter = new DrawerAdapter(getActivity(), list);
        listView.setAdapter(adapter);

        llIcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://appsoft.us/"));
                context.startActivity(browserIntent);
            }
        });

    }

    public static void loadProfilePhoto() {

        if (!TextUtils.isEmpty(new SessionManager(context).getUserInfo().designation)) {
            txtDesignation.setVisibility(View.VISIBLE);
            txtDesignation.setText(new SessionManager(context).getUserInfo().designation);

        }
        Glide.with(context).load(ApiClient.BASE_URL + new SessionManager(context).getUserInfo().photo).into(imageView);
    }



    /*  - User cant see price while modify order
     * - Need to changge aboutus url */


}
