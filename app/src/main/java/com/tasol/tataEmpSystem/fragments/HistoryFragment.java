package com.tasol.tataEmpSystem.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tasol.tataEmpSystem.DashboardActivity;
import com.tasol.tataEmpSystem.R;
import com.tasol.tataEmpSystem.database.TimelineHelper;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.models.TimeLineModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends BaseFragment {

    private String userId = "";


    private RecyclerView recyclieviewHistory;
    private RecyclerViewAdapterStudentHistory recyclerViewAdapterHistory;

    ArrayList<TimeLineModel> timeLineModelList = new ArrayList<>();
    private ImageView ivDate;
    TextView txt_no_data_found;

    Calendar myCalendar = Calendar.getInstance();
    private String myFormat = "dd MMM, yyyy";

    public static JSONArray locationArray;
    private TextView rbTVPresent;
    TimelineHelper timelineHelper;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        timelineHelper = new TimelineHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        setUpHistoryData(view);


        return view;
    }

    private void setUpHistoryData(View view) {

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        txt_no_data_found = (TextView) view.findViewById(R.id.txt_no_data_found);


        ivDate = (ImageView) view.findViewById(R.id.date_picker);

        rbTVPresent = (TextView) view.findViewById(R.id.txt_date_student_history);


        ivDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datedialog = new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datedialog.getDatePicker().setMaxDate(new Date().getTime());
                datedialog.show();
            }
        });

        recyclieviewHistory = (RecyclerView) view.findViewById(R.id.rv_history_activity);
        recyclieviewHistory.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerViewAdapterHistory = new RecyclerViewAdapterStudentHistory(timeLineModelList);
        recyclieviewHistory.setAdapter(recyclerViewAdapterHistory);

        LoginModel userInfo = new SessionManager(getActivity()).getUserInfo();

        userId = userInfo.id;

        Date cuurentDate = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(cuurentDate);

        df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedResponseDate = df.format(cuurentDate);

        if (isNetworkAvailable(getActivity())) {
            getTimeLine(userId, formattedDate, formattedResponseDate);

        } else {
            timeLineModelList.clear();
            timeLineModelList.addAll(timelineHelper.getAllHistory(formattedResponseDate));
            recyclerViewAdapterHistory.notifyDataSetChanged();
            if (timeLineModelList.size() == 0) {
                recyclieviewHistory.setVisibility(View.GONE);
                txt_no_data_found.setVisibility(View.VISIBLE);
            } else {
                recyclieviewHistory.setVisibility(View.VISIBLE);
                txt_no_data_found.setVisibility(View.GONE);
            }

        }

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        rbTVPresent.setText(sdf.format(cuurentDate));
    }


    @Override
    public void onResume() {
        super.onResume();
        ((DashboardActivity) getActivity()).setTitle("Timeline");

    }

    //    {"task":"getLocAttendance","taskData":{"user_id":"6","attendanceday_from":"07/10/2018"}}
    private void getTimeLine(String userId, String formattedDate, String formatedResponseDate) {

        timelineHelper.deleteRecordForDate(formatedResponseDate);

        initDialog(getActivity());
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", "191");
            json1.put("attendanceday_from", formattedDate);

            jsonObject.put("task", "timeline");
            jsonObject.put("taskData", json1);
            Log.e("getDashboa res", "timeline : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("getDash res", "data  body" + response.body());
                dismissDialog();
                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {

                            recyclieviewHistory.setVisibility(View.VISIBLE);
                            txt_no_data_found.setVisibility(View.GONE);
                            timeLineModelList.clear();
                            JSONObject jsonDataObject = jsonObject.getJSONObject("data");

                            JSONArray jsonDataArray = jsonDataObject.getJSONArray("studentTimeline");

                            for (int i = 0; i < jsonDataArray.length(); i++) {
                                JSONObject jsonObject1 = jsonDataArray.getJSONObject(i);

                                Gson gson = new Gson();
                                TimeLineModel model = gson.fromJson(jsonObject1.toString(), TimeLineModel.class);
                                timeLineModelList.add(model);
                            }
                            timelineHelper.insertAllHistory(timeLineModelList);
                            recyclerViewAdapterHistory.notifyDataSetChanged();
                        } else if (jsonObject.getInt("status_code") == 204) {
                            recyclieviewHistory.setVisibility(View.GONE);
                            txt_no_data_found.setVisibility(View.VISIBLE);
                            dismissDialog();
                        } else {
                            recyclieviewHistory.setVisibility(View.GONE);
                            txt_no_data_found.setVisibility(View.VISIBLE);
                            dismissDialog();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("getDashboar res", "error = " + t.toString());

            }
        });
    }

    private void updateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        rbTVPresent.setText(sdf.format(myCalendar.getTime()));

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String reformattedDate = df.format(myCalendar.getTime());

        df = new SimpleDateFormat("yyyy-MM-dd");
        String formatedResponseDate = df.format(myCalendar.getTime());

        if (isNetworkAvailable(getActivity())) {
            getTimeLine(userId, reformattedDate, formatedResponseDate);

        } else {
            timeLineModelList.clear();
            timeLineModelList.addAll(timelineHelper.getAllHistory(formatedResponseDate));
            recyclerViewAdapterHistory.notifyDataSetChanged();
            if (timeLineModelList.size() == 0) {
                recyclieviewHistory.setVisibility(View.GONE);
                txt_no_data_found.setVisibility(View.VISIBLE);
            } else {
                recyclieviewHistory.setVisibility(View.VISIBLE);
                txt_no_data_found.setVisibility(View.GONE);
            }
        }
    }

    private class RecyclerViewAdapterStudentHistory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        ArrayList<TimeLineModel> timeLineModelArrayList = new ArrayList<>();

        public RecyclerViewAdapterStudentHistory(ArrayList<TimeLineModel> timeLineModelArrayList) {
            this.timeLineModelArrayList = timeLineModelArrayList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            View parentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_history_row_item,
                    parent, false);
            viewHolder = new ViewHolder(parentView);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
            if (viewHolder instanceof ViewHolder) {
                final ViewHolder holder = (ViewHolder) viewHolder;


                final TimeLineModel timeLineModel = timeLineModelArrayList.get(position);

                holder.txt_punch_type.setText(timeLineModel.punch_type);
                holder.txt_punch_time.setText(timeLineModel.checkIntime);
                holder.txt_punch_address.setText(timeLineModel.address);
                if (timeLineModel.remote.equalsIgnoreCase("1")) {
                    holder.location_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
                } else {
                    holder.location_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_circular_date_green));
                }
            } else {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemCount() {
            return timeLineModelArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txt_punch_type, txt_punch_time, txt_punch_address;
            public ImageView location_icon;

            public ViewHolder(View itemView) {
                super(itemView);
                txt_punch_type = (TextView) itemView.findViewById(R.id.txt_punch_type_history);
                txt_punch_time = (TextView) itemView.findViewById(R.id.txt_punch_time_history);
                txt_punch_address = (TextView) itemView.findViewById(R.id.txt_punch_address_history);
                location_icon = (ImageView) itemView.findViewById(R.id.location_icon);
            }
        }

        public class ProgressViewHolder extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public ProgressViewHolder(View itemView) {
                super(itemView);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            }
        }
    }

}
