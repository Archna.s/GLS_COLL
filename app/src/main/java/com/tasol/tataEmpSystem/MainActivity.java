package com.tasol.tataEmpSystem;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {
    private boolean mPermissionReady;
    public static final String TRAIN_FOLDER = "train_folder";
    public static boolean isFromTrain = false;
    Button btnOpenCv, btnSendFiles;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    private String TAGLOGIN = "%%%MainACt";
    List<MultipartBody.Part> files = new ArrayList<>();

    String MY_PREFS_NAME = "MY_PREFS_NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        editor = prefs.edit();


        btnOpenCv = (Button) findViewById(R.id.btnOpenCv);

        btnSendFiles = (Button) findViewById(R.id.btnSendFiles);


        btnSendFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userID = prefs.getString("userid", null);
                getOtpTask(userID);

            }
        });


        btnOpenCv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPermissionReady) {
                    startActivity(new Intent(MainActivity.this, OpenCvRecognizeActivity.class));
                    int remainigPhotos = TrainHelper.PHOTOS_TRAIN_QTY - TrainHelper.qtdPhotos(getBaseContext());
                    if (remainigPhotos > 0) {
                        Toast.makeText(MainActivity.this, "You need complete your training before recognize.", Toast.LENGTH_SHORT).show();
                    } else {
                        isFromTrain = false;
                        startActivity(new Intent(MainActivity.this, OpenCvRecognizeActivity.class));

                    }
                }
            }
        });

        findViewById(R.id.btnTrain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPermissionReady) {
                    int remainigPhotos = TrainHelper.PHOTOS_TRAIN_QTY - TrainHelper.qtdPhotos(getBaseContext());
                    if (remainigPhotos > 0) {
                        if (remainigPhotos != TrainHelper.PHOTOS_TRAIN_QTY) {
                            // isFromTrain = false;
                        } else {
                            // isFromTrain = true;
                            // customDialog();
                        }
                        startActivity(new Intent(MainActivity.this, OpenCvRecognizeActivity.class));
                    } else {

                        displayDialog();
                    }
                }
            }
        });


        File directory = new File(Environment.getExternalStorageDirectory() + java.io.File.separator + "Neel Directory" + java.io.File.separator + TRAIN_FOLDER);
        if (!directory.exists())
            Toast.makeText(this,
                    (directory.mkdirs() ? "Directory has been created" : "Directory not created"),
                    Toast.LENGTH_SHORT).show();
        else {
            // Toast.makeText(this, "Directory exists", Toast.LENGTH_SHORT).show();

        }


        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storagePermssion = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        mPermissionReady = cameraPermission == PackageManager.PERMISSION_GRANTED
                && storagePermssion == PackageManager.PERMISSION_GRANTED;
        if (!mPermissionReady)
            requirePermissions();
    }

    private void requirePermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Map<String, Integer> perm = new HashMap<>();
        perm.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_DENIED);
        perm.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_DENIED);
        for (int i = 0; i < permissions.length; i++) {
            perm.put(permissions[i], grantResults[i]);
        }
        if (perm.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && perm.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mPermissionReady = true;
        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    || !ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(this)
                        .setMessage(R.string.permission_warning)
                        .setPositiveButton(R.string.dismiss, null)
                        .show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void displayDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to train?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            TrainHelper.reset(getBaseContext());
                            isFromTrain = true;
                            customDialog();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.setTitle("Face Recognize");
        alert.show();
    }

    public void customDialog() {
        // Create custom dialog object

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);


        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog);
        // Set dialog title

        // set values for custom dialog components - text, image and button
        EditText edname;
        Button btnDone, btnCancel;
        edname = (EditText) dialog.findViewById(R.id.edname);
        btnDone = (Button) dialog.findViewById(R.id.btnDone);
        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                finish();

            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(edname.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
                } else {
                    editor.putString("name", edname.getText().toString());
                    editor.apply();
                    dialog.dismiss();
                    startActivity(new Intent(MainActivity.this, OpenCvRecognizeActivity.class));


                }
            }
        });

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    public void getOtpTask(String userID) {

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject taskData = new JSONObject();
            taskData.put("user_id", "2");
            jsonObject.put("task", "storeLocationAttendance");
            jsonObject.put("taskData", taskData);

            Log.v(TAGLOGIN, "reqObj : " + jsonObject);

            File directory = new File(Environment.getExternalStorageDirectory() + java.io.File.separator + "Neel Directory" + java.io.File.separator + TRAIN_FOLDER);

            File[] listFiles = directory.listFiles();
            files.clear();
            for (int i = 0; i < listFiles.length; i++) {
                File tempFile = new File(listFiles[i].getAbsolutePath());
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), tempFile);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("images", tempFile.getName(), requestFile);
                files.add(body);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.sendMultFiles("" + jsonObject, files);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.body() != null) {
                    try {
                        String res = response.body().string();
                        Log.e("res", "get otp" + response.body());
                        JSONObject jsonObject = new JSONObject(res);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v(TAGLOGIN, "error = " + t.toString());
            }
        });
    }


}
