package com.tasol.tataEmpSystem.cam;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;

import com.tasol.tataEmpSystem.KairosActivity;
import com.tasol.tataEmpSystem.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.tasol.tataEmpSystem.KairosActivity.activity;

/**
 * Manages the android camera and sets it up for face detection
 * can throw an error if face detection is not supported on this device
 */
public class FaceDetectionCamera implements OneShotFaceDetectionListener.Listener {

    private final Camera camera;
    int angleToRotate = 0;
    private Listener listener;
    public static String filename = "picture.jpg";
    public static String originalFilename = "original_picture.jpg";

    public static String mainFoldername = "tataEmployeeSystem";
    public static String offlineFolderName = "tataEmployeeSystem_offline";


    public FaceDetectionCamera(Camera camera) {
        this.camera = camera;
    }

    /**
     * Use this to detect faces when you have a custom surface to display upon
     *
     * @param holder the {@link SurfaceHolder} to display upon
     */
    public void initialise(Listener listener, SurfaceHolder holder) {
        this.listener = listener;
        try {
            camera.stopPreview();
        } catch (Exception swallow) {
            // ignore: tried to stop a non-existent preview
        }
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
            camera.setFaceDetectionListener(new OneShotFaceDetectionListener(this));
            camera.startFaceDetection();


            angleToRotate = setCameraDisplayOrientation(activity, Camera.CameraInfo.CAMERA_FACING_BACK, camera);

        } catch (IOException e) {
            this.listener.onFaceDetectionNonRecoverableError();
        }
    }

    @Override
    public void onFaceDetected() {
        listener.onFaceDetected();
    }

    @Override
    public void onFaceTimedOut() {
        listener.onFaceTimedOut();
    }

    public void recycle() {
        if (camera != null) {
            camera.release();
        }
    }

    public interface Listener {
        void onFaceDetected();

        void onFaceTimedOut();

        void onFaceDetectionNonRecoverableError();

    }

    public void takePhoto() {
        camera.takePicture(null, null, jpegCallback);

    }

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            //make a new picture file

            File pictureFile = getOutputMediaFile();

            if (pictureFile == null) {
                return;
            }


            try {

                /*if (new SessionManager(activity).getUserInfo().istrain.equalsIgnoreCase("0")) {
                    File origibalFile = getOutputMediaFileForTraning();
                    FileOutputStream fos1 = new FileOutputStream(origibalFile);
                    fos1.write(data);
                    fos1.close();
                    Log.e("qqqqq", "Picture saved origibalFile : " + origibalFile.getName());

                }
*/
                // Solve image inverting problem
                angleToRotate = angleToRotate + 180;
                Bitmap orignalImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                Bitmap bitmapImage = rotate(orignalImage, angleToRotate);
                Utils.originalBitmap = bitmapImage;
                Display mDisplay = activity.getWindowManager().getDefaultDisplay();
                final int width = (mDisplay.getWidth() / 3);
                final int height = (mDisplay.getHeight() / 3);

                Bitmap resized = Bitmap.createScaledBitmap(bitmapImage, width, height, true);

                //write the file
                FileOutputStream fos = new FileOutputStream(pictureFile);
                resized.compress(Bitmap.CompressFormat.PNG, 50, fos);

                // fos.write(data);
                //fos.flush();
                fos.close();
                Log.e("qqqqq", "Picture saved: " + pictureFile.getName());


                ((KairosActivity) activity).startKairosApiActivity(resized);

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }


            camera.startPreview();

            //refresh camera to continue preview
            //    mPreview.refreshCamera(mCamera);
        }
    };


    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);
        // mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private static File getOutputMediaFile() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        File[] files = mediaStorageDir.listFiles();
        int numberOfFiles = files.length;
        if (numberOfFiles > 1)

            //if this "JCGCamera folder does not exist
            if (!mediaStorageDir.exists()) {
                //if you cannot make this folder return
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + filename);

        return mediaFile;
    }

    private static File getOutputMediaFileForTraning() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + originalFilename);

        return mediaFile;
    }


    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, FaceDetectionCamera.mainFoldername);
    }


    public int setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
        return result;
    }
}
