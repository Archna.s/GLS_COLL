package com.tasol.tataEmpSystem;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;
import com.kairos.Kairos;
import com.kairos.KairosListener;
import com.tasol.tataEmpSystem.GLS_COLLAGE.utils.SessionManager_GLS;
import com.tasol.tataEmpSystem.cam.FaceDetectionCamera;
import com.tasol.tataEmpSystem.database.TakeUserPhotoHelper;
import com.tasol.tataEmpSystem.models.LoginModel;
import com.tasol.tataEmpSystem.models.TakeUserPhotoModel;
import com.tasol.tataEmpSystem.network.ApiClient;
import com.tasol.tataEmpSystem.network.ApiInterface;
import com.tasol.tataEmpSystem.services.FilledAttendaceService;
import com.tasol.tataEmpSystem.services.StoreLocuserAttendanceService;
import com.tasol.tataEmpSystem.utils.FieldsValidator;
import com.tasol.tataEmpSystem.utils.SessionManager;
import com.tasol.tataEmpSystem.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.filename;
import static com.tasol.tataEmpSystem.cam.FaceDetectionCamera.originalFilename;

public class KairosApiCallingActivity extends BaseActivity {

    ArrayList<String> gallerylist = new ArrayList<>();
    KairosListener listener;
    KairosListener listenerForGallry;
    Kairos myKairos;
    Bitmap finalBitmap = null;
    String onClickWhat = "";
    String galleryId = "";
    String getuserIDfromKairos = "";
    DonutProgress progress;
    int intProgress = 0;
    ImageView ivRight, ivBackground;
    FrameLayout llProgress;
    TextView txtBottomText, txtProgresstext, txtKairosTime, txtWebserviceTime, txtTotalTime;

    FrameLayout ffBottom;

    boolean isCommingForTranning = false;
    int timerCount = 0;
    int timeOutSecond = 30;


    Timer timer;
    TimerTask task;
    boolean istimerRunning = false;

    boolean isTimeOut = false;
    boolean isResponseSuccess = false;
    TakeUserPhotoHelper takeUserPhotoHelper = new TakeUserPhotoHelper(KairosApiCallingActivity.this);
    String autoApprove;
    String isTrain;
    boolean isUserAlreadyTrain = false;
    boolean isOnScreenforTraning = false;


    // get image while login.
    // default image


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (istimerRunning) {
            timer.cancel();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kairos_api_calling);
        initDialog(KairosApiCallingActivity.this);
        progress = (DonutProgress) findViewById(R.id.donut_progress);
        ivRight = (ImageView) findViewById(R.id.ivRight);
        ivBackground = (ImageView) findViewById(R.id.ivBackground);
        llProgress = (FrameLayout) findViewById(R.id.llProgress);
        txtBottomText = (TextView) findViewById(R.id.txtBottomText);
        txtProgresstext = (TextView) findViewById(R.id.txtProgresstext);
        ffBottom = (FrameLayout) findViewById(R.id.ffBottom);
        txtKairosTime = (TextView) findViewById(R.id.txtKairosTime);
        txtWebserviceTime = (TextView) findViewById(R.id.txtWebserviceTime);
        txtTotalTime = (TextView) findViewById(R.id.txtTotalTime);


        if (!isNetworkConnected()) {


            txtProgresstext.setText("");
            saveBitmap(Utils.finalBitmap, storeOflineImages());
            TakeUserPhotoModel model = new TakeUserPhotoModel();

            model.user_office_id = new SessionManager(KairosApiCallingActivity.this).getUserInfo().office_id;
            model.user_id = new SessionManager(KairosApiCallingActivity.this).getUserInfo().id;
            model.latitude = "" + location.getLatitude();
            model.longitude = "" + location.getLongitude();
            model.beacon_major = "258";
            model.beacon_minor = "772";
            model.punch_type = Utils.strPunchinType;
            model.status = Utils.imageVerificationStatus;
            model.punching_time = getCurrentTimeStamp("yyyy-MM-dd hh:mm:ss");
            model.timestamp = "" + getCurrentTimeStamp("dd-MM-yyyy hh:mm:ss");

            // model.imagepath = kdfjhgkhghskgsdfg;

            takeUserPhotoHelper.insertFloorDetails(model);
            openDashboard();

            return;
        } else {
            if (new SessionManager(KairosApiCallingActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {

                isCommingForTranning = true;
                txtProgresstext.setText("TRAINING...");
            }
            kairosSetup();


            ivRight.setOnClickListener(new View.OnClickListener() {

                @SuppressLint("WrongConstant")
                @Override
                public void onClick(View view) {
                    if (!isUserAlreadyTrain) {
                        // openTakeAttendaceScreen();
                        if (isCommingForTranning) {
                            Intent i = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        } else {
                            if (Utils.imageVerificationStatus.equalsIgnoreCase("2")) // if user is not verify
                            {
                                Intent i = new Intent(KairosApiCallingActivity.this, KairosActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                            } else {
//                                storeLocationAttendance();

//                                {"task":"storeLocationAttendance","taskData":{"user_office_id":1,"user_id":"3","latitude":"23.0371522","longitude":"72.51219930000002","beacon_major":"151","beacon_minor":"151","punch_type":"In","remote":1,"status":1,"punching_time" : "2018-07-11 13:02:55"}}
                                Log.e("TAG----", new SessionManager(KairosApiCallingActivity.this).getUserInfo().role);
                                if (new SessionManager(KairosApiCallingActivity.this).getUserInfo().role.equalsIgnoreCase("student")) {
                                    storeLocationAttendanceService();
//                                    storeLocationAttendance();
                                } else {
                                    createLecture();
                                    // openDashboard();
                                }
                            }
                        }
                    } else {
                        Intent i = new Intent(KairosApiCallingActivity.this, KairosActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                }
            });


        }
    }

    public void kairosSetup() {

        myKairos = new Kairos();

        String app_id = new SessionManager(KairosApiCallingActivity.this).getAppConfig().kairos_appid;
        String api_key = new SessionManager(KairosApiCallingActivity.this).getAppConfig().kairos_appkey;
        myKairos.setAuthentication(this, app_id, api_key);


        startTimer();
        progress.setShowText(false);

        ivBackground.setImageBitmap(Utils.originalBitmap);


        int delay = 1500; //milliseconds
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                startProgressbar();

            }
        }, delay);

        // AA - 1-> Yes Auto Accept
        //      2-> No

        //autoApprove = "0";
        //isTrain = "1";

        autoApprove = new SessionManager(KairosApiCallingActivity.this).getAppConfig().AutoApprove;
        isTrain = new SessionManager(KairosApiCallingActivity.this).getUserInfo().istrain;
        galleryId = new SessionManager(KairosApiCallingActivity.this).getAppConfig().kairos_galleryname;
//        galleryId = "TATACLOCK";

        Log.e("RRR", "autoApprove : " + autoApprove);
        Log.e("RRR", "isTrain : " + isTrain);
        Log.e("RRR", "gallery name : " + galleryId);


        if (autoApprove.equalsIgnoreCase("0") && isTrain.equalsIgnoreCase("0")) {
            startSericeAndSendData(true);
            openDashboard();

        } else {
            getIntentData();

        }


    }

    public void openDashboard() {
        Intent i = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
        startActivity(i);
        finish();
    }

    private class AsynTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onPostExecute(Void params) {


        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Do some work
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                }
            });

            return null;
        }
    }

    public void startProgressbar() {
        Handler handler = new Handler();
        int delay = 10; //milliseconds


        handler.postDelayed(new Runnable() {
            public void run() {
                //do something
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        intProgress = intProgress + 1;
                        handler.postDelayed(this, delay);
                        progress.setProgress(intProgress);
                    }
                });

            }
        }, delay);

    }

    public static Bitmap reflection(Bitmap mainImage) {
        // gap space between original and reflected
        final int reflectionGap = 4;
        // get image size
        int width = mainImage.getWidth();
        int height = mainImage.getHeight();

        Matrix matrix = new Matrix();
        matrix.preScale(1, -1);

        Bitmap reflectionImage = Bitmap.createBitmap(mainImage, 0,
                height / 2, width, height / 2, matrix, false);

        Bitmap reflectedBitmap = Bitmap.createBitmap(width,
                (height + height / 2), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(reflectedBitmap);
        canvas.drawBitmap(mainImage, 0, 0, null);
        Paint defaultPaint = new Paint();
        canvas.drawRect(0, height, width, height + reflectionGap, defaultPaint);
        canvas.drawBitmap(reflectionImage, 0, height + reflectionGap, null);
        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0,
                mainImage.getHeight(), 0, reflectedBitmap.getHeight()
                + reflectionGap, 0x70ffffff, 0x00ffffff, Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0, height, width, reflectedBitmap.getHeight()
                + reflectionGap, paint);

        return reflectedBitmap;
    }

    public void getAllSubjectID() {
        try {
            listenerForGallry = new KairosListener() {
                boolean isRecSuccess = false;

                @Override
                public void onSuccess(String response) {

                    try {
                        String username = new SessionManager(KairosApiCallingActivity.this).getUserInfo().fullName + "%" + new SessionManager(KairosApiCallingActivity.this).getUserInfo().id;

                        Log.e("rrrr", "gallery : " + response.toString());
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("status")) {
                            String isComplete = jsonObject.getString("status");
                            if (isComplete.equalsIgnoreCase("Complete")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("subject_ids");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    gallerylist.add(jsonArray.get(i).toString());
                                    if (username.equalsIgnoreCase(jsonArray.get(i).toString())) {
                                        isRecSuccess = true;
                                        break;
                                    }

                                }
                                Log.e("isRecSuccess : ", "" + isRecSuccess);
                                if (isRecSuccess) {
                                    startSericeAndSendData(true);
                                } else {
                                    startFaceRecognize();
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFail(String response) {
                    //     dismissDialog();

                    Log.e("KAIROS DEMO error ", response);
                }
            };

            myKairos.listSubjectsForGallery(new SessionManager(KairosApiCallingActivity.this).getAppConfig().kairos_galleryname, listenerForGallry);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        finalBitmap = Utils.finalBitmap;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isTimeOut = bundle.getBoolean("isTimeout", false);
            if (isTimeOut) {
                getAllSubjectID();
            }
        } else {
            startFaceRecognize();

        }
        Log.e("isTime out : ", "" + isTimeOut);


    }

    private void startFaceRecognize() {
        //  startanimate();
        listener = new KairosListener() {


            @Override
            public void onSuccess(String response) {

                Log.e("KAIROS DEMO", response);
                Log.e("KAIROS DEMO time out ", "" + isTimeOut);


                if (isTimeOut) {
                    return;
                }

                //{"Errors":[{"ErrCode":5002,"Message":"no faces found in the image"}]}
                //    dismissDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("ErrCode")) {
                        // reScanDialog();
                    } else {
                        if (onClickWhat.equals("recognition")) {
                            isResponseSuccess = true;
                            timer.cancel();
                            OnclickRecognition(response);
                        } else if (onClickWhat.equalsIgnoreCase("enroll")) {
                            IsEnrolll(response);
                        } else {
                            //finalenroll

                            isResponseSuccess = true;
                            timer.cancel();

                            finalRecognizeAfterEnroll(response);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFail(String response) {
                //     dismissDialog();

                Log.e("KAIROS DEMO error", response);
            }
        };


        recognizeUsingKairos(finalBitmap);
    }


    public void recognizeUsingKairos(final Bitmap bp) {

        Log.e("ttt", "call kairos api");
        finalBitmap = bp;
        // showDialog();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                Handler handler = new Handler();
                //  final Bitmap finalCompressedImageBitmap = compressedImageBitmap;
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Log.e("ttt", "reached in");
                        try {
                            if (isTrain.equalsIgnoreCase("0")) {
                                onClickWhat = "enroll";
                                Log.e("ggg", "enrolling");
                                //myKairos.enroll(finalBitmap, new SessionManager(KairosApiCallingActivity.this).getUserInfo().fullName + "%" + new SessionManager(KairosApiCallingActivity.this).getUserInfo().id, galleryId, null, null, null, listener);
                                myKairos.recognize(bp, galleryId, null, null, null, null, listener);
                            } else {
                                onClickWhat = "recognition";
                                Log.e("ggg", "recognizing");
                                myKairos.recognize(bp, galleryId, null, null, null, null, listener);
                            }

                        } catch (JSONException | UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e("ttt", "catch kairos" + e.toString());
                        }

                    }
                }, 1000);
            }
        }, 00);

    }


    private void IsEnrolll(String response) {
        // dismissDialog();
        Log.e("3333", "final enrolling");
        try {
            JSONObject jsonObject = new JSONObject(response);

            Boolean isSuccess = jsonObject.has("images");

            if (isSuccess) {
                JSONArray jsonArray = jsonObject.optJSONArray("images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    JSONObject trasObj = c.getJSONObject("transaction");

                    String status = trasObj.getString("status");
                    if (status.equalsIgnoreCase("success")) {

                        isResponseSuccess = true;
                        timer.cancel();


                        String name = trasObj.getString("subject_id");
                        //   Toast.makeText(AndroidCameraExample.this, name, Toast.LENGTH_SHORT).show();

                        String str = name;
                        String[] split = str.split("%");
                        // String firstSubString = split[0];
                        getuserIDfromKairos = name;

                        Log.e("890", "  full name : " + name);
                        Log.e("890", " k id : " + getuserIDfromKairos);
                        Log.e("890", " user id : " + new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);

                        txtBottomText.setVisibility(View.VISIBLE);
                        txtBottomText.setText("User is already trained. Please contact administrator.");
                        llProgress.setVisibility(View.GONE);
                        ivRight.setVisibility(View.VISIBLE);
                        ffBottom.setVisibility(View.VISIBLE);
                        ivRight.setImageResource(R.mipmap.wrong);
                        isUserAlreadyTrain = true;


                    } else {
                        String name = trasObj.getString("message");
                        if (name.equalsIgnoreCase("no match found")) {
                            onClickWhat = "finalenroll";
                            timerCount = 0;
                            myKairos.enroll(finalBitmap, new SessionManager(KairosApiCallingActivity.this).getUserInfo().id, galleryId, null, null, null, listener);

                        }

                    }
                }

            } else {
                JSONArray jsonArray = jsonObject.optJSONArray("Errors");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    String message = c.getString("Message");
                    //    openDialog(message, false);
                    txtBottomText.setVisibility(View.VISIBLE);
                    txtBottomText.setText(message);
                    llProgress.setVisibility(View.GONE);
                    ivRight.setVisibility(View.VISIBLE);
                    ffBottom.setVisibility(View.VISIBLE);
                    ivRight.setImageResource(R.mipmap.wrong);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    private void finalRecognizeAfterEnroll(String response) {
        // dismissDialog();
        Log.e("3333", "final enrolling");
        try {
            JSONObject jsonObject = new JSONObject(response);

            Boolean isSuccess = jsonObject.has("images");

            if (isSuccess) {
                JSONArray jsonArray = jsonObject.optJSONArray("images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    JSONObject trasObj = c.getJSONObject("transaction");

                    String status = trasObj.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        String name = trasObj.getString("subject_id");
                        //   Toast.makeText(AndroidCameraExample.this, name, Toast.LENGTH_SHORT).show();

                        String str = name;
                        String[] split = str.split("%");
                        //  String firstSubString = split[0];
                        getuserIDfromKairos = name;

                        Log.e("890", "  full name : " + name);
                        Log.e("890", " k id : " + getuserIDfromKairos);
                        Log.e("890", " user id : " + new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);

                        txtBottomText.setVisibility(View.VISIBLE);
                        txtBottomText.setText("Your Image Training is Completed.");
                        llProgress.setVisibility(View.GONE);
                        ivRight.setVisibility(View.VISIBLE);
                        ffBottom.setVisibility(View.VISIBLE);
                        ivRight.setImageResource(R.mipmap.checked);

                        LoginModel model = new SessionManager(KairosApiCallingActivity.this).getUserInfo();
                        model.istrain = "1";
                        new SessionManager(KairosApiCallingActivity.this).setUserInfo(model);
                        isOnScreenforTraning = true;
                        startSericeAndSendData(true);

                    }
                }

            } else {
                JSONArray jsonArray = jsonObject.optJSONArray("Errors");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    String message = c.getString("Message");
                    // openDialog(message, false);
                    txtBottomText.setVisibility(View.VISIBLE);
                    txtBottomText.setText(message);
                    llProgress.setVisibility(View.GONE);
                    ivRight.setVisibility(View.VISIBLE);
                    ffBottom.setVisibility(View.VISIBLE);
                    ivRight.setImageResource(R.mipmap.wrong);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void OnclickRecognition(String response) {
        // dismissDialog();
        try {
            txtKairosTime.setText("Kairos Response : " + timerCount);

            JSONObject jsonObject = new JSONObject(response);

            Boolean isSuccess = jsonObject.has("images");

            if (isSuccess) {
                JSONArray jsonArray = jsonObject.optJSONArray("images");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    JSONObject trasObj = c.getJSONObject("transaction");

                    String status = trasObj.getString("status");
                    if (status.equalsIgnoreCase("success")) {
                        String name = trasObj.getString("subject_id");
                        //   Toast.makeText(AndroidCameraExample.this, name, Toast.LENGTH_SHORT).show();

                        String str = name;
                        String[] split = str.split("%");
                        // String firstSubString = split[0];
                        getuserIDfromKairos = name;

                        Log.e("890", "  full name : " + name);
                        Log.e("890", " k id : " + getuserIDfromKairos);
                        Log.e("890", " user id : " + new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);
                        //     openTakeAttendaceScreen();

                        isResponseSuccess = true;
                        timer.cancel();
                        ivRight.setVisibility(View.VISIBLE);
                        ffBottom.setVisibility(View.VISIBLE);
                        llProgress.setVisibility(View.GONE);

                        //storeLocationAttendanceService();

                        if (new SessionManager(KairosApiCallingActivity.this).getUserInfo().id.equalsIgnoreCase(getuserIDfromKairos)) {
                            Utils.imageVerificationStatus = "1";

                            // startSericeAndSendData();

                            //saveUserDetectedImage();
                            // openDialog(name, true);

                        } else {
                            Utils.imageVerificationStatus = "2";

                            // sdjfhsfhskfhskdhfshfhsdkfhkd
                            // notAuthPrsonDialog();
                            //  saveUserDetectedImage();
                            reScanDialog();
                        }


                    } else {
                        String name = trasObj.getString("message");
                        //  Toast.makeText(KairosApiCallingActivity.this, name, Toast.LENGTH_SHORT).show();
                        if (new SessionManager(KairosApiCallingActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
                            recognizeUsingKairos("enroll", new SessionManager(KairosApiCallingActivity.this).getUserInfo().fullName);

                            //enrollDialog();
                        } else {
                            reScanDialog();
                        }
                    }
                }

            } else {

                JSONArray jsonArray = jsonObject.optJSONArray("Errors");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject c = jsonArray.getJSONObject(i);
                    String message = c.getString("Message");
                    //  openDialog(message, false);
                    txtBottomText.setVisibility(View.VISIBLE);
                    txtBottomText.setText(message);
                    llProgress.setVisibility(View.GONE);
                    ivRight.setVisibility(View.VISIBLE);
                    ffBottom.setVisibility(View.VISIBLE);
                    ivRight.setImageResource(R.mipmap.wrong);

                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openTakeAttendaceScreen() {


        Intent intent = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    public void startSericeAndSendData(boolean isStartService) {


        // isStart service false mean need to start locationAtte service
        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);


        String filePath = getOutputMediaFileForTraning().getPath();
        File f1 = new File(filePath);
        if (!f1.exists()) {

            try {
                copyFile(file, f1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        isResponseSuccess = true;
        timer.cancel();
        ivRight.setVisibility(View.VISIBLE);
        ffBottom.setVisibility(View.VISIBLE);
        llProgress.setVisibility(View.GONE);

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("subject_id", new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);

            jsonObject.put("task", "saveUserDetectedImage");
            jsonObject.put("taskData", json1);
            Log.e("res ", "saveUserDetectedImage  -  : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isStartService) {
            Intent myIntent = new Intent(KairosApiCallingActivity.this, FilledAttendaceService.class);
            Bundle bundle = new Bundle();
            bundle.putString("jsondata", jsonObject.toString());
            myIntent.putExtras(bundle);
            startService(myIntent);
        } else {

        }
        // openDashboard();

    }

    public void openDialog(String name, boolean isSuccess) {

        final AlertDialog alertDialog = new AlertDialog.Builder(KairosApiCallingActivity.this).create();
        Log.e("098", "recog  name : " + name);

        alertDialog.setTitle("Success");

        if (name.contains("%")) {
            String str = name;
            String[] split = str.split("%");
            String firstSubString = split[0];
            getuserIDfromKairos = split[1];
            alertDialog.setMessage("Name : " + firstSubString);
        } else {
            alertDialog.setMessage("Name : " + name);

        }


        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
                //finish();
                Intent i = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
                //  startActivity(i);
                if (isSuccess) {


                    if (new SessionManager(KairosApiCallingActivity.this).getUserInfo().id.equalsIgnoreCase(getuserIDfromKairos)) {
                        //  saveUserDetectedImage();

                    } else {
                        notAuthPrsonDialog();
                        //  saveUserDetectedImage();
                    }


                } else {
                    // isPhototaken = false;
                }
            }
        });
        alertDialog.show();
    }

    public void notAuthPrsonDialog() {

        final AlertDialog alertDialog = new AlertDialog.Builder(KairosApiCallingActivity.this).create();

        alertDialog.setTitle("Fail");
        alertDialog.setMessage("You are not a authorize person.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //    isPhototaken = false;
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    public void enrollDialog() {
        Button btnOk, btnCancel;
        final EditText enName;
        final Dialog dialog = new Dialog(KairosApiCallingActivity.this);

        dialog.setContentView(R.layout.enroll_dialog);
        dialog.setTitle("Enter Name");

        enName = (EditText) dialog.findViewById(R.id.enName);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                //   showDialog();
                recognizeUsingKairos("enroll", enName.getText().toString());


            }
        });


        dialog.show();


    }

    private void recognizeUsingKairos(final String str, final String name) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                Handler handler = new Handler();
                //   final Bitmap finalCompressedImageBitmap = compressedImageBitmap;
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Log.e("WWW", "reached in");

                        try {


                            // myKairos.recognize(s, galleryId, null, null, null, null, listener);

                            if (str.equals("recognition")) {
                                onClickWhat = "recognition";
                                myKairos.recognize(finalBitmap, galleryId, null, null, null, null, listener);
                            } else {
                                onClickWhat = "enroll";
                                Log.e("098", "enroll name : " + name + "%" + new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);
                                myKairos.enroll(finalBitmap, new SessionManager(KairosApiCallingActivity.this).getUserInfo().id, galleryId, null, null, null, listener);

                            }
                        } catch (JSONException | UnsupportedEncodingException e) {
                            e.printStackTrace();
                            Log.e("WWW", "catch " + e.toString());
                        }

                    }
                }, 300);
            }
        }, 00);


    }

    public void reScanDialog() {
       /* llProgress.setVisibility(View.GONE);
        final AlertDialog alertDialog = new AlertDialog.Builder(KairosApiCallingActivity.this).create();

        alertDialog.setTitle("Fail");
        alertDialog.setMessage("Please try again.");
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //isPhototaken = false;
                alertDialog.dismiss();
                Intent i = new Intent(KairosApiCallingActivity.this, KairosActivity.class);
                startActivity(i);
                finish();

            }
        });
        alertDialog.show();*/

        ivRight.setVisibility(View.VISIBLE);
        ffBottom.setVisibility(View.VISIBLE);
        llProgress.setVisibility(View.GONE);
        ivRight.setImageResource(R.mipmap.wrong);
        //  customToast("Please try again.", R.mipmap.cancel_toast_new);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void saveUserDetectedImage() {
        //showDialog();
        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);


        String filePath = getOutputMediaFileForTraning().getPath();
        File f1 = new File(filePath);
        if (!f1.exists()) {

            try {
                copyFile(file, f1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);
            json1.put("count", "1");
            json1.put("istrain", "1");
            json1.put("latitude", "" + location.getLatitude());
            json1.put("longitude", "" + location.getLongitude());

            jsonObject.put("task", "saveUserDetectedImage");
            jsonObject.put("taskData", json1);
            Log.e("res ", "saveUserDetectedImage  -  : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        RequestBody req = createPartFromString("" + jsonObject);

        Call<ResponseBody> call = apiService.saveUserDetectedImage(req, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


                txtWebserviceTime.setText("Webservice Response : " + timerCount);
                if (response.body() != null) {
                    //successFinalDialog();

                    try {
                        // String res = ;
                        String res = response.body().string();
                        Log.e("res", "saveUserDetectedImage - " + res);

                        JSONObject jsonObject = new JSONObject(res);
                        ivRight.setVisibility(View.VISIBLE);
                        ffBottom.setVisibility(View.VISIBLE);
                        llProgress.setVisibility(View.GONE);
                        if (jsonObject.getInt("status_code") == 200) {

                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }


                //();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("saveUserDetectedImage", "error = " + t.toString());

            }
        });
    }

    public static File getDir() {
        File sdDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        return new File(sdDir, FaceDetectionCamera.mainFoldername);
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    public void setCameraPicOrientation() {
        try {

            File mediaStorageDir = getDir();
            File file = new File(mediaStorageDir.getPath() + File.separator + filename);


            ExifInterface exif = new ExifInterface(file.getPath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            Matrix mat = new Matrix();
            mat.postRotate(angle);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(file),
                    null, options);
            Bitmap bitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);
            ByteArrayOutputStream outstudentstreamOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100,
                    outstudentstreamOutputStream);

        } catch (IOException e) {
            Log.w("TAG", "-- Error in setting image");
        } catch (OutOfMemoryError oom) {
            Log.w("TAG", "-- OOM Error in setting image");
        }
    }

    private static File getOutputMediaFileForTraning() {
        //make a new file directory inside the "sdcard" folder
        File mediaStorageDir = getDir();

        //if this "JCGCamera folder does not exist
        if (!mediaStorageDir.exists()) {
            //if you cannot make this folder return
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        //take the current timeStamp
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + originalFilename);

        return mediaFile;
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }

    }

    public void startTimer() {
        istimerRunning = true;
        timer = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        timerCount = timerCount + 1;

                        if (timerCount == timeOutSecond) {

                            isTimeOut = true;
                            dialogTimeOut();
                        }
                        txtTotalTime.setText("Total time : " + timerCount);

                    }
                });
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

    public void dialogTimeOut() {
        // Create custom dialog object

        timer.cancel();
        ivRight.setVisibility(View.VISIBLE);
        ffBottom.setVisibility(View.VISIBLE);
        llProgress.setVisibility(View.GONE);
        ivRight.setImageResource(R.mipmap.wrong);

        final Dialog dialog = new Dialog(KairosApiCallingActivity.this);
        dialog.setCancelable(false);


        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_timeout);
        // Set dialog title

        // set values for custom dialog components - text, image and button
        Button btnOkay;
        TextView txtname, txtMsg;

        txtname = (TextView) dialog.findViewById(R.id.txtname);
        txtMsg = (TextView) dialog.findViewById(R.id.txtMsg);
        btnOkay = (Button) dialog.findViewById(R.id.btnOkay);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();
                Intent i = new Intent(KairosApiCallingActivity.this, KairosApiCallingActivity.class);
                if (new SessionManager(KairosApiCallingActivity.this).getUserInfo().istrain.equalsIgnoreCase("0")) {
                    i.putExtra("isTimeout", isTimeOut);
                }
                startActivity(i);
                finish();

            }
        });


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    private void saveBitmap(Bitmap bitmap, String path) {
        if (bitmap != null) {
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path); //here is set your file path where you want to save or also here you can set file object directly

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void storeLocationAttendanceService() {


        // isStart service false mean need to start locationAtte service
        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);


        String filePath = getOutputMediaFileForTraning().getPath();
        File f1 = new File(filePath);
        if (!f1.exists()) {

            try {
                copyFile(file, f1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        isResponseSuccess = true;
        timer.cancel();
        ivRight.setVisibility(View.VISIBLE);
        ffBottom.setVisibility(View.VISIBLE);
        llProgress.setVisibility(View.GONE);


        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("user_id", new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);
            json1.put("user_office_id", "13");
            json1.put("latitude", location.getLatitude());
            json1.put("longitude", location.getLongitude());
            json1.put("beacon_major", "258");
            json1.put("beacon_minor", "772");
            json1.put("punch_type", "In");
            json1.put("remote", "1");
            json1.put("status", "1");
            Calendar c = Calendar.getInstance();

            SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String formattedDate1 = df1.format(c.getTime());

            json1.put("punching_time", formattedDate1);
//            json1.put("user_office_id", new SessionManager(KairosApiCallingActivity.this).getUserInfo().office_id);

            jsonObject.put("task", "storeLocationAttendance");
            jsonObject.put("taskData", json1);
            Log.e("request ", "storeLocationAttendance  -  : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent myIntent = new Intent(KairosApiCallingActivity.this, StoreLocuserAttendanceService.class);
        Bundle bundle = new Bundle();
        bundle.putString("jsondata", jsonObject.toString());
        myIntent.putExtras(bundle);
        startService(myIntent);

//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//
//        Call<ResponseBody> call = apiService.getData("" + jsonObject);
//        call.enqueue(new Callback<ResponseBody>() {
//            @SuppressLint("WrongConstant")
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                dismissDialog();
//                if (response.body() != null) {
//                    try {
//
//                        String res = response.body().string();
//                        Log.e("response : ", "createLecture : " + res);
//                        JSONObject jsonObject = new JSONObject(res);
//
//
//                        if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {
//
//
//                            Intent intent = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
//                            intent.putExtra("whatOpen", "mainDashboard");
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
//
//                        } else {
//                            new FieldsValidator(KairosApiCallingActivity.this).customToast(jsonObject.getString("message"));
//                        }
//                    } catch (IOException | JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else if (response.code() == 402) {
//                    //errorBody(response.errorBody());
//                } else {
//                    //errorBody(response.errorBody());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dismissDialog();
//                Log.e("res", "error = " + t.toString());
//
//            }
//        });


//        storeLocationAttendance();
//
    }
    // openDashboard();


    public void storeLocationAttendance() {
        showDialog();


        File mediaStorageDir = getDir();
        File file = new File(mediaStorageDir.getPath() + File.separator + filename);


        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        RequestBody req = createPartFromString("" + Utils.attendaceObject);


        Call<ResponseBody> call = apiService.saveUserDetectedImage(req, body);

        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();

                if (response.body() != null) {
                    //successFinalDialog();

                    try {
                        // String res = ;
                        String res = response.body().string();
                        Log.e(" response", "storeLocationAttendance - " + res);

                        JSONObject jsonObject = new JSONObject(res);

                        if (jsonObject.getInt("status_code") == 200) {
                            new SessionManager(KairosApiCallingActivity.this).setPunchin(Utils.strPunchinType);
                            Intent intent = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            //replaceFragment(new MonthWiseAttendanceChildeFragment(), false);

                        } else {
                            new FieldsValidator(KairosApiCallingActivity.this).customToast(jsonObject.getString("message"));
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {

                    //errorBody(response.errorBody());
                }


                //();


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("storeLocationAttendance", "error = " + t.toString());

            }
        });

    }

    private void createLecture() {
        initDialog(KairosApiCallingActivity.this);
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject json1 = new JSONObject();
            json1.put("classId", com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils.classId);
            json1.put("sectionId", com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils.sectionId);
            json1.put("subjectId", com.tasol.tataEmpSystem.GLS_COLLAGE.utils.Utils.subjectId);
            json1.put("teacherId", new SessionManager(KairosApiCallingActivity.this).getUserInfo().id);
            json1.put("major", "258");
            json1.put("minor", "772");

            jsonObject.put("task", "createLecture");
            jsonObject.put("taskData", json1);
            Log.e("res", "createLecture : " + jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getData("" + jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissDialog();
                if (response.body() != null) {
                    try {

                        String res = response.body().string();
                        Log.e("response : ", "createLecture : " + res);
                        JSONObject jsonObject = new JSONObject(res);


                        if (jsonObject.getString("status_code").equalsIgnoreCase("200")) {

                            new SessionManager_GLS(KairosApiCallingActivity.this).setISSessionStart("true");
                            Intent intent = new Intent(KairosApiCallingActivity.this, DashboardActivity.class);
                            intent.putExtra("whatOpen", "mainDashboard");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        } else {
                            new FieldsValidator(KairosApiCallingActivity.this).customToast(jsonObject.getString("message"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == 402) {
                    //errorBody(response.errorBody());
                } else {
                    //errorBody(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissDialog();
                Log.e("res", "error = " + t.toString());

            }
        });
    }


}
